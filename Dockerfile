#FROM node:14.1-alpine as build
#WORKDIR /app
#COPY package*.json ./
#RUN npm install --production
#COPY . .
#RUN npm run build

#FROM nginx:1.17-alpine
#COPY --from=build /app/build /bin/www
#COPY /nginx.conf /etc/nginx/conf.d/default.conf
#EXPOSE 80
#CMD [ "nginx", "-g", "daemon off;" ]


FROM nginx:1.17-alpine
COPY --from=build https://git.irisaco.com/mss/artifacts/client/build /bin/www
COPY https://git.irisaco.com/mss/artifacts/client/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD [ "nginx", "-g", "daemon off;" ]