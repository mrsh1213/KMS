
### Docker

This project works in a docker container as well

###Build
`docker build --tag kms-front:1.0.0 .`

###Run
`sudo docker run -d --memory=1024MB --network=kms-network  -p 80:1213 kms-front:1.0.0 `
###Save
`docker save -o ./kms-front:1.0.0.tar kms-front:1.0.0`
###Load
`docker load -i <path to image tar file>`