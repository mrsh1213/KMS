import React from 'react';
import "./assets/css/bootstrap.css";
import "./assets/css/customBS4.css";
import './App.css';
import {BrowserRouter as Router, Route} from "react-router-dom";
import Dashboard from "./components/Dashboard";
import SignIn from "./views/SignIn";
import {makeStyles} from "@material-ui/core/styles";
import {SnackbarProvider} from "notistack";
import "./assets/css/anim.css";
import {BackDropProvider} from "./config/BackDropContext"
import Root from "./components/Root";
import Home from "./views/Home";
import moment from "moment";
import jMoment from "moment-jalaali";


jMoment.loadPersian({dialect: "persian-modern", usePersianDigits: true});
moment.updateLocale('fa'
    , {
        jMonthsShort: ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'],
    }
);
const styles = makeStyles((theme) => ({
    success: {backgroundColor: 'purple'},
    error: {backgroundColor: 'blue'},
    warning: {backgroundColor: 'green'},
    info: {backgroundColor: 'yellow'},
}));
export default function App() {
    const classes = styles();
        return (
            <BackDropProvider>
                <SnackbarProvider
                    autoHideDuration={1500}
                    className="animate__bounceIn  animate__animated"
                    classes={{
                        variantAlarm: classes.alarm,
                    }}
                    maxSnack={5}>
                    <Router>
                        <Route path='/main' component={Root}/>
                        <Route path='/dashboard' component={Dashboard}/>
                        <Route path='/signIn' component={SignIn}/>
                        <Route exact path='/' component={Home}/>
                    </Router>
                </SnackbarProvider>
            </BackDropProvider>
        )
}

