import React, {memo} from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import {makeStyles} from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
    Dialog: {
        [theme.breakpoints.down('md')]: {
            minWidth: "80%"
        },
        [theme.breakpoints.up('md')]: {
            minWidth: "40%"
        },

    }
}));

DevDialog.propTypes = {
    open: PropTypes.bool,
    handleClose: PropTypes.func,
};


function DevDialog(props) {
    const {open, handleClose, children, className, ...rest} = props;
    const classes = useStyles();


    return (

        <Dialog
            className={className}
            classes={{paper: classes.Dialog}}
            // fullScreen={fullScreen}
            open={open}
            onClose={handleClose}
            aria-labelledby="responsive-dialog-title"
            {...rest}
        >
            <DialogContent>
                {children}
            </DialogContent>
        </Dialog>
    );
}

export default memo(DevDialog);