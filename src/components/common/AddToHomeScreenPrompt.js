import * as React from "react";
import {useEffect} from "react";
import {useState} from "react";
import DevDialog from "./DevDialog";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

export default function AddToHomeScreenPrompt({children}) {
    const [prompt, setState] = useState(null);
    const [isVisible, setVisibleState] = useState(false);

    const hide = () => setVisibleState(true);

    const promptToInstall = () => {
        if (prompt) {
            return prompt.prompt();
        }
        return Promise.reject(
            new Error(
                'Tried installing before browser sent "beforeinstallprompt" event'
            )
        );
    };

    useEffect(() => {
        const ready = (e) => {
            e.preventDefault();
            setState(e);
        };
        window.addEventListener("beforeinstallprompt", ready);
        return () => {
            window.removeEventListener("beforeinstallprompt", ready);
        };
    }, []);

    useEffect(
        () => {
            if (prompt) {
                setVisibleState(true);
            }
        },
        [prompt]
    );
    if (!isVisible) {

        return children;

    } else {
        return (
            <DevDialog open={isVisible} handleClose={hide}>
                <Grid spacing={4} container>
                    <Grid xs={12} item>
                        برای ادامه کار نرم افزار را باید نصب نمایید
                    </Grid>
                    <Grid xs={4} item/>
                    <Grid xs={4} item>
                        <Button size={"small"} fullWidth variant={"contained"} onClick={promptToInstall}
                                color={"primary"}>نصب</Button>
                    </Grid>
                    <Grid xs={4} item/>

                </Grid>
            </DevDialog>
        )
    }
}