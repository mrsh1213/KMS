import React, {useCallback, useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import imageCompression from "browser-image-compression";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import ReactCrop from "react-image-crop";

const pixelRatio = window.devicePixelRatio || 1;


DevImageCrop.propTypes = {
    inputRef: PropTypes.any,
    aspect: PropTypes.number,
    maxSizeMB: PropTypes.number,
    circularCrop: PropTypes.bool,
    uploadHandler: PropTypes.func,
    getPreviewCanvas: PropTypes.func,
};

function DevImageCrop(props) {
    const {inputRef, getPreviewCanvas, maxSizeMB, uploadHandler, aspect, circularCrop} = props;
    const optionsImageCompression = {
        maxSizeMB: maxSizeMB ? maxSizeMB : 1,          // (default: Number.POSITIVE_INFINITY)
        maxWidthOrHeight: 500,   // compressedFile will scale down by ratio to a point that width or height is smaller than maxWidthOrHeight (default: undefined)
        useWebWorker: true,      // optional, use multi-thread web worker, fallback to run in main-thread (default: true)
    };
    const [upImg, setUpImg] = useState(null);
    const imgRef = useRef(null);
    const previewCanvasRef = useRef(null);
    const [crop, setCrop] = useState({unit: "%", width: 100, aspect: aspect ? aspect : 9 / 9});
    const [openCrop, setOpenCrop] = useState(false);
    const [completedCrop, setCompletedCrop] = useState(null);

    useEffect(() => {
        if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
            return;
        }

        const image = imgRef.current;
        const canvas = previewCanvasRef.current;
        const crop = completedCrop;

        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        const ctx = canvas.getContext("2d");

        canvas.width = crop.width * pixelRatio;
        canvas.height = crop.height * pixelRatio;

        ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
        ctx.imageSmoothingQuality = "high";

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );
    }, [completedCrop]);
    const onSelectFile = (e) => {
        if (e.target.files && e.target.files.length > 0) {
            handleOpenCrop();
            const reader = new FileReader();
            reader.addEventListener("load", () => setUpImg(reader.result));
            reader.readAsDataURL(e.target.files[0]);
        }
    };
    const getResizedCanvas = useCallback((canvas, newWidth, newHeight) => {
            const tmpCanvas = document.createElement("canvas");
            tmpCanvas.width = newWidth;
            tmpCanvas.height = newHeight;

            const ctx = tmpCanvas.getContext("2d");
            ctx.drawImage(
                canvas,
                0,
                0,
                canvas.width,
                canvas.height,
                0,
                0,
                newWidth,
                newHeight
            );

            return tmpCanvas;
        }
        , []);
    const onLoad = useCallback((img) => {
        imgRef.current = img;
    }, []);

    const generateDownload = useCallback((previewCanvas, crop) => {
        if (!crop || !previewCanvas) {
            return;

        }
        const canvas = getResizedCanvas(previewCanvas, crop.width, crop.height);
        canvas.toBlob(
            (blob) => {
                const form = new FormData();
                imageCompression(blob, optionsImageCompression).then(file => {
                    form.append("image", file);
                    uploadHandler(form, file);
                })
            },
            "image/png",
            1
        );
    }, [getResizedCanvas, uploadHandler, optionsImageCompression]);

    function closeCrop() {
        cancelCrop()
    }

    function handleOpenCrop() {
        setOpenCrop(true);
    }

    const cancelCrop = useCallback(() => {
        setOpenCrop(false);
        setUpImg(null);
        imgRef.current = null;
        // inputRef.current = null;
        previewCanvasRef.current = null;
        setCrop({unit: "%", width: 100, aspect: aspect ? aspect : 9 / 9});
        setCompletedCrop(null);
    }, [aspect]);
    const acceptCrop = useCallback(() => {
        generateDownload(previewCanvasRef.current, completedCrop)
        cancelCrop()

    }, [completedCrop, previewCanvasRef, cancelCrop, generateDownload]);
    useEffect(() => {
        if (getPreviewCanvas) {
            getPreviewCanvas(previewCanvasRef)
        }
    }, [previewCanvasRef, getPreviewCanvas])


    return (
        <><input ref={inputRef} style={{display: "none"}} type="file" accept="image/*"
                 onChange={onSelectFile}/>
            <Dialog
                open={openCrop}
                onClose={closeCrop}
            >
                <ReactCrop
                    circularCrop={circularCrop}
                    keepSelection={true}
                    imageStyle={{minWidth: 200, minHeight: 200}}
                    src={upImg}
                    onImageLoaded={onLoad}
                    crop={crop}
                    onChange={(c) => setCrop(c)}
                    onComplete={(c) => setCompletedCrop(c)}
                />
                <Button disabled={!completedCrop?.width || !completedCrop?.height} onClick={acceptCrop}
                        style={{color: "#49cc40"}}>تایید</Button>
                <Button onClick={cancelCrop} style={{color: "#cc3e2f"}}>انصراف</Button>
                <canvas
                    ref={previewCanvasRef}
                    // Rounding is important so the canvas width and height matches/is a multiple for sharpness.
                    style={{
                        display: "none",
                        width: Math.round(completedCrop?.width ?? 0),
                        height: Math.round(completedCrop?.height ?? 0)
                    }}
                />
            </Dialog>
        </>
    );
}

export default DevImageCrop;