import React, {useEffect, useState} from 'react';
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import CardContent from "@material-ui/core/CardContent";
import {ExpandMore} from "@material-ui/icons";
import {makeStyles} from "@material-ui/styles";

const styles = makeStyles(() => ({
    card: {
        "borderRadius": "10px",
        "boxShadow": "0 4px 16px 0 rgba(37, 56, 88, 0.1)",
        "backgroundColor": "#ffffff",
        marginTop: 12
    },
    cardAction: {
        paddingTop: 0,
        paddingBottom: 0
    },
    showExpandButton: {
        margin: 6,
        fontSize: 24,
        color: "#757575"
    },
    title: {
        "fontFamily": "'IranSans'",
        "fontSize": "12px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.58",
        "letterSpacing": "normal",
        "textAlign": "right",
        "color": "#42516e"
    },
    content: {
        textAlign: "justify",
        paddingTop: 5
    }
}));
DevCardCollapse.propTypes = {};

function DevCardCollapse(props) {
    const [expanded, setExpanded] = useState(Boolean(props.renderChildren));
    const {title, children, renderChildren} = props;
    const classes = styles();

    useEffect(() => {
        if (renderChildren) {
            let id = setTimeout(() => {
                setExpanded(false)
            }, 150);
            return () => {
                clearTimeout(id);
            }
        }
    }, [renderChildren]);

    function toggleExpanded() {
        setExpanded(prevState => !prevState)

    }

    return (
        <Card className={classes.card}>
            <CardActions
                onClick={toggleExpanded}
                className={classes.cardAction} disableSpacing>
                <div className={classes.title}>{title}</div>
                <div style={{marginRight: "auto"}}/>
                <ExpandMore className={classes.showExpandButton}
                            style={expanded ? {transform: "rotate(180deg)"} : undefined}/>
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent className={classes.content}>{children}</CardContent>
            </Collapse>
        </Card>
    );
}

export default DevCardCollapse;