import React, {useCallback, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Search} from "@material-ui/icons";
import InputBase from "@material-ui/core/InputBase";
import {makeStyles} from "@material-ui/styles";
import {fade} from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import {debounce} from "lodash";
import {detectDirection, fixNumbersToEn} from "../../config/Helper";

DevSearchBox.propTypes = {
    searchMethod: PropTypes.func
};

const searchBar = makeStyles((theme) => ({
    container: {
        paddingBottom: theme.spacing(2)
    },
    search: {
        border: "unset",
        position: 'relative',
        // fontSize:"4rem",
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        width: '100%',
        height: "48px",

    },
    searchIcon: {
        padding: "0 12px",
        height: '95%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
        fontFamily: "'IranSans'",
    },
    divider: {
        marginLeft: 40,
        position: 'absolute',
        display: 'flex',
        marginTop: 12,
        width: 1,
        height: 24,
        borderRadius: 1,
        backgroundColor: "#e0e1e5"
    },
    inputInput: {
        fontFamily: "'IranSans'",
        minHeight: "-webkit-fill-available",
        borderRadius: 10,
        boxShadow: "0 4px 16px 0 rgba(37, 56, 88, 0.1)",
        fontSize: 16,
        padding: 12,
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(6)}px)`,
        transition: theme.transitions.create('width'),
    },

}));

function DevSearchBox({searchMethod, disabled, Icon, ...rest}) {
    const [text, setText] = useState("");
    const classes = searchBar();

    function updateQuery() {
        let keyword = fixNumbersToEn(text.trim()).replace(/ك/g, "ک");
        keyword = keyword.replace(/ي/g, "ی");
        searchMethod(keyword);
    }

    const delayedQuery = useCallback(debounce(updateQuery, 500), [text]);
    useEffect(() => {
        delayedQuery();
        // Cancel previous debounce calls during useEffect cleanup.
        return delayedQuery.cancel;
    }, [text, delayedQuery]);

    function onChange(e) {
        setText(e.target.value);
    }

    return (
        <div className={classes.container}>
            <div className={classes.search}>

            <span className={classes.searchIcon}>
                {Icon ? <Icon style={{fontSize: 18, color: "#a7aeb8"}} className={classes.appBarIcon}/> :
                    <Search className={classes.appBarIcon}/>}

            </span>
                <Divider className={classes.divider} variant={"middle"} orientation={"vertical"}/>
                <InputBase
                    dir={text ? detectDirection(text.trim().replace(/#|@/g, "")) : "rtl"}
                    disabled={disabled}
                    onChange={onChange}
                    value={text}
                    fullWidth
                    placeholder="جستجو"
                    classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput,
                    }}
                    inputProps={{'aria-label': 'search'}}
                    {...rest}
                />
            </div>
        </div>
    );
}

export default DevSearchBox;