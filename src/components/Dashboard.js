import React, {useEffect} from 'react';
import {DashboardRouter} from "../routes";
import SideBar from "./Layout/SideBar";
import {withRouter} from 'react-router';
import {Route, Switch, useHistory} from "react-router";
import ScrollToTop from "./ScrollTop";
import {makeStyles} from "@material-ui/core/styles";
import TabDashboard from "./Layout/Tabs/TabDashboard";
import Page404 from "./Layout/Page404";
import AddToHomeScreenPrompt from "./common/AddToHomeScreenPrompt";

const useStyles = makeStyles((theme) => ({
    content: {
        height: "100%",
        paddingLeft: 16,
        paddingRight: 16,
        [theme.breakpoints.up('md')]: {
            marginLeft: 260,
            paddingTop: 72,
            paddingBottom: 72,
        },
        paddingTop: 72,
        paddingBottom: 86
    }
}));

const routerPath = {
    "/dashboard": {
        title: "صفحه اصلی",
        id: 0
    },
    "/dashboard/wiki": {
        title: "مقالات",
        id: 1
    },
    "/dashboard/posts": {
        title: "گفتمان",
        id: 2
    },
    "/dashboard/documents": {
        title: "اسناد",
        id: 3
    }
}

function Dashboard(props) {
    const history = useHistory();
    const classes = useStyles();


    useEffect(() => {
        if (!localStorage.getItem("user")) {
            history.push("/signIn")
        }
    }, [props, history])

    if (!localStorage.getItem("user")) {
        return <span/>
    }
    return (
        routerPath[props.location.pathname] ? <AddToHomeScreenPrompt>

            <SideBar title={routerPath[props.location.pathname].title}/>
            <div className={classes.content}>
                <ScrollToTop>
                    <Switch>
                        <Route path={"/dashboard"}>
                            <DashboardRouter/>
                        </Route>
                    </Switch>
                </ScrollToTop>
            </div>
            <TabDashboard selected={routerPath[props.location.pathname].id}/>
        </AddToHomeScreenPrompt> : <Page404/>
    );


}

export default withRouter(Dashboard);
