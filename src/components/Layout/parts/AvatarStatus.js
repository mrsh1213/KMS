import {Avatar, Badge, withStyles} from "@material-ui/core";
import React from "react";
import jpg from "../../../assets/images/1.jpg";
import PropTypes from 'prop-types';
import {makeStyles} from "@material-ui/styles";
// const StyledBadgeOnline = withStyles((theme) => ({
//     badge: {
//         backgroundColor: '#44b700',
//         color: '#44b700',
//         boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
//         '&::after': {
//             position: 'absolute',
//             top: 0,
//             left: 0,
//             width: '100%',
//             height: '100%',
//             borderRadius: '50%',
//             animation: '$ripple 1.2s infinite ease-in-out',
//             border: '1px solid currentColor',
//             content: '""',
//         },
//     },
//     '@keyframes ripple': {
//         '0%': {
//             transform: 'scale(.5)',
//             opacity: 1,
//         },
//         '100%': {
//             transform: 'scale(2.2)',
//             opacity: 0,
//         },
//     },
// }))(Badge);

const useStyles = makeStyles((theme) => ({
    badge: {
        zIndex: 0,
        right: 0,
        [theme.breakpoints.down('md')]: {
            transform: "scale(1.3) translate(-50%, 50%)"
        }, [theme.breakpoints.up('md')]: {
            transform: "scale(1.2) translate(-50%, 50%)"
        }
        ,
        backgroundColor: props => props.status ? '#44b700' : '#b70013',
        color: props => props.status ? '#44b700' : '#b70013',
        boxShadow: `0 0 0 0px ${theme.palette.background.paper}`,
        '&::after': {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            animation: '$ripple 1.2s infinite ease-in-out',
            border: props => props.status ? '1px solid currentColor' : "none",
            content: '""',
        },
    },
    '@keyframes ripple': {
        '0%': {
            transform: 'scale(.95)',
            opacity: 1,
        },
        '100%': {
            transform: 'scale(1.97)',
            opacity: 0,
        },
    },
    avatar: {
        [theme.breakpoints.down('md')]: {
            width: '3rem',
            height: '3rem',
        }, [theme.breakpoints.up('md')]: {
            width: '2.5rem',
            height: '2.5rem',
        }
    },
}));
// const useStyles = makeStyles((theme) => ({
//
// }));
AvatarStatus.propTypes = {
    img: PropTypes.any,
    status: PropTypes.bool,
    style: PropTypes.object,
    className: PropTypes.object
};

function AvatarStatus(props) {
    const classes = useStyles(props);

    const {style, img, className} = props;
    return (
        <Badge
            classes={{badge: classes.badge}}
            className={className ? className.badge : undefined}
            overlap="circle"
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
            }}
            variant="dot"
        >
            <Avatar className={className ? className.avatar : classes.avatar} style={style} alt="Remy Sharp"
                    src={img ? img : jpg}/>
        </Badge>
    )
}

export default (AvatarStatus);