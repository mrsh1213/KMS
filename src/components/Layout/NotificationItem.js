import React from 'react';
import PropTypes from 'prop-types';
import IconButton from "@material-ui/core/IconButton";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import {AiOutlineIssuesClose} from "react-icons/ai";
import moment from "moment";
import {Alarm, Notifications, Visibility} from "@material-ui/icons";
import {AiFillBug, AiFillThunderbolt, AiFillCheckSquare} from 'react-icons/ai';
import {MdBookmark} from 'react-icons/md';

const useStyles = makeStyles((theme) => ({
    root: {
        color: "#000"
    }
}));

const ContentMessage = function ({message, type}) {

    if (type === "issueCreated") {
        if (message.includes("BUG")) {
            return <><span className={"badge badge-danger mr-1"}><AiFillBug/></span>{message.replace("-BUG", " ")}</>;
        } else if (message.includes("TASK")) {
            return <><span
                className={"badge badge-primary mr-1"}><AiFillCheckSquare/></span>{message.replace("-TASK", " ")}</>;
        } else if (message.includes("EPIC")) {
            return <><span
                className={"badge badge-secondary mr-1"}><AiFillThunderbolt/></span>{message.replace("-EPIC", " ")}</>;
        } else if (message.includes("STORY")) {
            return <><span
                className={"badge badge-success mr-1"}><MdBookmark/></span>{message.replace("-STORY", " ")}</>;
        }
    } else {
        return message;
    }

};
const HeaderType = function ({type, classes}) {
    switch (type) {
        case "reminder":
            return (<>
                <Grid xs={3} item>
                    <Alarm className="animate__headShake animate__delay-1s  animate__animated"
                           style={{color: "#d32f2f"}}/>
                </Grid>
                <Grid xs={4} item>
                    <Typography classes={{root: classes.root}} variant="subtitle2">
                        رویداد
                    </Typography>
                </Grid>
            </>);
        case "issueCreated":
            return (<>
                <Grid xs={2} item>
                    <AiOutlineIssuesClose className="animate__headShake animate__delay-1s  animate__animated"
                                          style={{fontSize: '1.8rem', color: "#5b10d3"}}/>
                </Grid>
                <Grid xs={5} item>
                    <Typography classes={{root: classes.root}} variant="subtitle2">
                        درخواست
                    </Typography>
                </Grid>
            </>);
        default:
            return (<>
                <Grid xs={3} item>
                    <Notifications/>
                </Grid>
                <Grid xs={4} item>
                    <Typography classes={{root: classes.root}} variant="subtitle2">
                        اعلان
                    </Typography>
                </Grid>
            </>);

    }

}
NotificationItem.propTypes = {
    message: PropTypes.string,
    type: PropTypes.string,
    backgroundColor: PropTypes.string,
    time: PropTypes.number,
    id: PropTypes.string,
    seenNotification: PropTypes.func,
};

function NotificationItem({message, type, backgroundColor, seenNotification, time, id}) {
    const classes = useStyles();
    return (
        <Card variant={"elevation"} elevation={3} style={{margin: 8}}>
            <CardActions style={backgroundColor ? {backgroundColor: backgroundColor} : {backgroundColor: "#eba589"}}>
                <Grid container>
                    <HeaderType classes={classes} type={type}/>
                    <Grid xs={5} item>
                        <Typography classes={{root: classes.root}} variant={"caption"} gutterBottom>
                            {moment(new Date(time)).fromNow()}
                        </Typography>
                    </Grid>
                </Grid>
            </CardActions>
            <CardContent>
                <Typography classes={{root: classes.root}} variant="body2" component="p">
                    <ContentMessage type={type} message={message}/>
                </Typography>
            </CardContent>
            <CardActions>
                <div style={{margin: "auto"}}/>
                <IconButton color={"primary"} variant={"outlined"} size="small" onClick={() => {
                    seenNotification(id)
                }}><Visibility/></IconButton>
            </CardActions>
        </Card>
    );
}

export default NotificationItem;