import React, {useCallback} from 'react';
import PropTypes from 'prop-types';

import NotificationItem from "./NotificationItem";

NotificationsList.propTypes = {
    notifications: PropTypes.array,
    ws: PropTypes.object,
};

function NotificationsList(props) {
    const {notifications,/*ws*/} = props;

    // ws.current.client.onmessage = (evt) => {
    //     console.log("evt", evt)
    // }
    const seenNotification = useCallback(function (id) {
        // console.log("id", id)
        // console.log("ws", ws)
        // console.log("ws", )

        // ws.current.sendMessage("/notifier/readNotification/" + localStorage.getItem("user"), id)
    }, []);
    return (
        <div className={notifications.length > 0 ? "" : "d-flex justify-content-center"} style={{
            maxHeight: 450,
            minHeight: 120, width: 250
        }}>
            {notifications.length > 0 ? notifications.map(noti => <NotificationItem key={noti.id}
                                                                                    id={noti.id}
                                                                                    seenNotification={seenNotification}
                                                                                    backgroundColor={noti.backgroundColor}
                                                                                    type={noti.type}
                                                                                    message={noti.message}
                                                                                    time={noti.time}/>) :
                <span className="text-muted font-weight-bold align-self-center">اعلانی موجود نمی باشد</span>
            }

        </div>
    );
}

export default NotificationsList;