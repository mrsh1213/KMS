import React from 'react';
import Button from "@material-ui/core/Button";
import {useHistory} from "react-router";

Page404.propTypes = {};

function Page404(props) {
    const history = useHistory();
    return (
        <div style={{
            flexDirection: "column",
            height: "100%",
            alignItems: "center",
            display: "flex",
            justifyContent: "center"
        }}>
            <h1 style={{color: "red", fontFamily: "'IranSansFaNum'"}}>404</h1>
            <h4>صفحه مورد نظر یافت نشد</h4>
            <Button color={"primary"} variant={"outlined"} onClick={() => history.goBack()}>بازگشت</Button>
        </div>
    );
}

export default Page404;