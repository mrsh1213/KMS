import {makeStyles} from "@material-ui/core/styles";


export default makeStyles((theme) => ({
    root: {
        zIndex: 2,
        [theme.breakpoints.down('sm')]: {
            width: "100%",
        },
        [theme.breakpoints.up('md')]: {
            display: "none",
            width: "calc(100% - 260px)",
        },
        // minWidth: "89%",
        flexGrow: 1,
        direction: "ltr",
        position: "fixed",
        bottom: 0,
        overflow: "hidden",
    },
    tabs: {
        backgroundColor: "#ffffff"
    },
    tab: {
        [theme.breakpoints.down('sm')]: {
            padding: 0,
            fontSize: 10,
            width: 48,
            height: 17,
            fontWeight: 500,
            fontStretch: "normal",
            fontStyle: "normal",
            lineHeight: 1.7,
            letterSpacing: "normal",
            textAlign: "right",
        }
    },
    iconsTab: {
        [theme.breakpoints.down('sm')]: {
            fontSize: "24px",
            color: "#c0c7d1"
        },
        [theme.selected]:{
            color:"#2684fe"
        }
    }
}));
