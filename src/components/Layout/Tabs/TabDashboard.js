import React, {useEffect} from 'react';
import {useHistory} from "react-router";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {GoFileSubmodule} from "react-icons/go";
import {MdForum, MdHome} from "react-icons/md";
import {GiBookmarklet} from 'react-icons/gi';
import Style from "./style";

TabDashboard.propTypes = {};

function TabDashboard(props) {
    const {selected} = props;
    const history = useHistory();
    const classes = Style();
    const [value, setValue] = React.useState(selected);

    useEffect(() => {
        setValue(selected);
    }, [selected])
    const handleChange = (event, newValue) => {
        setValue(newValue);
        switch (newValue) {
            case 0:
                history.push("/dashboard");
                break;
            case 1:
                history.push("/dashboard/wiki")
                break;
            case 2:
                history.push("/dashboard/posts")
                break;
            case 3:
                history.push("/dashboard/documents?currentPath=/")
                break;
            default:
                history.push("/dashboard")
        }

    };

    return (
        <Paper square={true} className={classes.root}>
            <Tabs
                className={classes.tabs}
                value={value}
                onChange={handleChange}
                variant="fullWidth"
                indicatorColor="secondary"
                textColor="secondary"
                aria-label="icon label tabs example"
            >
                <Tab className={classes.tab}
                     icon={<MdHome className={classes.iconsTab} style={value === 0 && {color: "#2684fe"}}/>}
                     label="صفحه اصلی"/>
                <Tab className={classes.tab}
                     icon={<GiBookmarklet className={classes.iconsTab} style={value === 1 && {color: "#2684fe"}}/>}
                     label="مقالات"/>
                <Tab className={classes.tab}
                     icon={<MdForum className={classes.iconsTab} style={value === 2 && {color: "#2684fe"}}/>}
                     label="گفتمان"/>
                <Tab className={classes.tab}
                     icon={<GoFileSubmodule className={classes.iconsTab} style={value === 3 && {color: "#2684fe"}}/>}
                     label="اسناد"/>
            </Tabs>
        </Paper>
    );
}

export default TabDashboard;