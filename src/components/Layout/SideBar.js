import React, {useRef} from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import {ExitToApp, Notifications} from "@material-ui/icons";
import {BsPersonBoundingBox} from "react-icons/bs";
import {useHistory, useLocation} from "react-router";
import Popover from "@material-ui/core/Popover";
import NotificationsList from "./NotificationsList";
import Badge from "@material-ui/core/Badge";
import {userService} from "../../config/constans";
import {BiArrowBack, GiBookmarklet, GoFileSubmodule, MdForum, MdHome} from "react-icons/all";
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";
import {Button} from "@material-ui/core";

const drawerWidth = 260;
const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('md')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        [theme.breakpoints.down('sm')]: {
            height: 56
        },
        [theme.breakpoints.up('md')]: {
            height: 56,
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
        boxShadow: '0 4px 16px 0 rgba(37, 56, 88, 0.1)'
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    // necessary for content to be below app bar
    topbar: {
        // backgroundColor: '#7467ef',
        backgroundColor: '#ffffff',
        minHeight: "100%",
        color: "#c0c7d1"
    },
    toolbar: {
        [theme.breakpoints.down('sm')]: {
            height: 89
        },
        [theme.breakpoints.up('md')]: {
            minHeight: "49px !important"
        },
        ...theme.mixins.toolbar
    },
    drawerPaper: {
        backgroundColor: '#ffffff',
        width: drawerWidth,
        [theme.breakpoints.down('sm')]: {
            width: "70%",
        },
    },
    cardAvatar: {
        "minHeight": "96px",
        "boxShadow": "0 1px 4px 0 rgba(37, 56, 88, 0.1)",
        "backgroundColor": "#fafbfd !important"
    }
    , avatar: {
        width: 64,
        height: 64
    },
    username: {
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.71",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#344563"
    },
    userRole: {
        "fontFamily": "'IranSans'",
        "fontSize": "10px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#c0c7d1"
    },
    content: {
        flexGrow: 1,
        paddingTop: theme.spacing(2),
    },
    drawerText: {
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.71",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#344563"
    },
    drawerIcon: {
        minWidth: 32,
        height: 24,
        color: "#c0c7d1",
        objectFit: "contain"
    },
    menuIcon: {
        fontSize: "24px",
        color: "#344563"
    },
    titleAppBar: {
        fontSize: "18px",
        color: "#344563",
        fontWeight: "500",
        fontStretch: "normal",
        fontStyle: "normal",
        lineHeight: "1.72",
        letterSpacing: "normal",
        textAlign: "left"
    },
    iconAppBar: {

        [theme.breakpoints.down('sm')]: {
            fontSize: "24px"
        },
        [theme.breakpoints.up('md')]: {
            fontSize: "24px"
        }


    },
    iconButtonAppBar: {
        backgroundColor: "#f5f7f9"
    },
    rootButton: {
        justifyContent: "flex-start"
    },
    copyRight: {
        "fontFamily": "'IranSans'",
        "fontSize": "8px",
        "fontWeight": "500",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#6b778c"
    },
    container: {height: "100%", display: "flex", flexDirection: "column"},
    sainaSub: {
        "fontFamily": "'IranSans'",
        "fontSize": "7px",
        "fontWeight": "600",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#455a64"
    },
    saina: {
        "fontFamily": "'IranSans'",
        "fontSize": "12px",
        "fontWeight": "600",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.54",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#0052cc"
    },
}));
const drawerRouter = [
    {text: 'صفحه اصلی', route: "/dashboard", icon: MdHome},
    {text: 'مقالات', route: "/dashboard/wiki", icon: GiBookmarklet},
    {text: 'گفتمان', route: "/dashboard/posts", icon: MdForum},
    {text: 'اسناد', route: "/dashboard/documents?currentPath=/", icon: GoFileSubmodule},
    // {text: 'تنظیمات', route: "/main/setting", icon: Settings},
    // {text: 'نشان شده ها', route: "/main/bookmarked", icon: Settings},
    {text: 'خروج از برنامه', route: "/main/exit", icon: ExitToApp},
];
// const listBackButton = ["projectManagement", "profile"];

function SideBar(props) {
    const {windows, title, backButton} = props;
    const classes = useStyles();
    // const {enqueueSnackbar} = useSnackbar();
    const theme = useTheme();
    const location = useLocation();
    const history = useHistory();
    const [mobileOpen, setMobileOpen] = React.useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [notifications] = React.useState([]);
    const ws = useRef(null);
    const user = JSON.parse(localStorage.getItem("user"));
    if (!user) {
        history.push("/signIn")
        return <span/>;
    }

    const handlePopoverOpen = (event) => {
        if (backButton) {
            history.goBack()
        } else {
            setAnchorEl(event.currentTarget);
        }
    };
    const handlePopoverClose = (event) => {
        setAnchorEl(null);
    };
    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    // function getAllNotifications() {
    //     setNotifications([]);
    //     let eventSource = new EventSource(API_SERVER + "/search/unReadBy/" + localStorage.getItem("user"));
    //     eventSource.addEventListener('message', (data) => {
    //         let vNotifications = JSON.parse(data.data);
    //         setNotifications(noti => [...noti, {
    //             id: vNotifications.id,
    //             type: vNotifications.type,
    //             message: vNotifications.message,
    //             backgroundColor: vNotifications.backgroundColor,
    //             time: vNotifications.time
    //         }]);
    //     });
    //     return eventSource;
    // }

    // useEffect(() => {
    //
    //     // let eventSource = getAllNotifications();
    //     //
    //     // eventSource.onerror = err => {
    //     //     eventSource.close();
    //     // }
    //     // return () => {
    //     //     eventSource.close();
    //     // }
    // }, []);
    // const sendMessage = (msg) => {
    //     // this.clientRef.sendMessage('/topics/all', msg);
    // }
    const container = windows !== undefined ? () => windows().document.body : undefined;

    // function handleMessage(msg) {
    //
    //     if (msg.type === "readNotification") {
    //         setNotifications(notifications.filter(notification => notification.id !== msg.id));
    //     } else {
    //         let audio = new Audio(AletrMp3);
    //         audio.play();
    //         if (window.navigator && window.navigator.vibrate) {
    //             window.navigator.vibrate([200, 80, 100]);
    //         }
    //         let eventSource = getAllNotifications();
    //         eventSource.onerror = err => {
    //             eventSource.close();
    //         }
    //     }
    // }
    function DrawerContent() {
        return (<div className={classes.container}>
            <Grid container>
                <Grid component={Button} onClick={() => {
                    history.push("/main/profile?uId=" + user.userId);
                    setMobileOpen(false)
                }} classes={{root: classes.rootButton}}
                      className={classes.cardAvatar} xs={12} item>
                    <div style={{display: "flex"}}>
                        <Avatar className={classes.avatar} alt="userProfile"
                                src={userService.getAvatarByUid(user.userId, 64, 64) + '&time=' + (new Date()).getMinutes()}>
                            <BsPersonBoundingBox/>
                        </Avatar>
                        <div style={{"paddingRight": "5px", "display": "flex", "alignItems": "center"}}>
                            <div>
                                <div className={classes.username}>{user.fullName}</div>
                                <div className={classes.userRole}>{user.jobTitle}</div>
                            </div>
                        </div>
                    </div>
                </Grid>
                <Grid xs={12} item>
                    <List>
                        {drawerRouter.map((item, index) => (
                            <ListItem key={item.text} button onClick={() => {
                                history.push(item.route);
                                setMobileOpen(false);
                            }}>
                                <ListItemIcon className={classes.drawerIcon}>
                                    <item.icon style={{fontSize: "24px"}}/>
                                </ListItemIcon>
                                <ListItemText primary={item.text}/>
                            </ListItem>
                        ))}
                    </List>
                </Grid>

            </Grid>
            <div style={{marginBottom: "auto"}}/>
            <Grid container style={{textAlign: "center", paddingBottom: 24}}>
                <Grid xs={12} item>
                    <img width={48} height={40} alt={"logoApp"} src="/icons/bgGray3x.png"/>
                </Grid>
                <Grid xs={12} item>
                    <div className={classes.saina}>ســـــاینا</div>
                    <div className={classes.sainaSub}>
                        <div> سیستم اشتراک‌گــــذاری</div>
                        <div>یافته‌هـای ناب اطلاعاتی</div>
                    </div>
                </Grid>
            </Grid>
        </div>)
    }

    return (
        <div className={classes.root}>
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar className={classes.topbar}>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        className={classes.menuButton}
                    >
                        <MenuIcon className={classes.menuIcon}/>
                    </IconButton>

                    <span className={classes.titleAppBar}>{title}</span>
                    <div style={{margin: "auto"}}/>
                    <div style={{textAlign: "center", float: "left"}}>
                        <div style={{textAlign: "left"}}>

                            <IconButton
                                className={classes.iconButtonAppBar}
                                color="inherit"
                                aria-label="open drawer"
                                edge="start"
                                onClick={handlePopoverOpen}
                            >
                                {backButton ? <BiArrowBack
                                    className={classes.iconAppBar}/> : <Badge anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }} color="error" badgeContent={notifications.length} max={99}>
                                    {notifications.length ?
                                        <Notifications
                                            className={classes.iconAppBar + " animate__swing  animate__animated animate__infinite"}/>
                                        : <Notifications
                                            className={classes.iconAppBar}/>}
                                </Badge>}
                            </IconButton>

                            <Popover
                                onClose={handlePopoverClose}
                                open={Boolean(anchorEl)}
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: 'center',
                                    horizontal: 'center',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                            >
                                <NotificationsList ws={ws} notifications={notifications}/>
                            </Popover>

                        </div>
                    </div>
                </Toolbar>
            </AppBar>
            <nav className={classes.drawer}>
                <Hidden smUp implementation="css">

                    <Drawer
                        container={container}
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'left' : 'right'}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        <DrawerContent/>
                    </Drawer>
                </Hidden>
                <Hidden smDown implementation="css">

                    <Drawer
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        variant="permanent"
                        open
                    >
                        <DrawerContent/>
                    </Drawer>
                </Hidden>
            </nav>

        </div>
    );
}

SideBar.propTypes = {
    windows: PropTypes.func,
    backButton: PropTypes.bool,
};


export default SideBar;