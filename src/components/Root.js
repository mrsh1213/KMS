import React, {useEffect} from 'react';
import {RootRouter} from "../routes";
import SideBar from "./Layout/SideBar";
import {withRouter} from 'react-router';
import {Route, Switch, useHistory} from "react-router";
import ScrollToTop from "./ScrollTop";
import {makeStyles} from "@material-ui/core/styles";
import Page404 from "./Layout/Page404";

const useStyles = makeStyles((theme) => ({
    content: {
        height: "100%",
        paddingLeft: 16,
        paddingRight: 16,
        [theme.breakpoints.up('md')]: {
            marginLeft: 260,
            paddingTop: 72,
        },
        paddingTop: 72,
        paddingBottom: 16,
    }
}));

const routerPath = {
    "/main/profile": {
        title: "پروفایل شخصی",
        id: 1
    },
    "/main/exit": {
        title: "خروج",
        id: 4
    },
    "/main/setting": {
        title: "تنظیمات",
        id: 4
    },
    "/main/createPost": {
        title: "ایجاد گفتگو",
        id: 2
    },
    "/main/post": {
        title: "نظرات",
        id: 2
    },
    "/main/createWiki": {
        title: "ایجاد مقاله",
        id: 11
    },
    "/main/listsWiki/new": {
        title: "جدیدترین مقالات",
        id: 12
    },
    "/main/listsWiki/seen": {
        title: "پر بازدیدترین مقالات",
        id: 13
    },
    "/main/wiki": {
        title: "مقاله",
        id: 14
    },
    "/main/createFolder": {
        title: "ایجاد پوشه",
        id: 15
    },
    "/main/createFile": {
        title: "ایجاد فایل",
        id: 16
    }
};

function Root(props) {
    const history = useHistory();
    const classes = useStyles();


    useEffect(() => {
        if (!localStorage.getItem("user")) {
            history.push("/signIn")
        }
    }, [props, history])

    if (!localStorage.getItem("user")) {
        return <span/>
    }
    return (
        routerPath[props.location.pathname] ?
            <>
                <SideBar backButton={true} title={routerPath[props.location.pathname].title}/>
                <div className={classes.content}>
                    <ScrollToTop>
                        <Switch>
                            {/*<Route path={"/dashboard/projectManagement"}>*/}
                            {/*    <ProjectManagement/>*/}
                            {/*</Route>*/}
                            {/*<Route path={"/dashboard/project"}>*/}
                            {/*    <ProjectRouter/>*/}
                            {/*</Route>*/}
                            <Route path={"/"}>
                                <RootRouter/>
                            </Route>
                        </Switch>
                    </ScrollToTop>
                </div>
                {/*<TabDashboard selected={routerPath[props.location.pathname].id}/>*/}
            </> : <Page404/>
    );
}

export default withRouter(Root);
