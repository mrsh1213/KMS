import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import API from "../../config/API";
import {fndService} from "../../config/constans";

SystemStatusHome.propTypes = {};
const useStyles = makeStyles((theme) => ({
        root: {
            textAlignLast: "center",
            fontFamily: "'IranSans'",
            "margin": "20px 0px",
            "padding": "30px 0px",
            "borderRadius": "50px",
            "boxShadow": "0 4px 16px 0 rgba(0, 0, 0, 0.16)",
            "backgroundImage": "linear-gradient(76deg, #0052cc 9%, #2684fe 92%)"
        },
        value: {
            fontFamily: "'IranSansFaNum'",
            "fontSize": "24px",
            "fontWeight": "bold",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.38",
            "letterSpacing": "normal",
            "textAlign": "right",
            "color": "#ffffff"
        },
        label: {
            fontFamily: "'IranSans'",
            "fontSize": "16px",
            "fontWeight": "500",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.56",
            "letterSpacing": "normal",
            "textAlign": "right",
            "color": "#ffffff"
        },
    }))
;

function ContentValue({classes, value, label}) {
    return <>
        <div className={classes.value}>{value}</div>
        <div className={classes.label}>{label}</div>
    </>
}

function SystemStatusHome(props) {
    const [status, setStatus] = useState({numberOfWiki: 0, numberOfUser: 0, numberOfFile: 0, numberOfPost: 0});
    const classes = useStyles();

    useEffect(() => {
        API({url: fndService.getSystemInfo()}).then(res => {
            if (res.status === 200) {
                setStatus(res.data)
            }
        });
    }, []);

    return (
        <Grid container className={classes.root}>
            <Grid xs={3} item>
                <ContentValue label={"مقاله"} value={status.numberOfWiki} classes={classes}/>
            </Grid>
            <Grid xs={3} item>
                <ContentValue label={"گفتگو"} value={status.numberOfPost} classes={classes}/>
            </Grid>
            <Grid xs={3} item>
                <ContentValue label={"سند"} value={status.numberOfFile} classes={classes}/>
            </Grid>
            <Grid xs={3} item>
                <ContentValue label={"عضو"} value={status.numberOfUser} classes={classes}/>
            </Grid>
        </Grid>
    );
}

export default SystemStatusHome;