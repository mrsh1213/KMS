import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import {userService} from "../../config/constans";
import Avatar from "@material-ui/core/Avatar";
import {ListItem} from "@material-ui/core";
import {useHistory} from "react-router";
import {FaUserCircle} from "react-icons/fa";

ProfileItem.propTypes = {
    data: PropTypes.object
};
const styles = makeStyles((theme) => ({
        cardItem: {
            "minWidth": "343px",
            "minHeight": "80px",
            "margin": "16px 0 12px",
            "padding": "8px 24px 8px 14px",
            "borderRadius": "10px",
            "boxShadow": "0 4px 16px 0 rgba(37, 56, 88, 0.1)",
            "backgroundColor": "#ffffff"
        },
        avatar: {
            minWidth: 64,
            minHeight: 64,
        },
        fullName: {
            "fontFamily": "'IranSans'",
            "fontSize": "14px",
            "fontWeight": "normal",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.57",
            "letterSpacing": "normal",
            "color": "#344563"
        },
        role: {
            "fontFamily": "'IranSans'",
            "fontSize": "10px",
            "fontWeight": "normal",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.5",
            "letterSpacing": "normal",
            "color": "#c0c7d1"
        }
    }
));

function ProfileItem(props) {
    const history = useHistory();
    const classes = styles();
    const {userId, jobTitle, fullName} = props.data;

    function handleToProfile() {
        history.push("/main/profile?uId=" + userId);
    }

    return (
        <Grid component={ListItem} button onClick={handleToProfile} container className={classes.cardItem}>
            <Grid xs={3} item>
                <Avatar className={classes.avatar} alt="userProf"
                        src={userService.getAvatarByUid(userId, 64, 64)}><FaUserCircle
                    className={classes.avatar}/></Avatar>
            </Grid>
            <Grid alignContent={"center"} container xs={6} item>
                <Grid className={classes.fullName} xs={12} item>{fullName}</Grid>
                <Grid className={classes.role} xs={12} item>{jobTitle}</Grid>
            </Grid>
            <Grid xs={3} item/>
        </Grid>
    );
}

export default ProfileItem;