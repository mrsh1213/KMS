import React, {useCallback, useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import Autocomplete from "@material-ui/lab/Autocomplete";
import DevTextField from "../../components/common/DevTextField";
import {debounce} from "lodash";
import API from "../../config/API";
import {userService} from "../../config/constans";
import {makeStyles} from "@material-ui/styles";
import {fixNumbersToEn} from "../../config/Helper";

KnowScopeAutoComplete.propTypes = {
    handleGetKnowScope: PropTypes.func
};
const styles = makeStyles((theme) => ({
    titleLabel: {
        marginTop: 8,
        "fontFamily": "'IranSans'",
        "fontSize": "10px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#c0c7d1"
    },
    autocompleteInput: {
        "fontSize": "12px",

    },
}));

function KnowScopeAutoComplete(props) {
    const [knScope, setKnScope] = useState(null);
    const [keywordKnScope, setKeywordKnScope] = useState("");
    const [knScopeOptions, setKnScopeOptions] = useState([]);
    const clearBtn = useRef(null);
    const delayedQueryKeywordKnScope = useCallback(debounce(searchKn, 890), [keywordKnScope]);
    const {handleGetKnowScope} = props;
    const classes = styles();

    useEffect(() => {
        delayedQueryKeywordKnScope();
        return delayedQueryKeywordKnScope.cancel;
    }, [keywordKnScope, delayedQueryKeywordKnScope]);

    useEffect(() => {
        handleGetKnowScope(knScope);
    }, [knScope, handleGetKnowScope]);

    if (clearBtn.current && clearBtn.current.children[0] && clearBtn.current.children[0].children[0] && clearBtn.current.children[0].children[0].children[1] && clearBtn.current.children[0].children[0].children[1].children[0]) {
        clearBtn.current.children[0].children[0].children[1].children[0].addEventListener("click", () => {
            setKeywordKnScope("")
            setKnScope(null);
        });
    }


    function searchKn() {
        API({method: "GET", url: userService.getProjects(keywordKnScope)}).then(res => {
            if (res.status === 200) {
                setKnScopeOptions(res.data);
            } else {
                setKnScopeOptions([]);
            }
        })
    }

    return (
        <>
            <div className={classes.titleLabel}>حوزه دانشی</div>
            <Autocomplete
                noOptionsText={"موردی یافت نشد"}
                style={{height: 36, borderRadius: 5, backgroundColor: "#f4f5f7", fontSize: "12px"}}
                fullWidth
                id="knScopeOptions"
                classes={{inputRoot: classes.autocompleteInput}}
                onChange={(e, value) => {
                    setKnScope(value)
                }}
                ref={clearBtn}
                options={knScopeOptions}
                getOptionLabel={(option) => option.title}
                value={knScope}
                // closeIcon={TiDeleteOutline}
                renderInput={(params) => {
                    if (params.inputProps.value) {
                        setKeywordKnScope(params.inputProps.value)
                    }
                    console.log(params);
                    return (
                        <DevTextField {...params} inputProps={{
                            ...params.inputProps,
                            value: keywordKnScope,
                            onChange: (e) => {
                                setKeywordKnScope(fixNumbersToEn(e.target.value))
                            }
                        }} fullWidth
                                      value={keywordKnScope}
                                      placeholder="حوزه دانشی"/>
                    )
                }}
            />

        </>
    );
}

export default KnowScopeAutoComplete;