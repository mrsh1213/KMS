import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import DevTextField from "../../components/common/DevTextField";
import {makeStyles} from "@material-ui/styles";

AccessLevel.propTypes = {
    handleGetAccessLevel: PropTypes.func
};
const items = [
    {label: "گروه تخصصی", value: "TECHNICAL_GROUPS"},
    {label: "امور", value: "WORKS"},
    {label: "عمومی", value: "PUBLIC"}
];
const styles = makeStyles((theme) => ({

    titleAccessLevel: {
        "fontFamily": "'IranSans'",
        "fontSize": "10px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#c0c7d1"
    },
    notchedOutline: {
        borderStyle: "none"
    },
    inputSelect: {
        padding: "7px 3px 7px 3px",
    }
}));

function AccessLevel(props) {
    const [accessLevel, setAccessLevel] = useState("TECHNICAL_GROUPS");
    const {handleGetAccessLevel} = props;
    const classes = styles();
    useEffect(() => {
        handleGetAccessLevel(accessLevel);
    }, [accessLevel, handleGetAccessLevel])

    function onChangeAccessLevel(e) {
        setAccessLevel(e.target.value);
    }

    return (
        <>
            <div className={classes.titleAccessLevel}>{"سطح دسترسی"}</div>
            <DevTextField InputProps={{
                classes: {
                    notchedOutline: classes.notchedOutline,
                    input: classes.inputSelect,
                }
            }} fullWidth
                          onChange={onChangeAccessLevel}
                          value={accessLevel}
                          items={items}/>
        </>
    );
}

export default AccessLevel;