import React, {useCallback, useContext, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import Button from "@material-ui/core/Button";
import Autocomplete from "@material-ui/lab/Autocomplete";
import {inputDetectDirection, uniqueArrayObject} from "../../config/Helper";
import DevTextField from "../../components/common/DevTextField";
import Grid from "@material-ui/core/Grid";
import API from "../../config/API";
import {postsService} from "../../config/constans";
import {debounce} from "lodash";
import DevDialog from "../../components/common/DevDialog";
import {BackDropContext} from "../../config/BackDropContext";
import {MdPlaylistAdd} from "react-icons/md";
import {makeStyles} from "@material-ui/styles";

HashTagsAutoComplete.propTypes = {
    handleGetListHashTags: PropTypes.func
};
const styles = makeStyles((theme) => ({
    titleLabel: {
        marginTop: 8,
        "fontFamily": "'IranSans'",
        "fontSize": "10px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#c0c7d1"
    },
    buttonAddHashTags: {
        "height": "32px", "borderRadius": "5px", "backgroundColor": "#2684fe !important",
        color: "#fff",
    },
    buttonCancelHashTags: {
        marginLeft: "8px",
        "height": "32px", "borderRadius": "5px", "backgroundColor": "#e0e1e5 !important",
        color: "#6b6b6b",
    },
    addHashTag: {
        "fontSize": "10px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "textAlign": "right",
        "color": "#2684fe"
    },
    addLabelField: {
        "fontFamily": "'IranSans'",
        "height": "36px",
        "borderRadius": "5px",
        "backgroundColor": "#f4f5f7",
        "fontSize": "12px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.58",
        "letterSpacing": "normal",
    },
    inputRootAddLabelField: {
        '&::placeholder': {
            "fontSize": "12px !important",
        },
    }
}));

function HashTagsAutoComplete(props) {
    const [hashTagsOptions, setHashTagsOptions] = useState([]);
    const [textSearchHashTag, setTextSearchHashTag] = useState("");
    const [openInsertHashTags, setOpenInsertHashTags] = useState(false);
    const [customHashTags, setCustomHashTags] = useState("");
    const [hashTags, setHashTags] = useState([]);
    const {dispatch} = useContext(BackDropContext);
    const {handleGetListHashTags} = props;
    const classes = styles();

    const delayedQueryKeywordHashTags = useCallback(debounce(searchHashTags, 890), [textSearchHashTag]);

    useEffect(() => {
        handleGetListHashTags(hashTags);
    }, [hashTags, handleGetListHashTags])

    useEffect(() => {
        delayedQueryKeywordHashTags();
        return delayedQueryKeywordHashTags.cancel;
    }, [textSearchHashTag, delayedQueryKeywordHashTags]);

    function handleAddHashTags() {
        let hasTagsReplaced = customHashTags.replace(/,|،/g, ",");
        let hashTagList = hasTagsReplaced.split(",").map(hashTagName => ({
            hashTagName: "#" + hashTagName.trim()
        }));
        if (hashTagList.length > 0) {
            API({data: {hashTagList}, method: "POST", url: postsService.postHashTag()}, dispatch).then(res => {
                if (res.status === 201) {
                    setHashTags(prevState => (uniqueArrayObject([...prevState, ...res.data], "hashTagId", "hashTagName")));
                    setOpenInsertHashTags(false);
                    setCustomHashTags([]);
                }
            })
        } else {
            setOpenInsertHashTags(false);
        }

    }

    function searchHashTags() {
        if (textSearchHashTag) {
            API({method: "GET", url: postsService.getHashTag(textSearchHashTag)}).then(res => {
                if (res.status === 200) {
                    setHashTagsOptions(res.data);
                } else {
                    setHashTagsOptions([]);
                }
            })
        }
    }

    return (
        <>
            <DevDialog handleClose={() => {
                setOpenInsertHashTags(false)
            }} open={openInsertHashTags}>
                <Grid spacing={2} container>
                    <Grid xs={12} item>
                        <div style={{color: "#42516e"}} className={classes.titleLabel}>{"برچسب دلخواه"}</div>
                        <DevTextField
                            error={true}
                            helperText={customHashTags.includes("#") ? "شما مجاز به استفاده از کاراکتر # نیستید" : undefined}
                            InputProps={{
                                classes: {input: classes.inputRootAddLabelField}
                            }} fullWidth placeholder={"برچسب مورد نظر خود را وارد نمایید و با کاما جدا کنید"}
                            className={classes.addLabelField} value={customHashTags} onChange={(e) => {
                            inputDetectDirection(e.target);
                            setCustomHashTags(e.target.value)
                        }}/>
                    </Grid>
                    <Grid style={{textAlign: "center"}} xs={12} item>
                        <Button disabled={customHashTags.includes("#")} onClick={handleAddHashTags}
                                className={classes.buttonAddHashTags}>افزودن</Button>
                        <Button onClick={() => {
                            setOpenInsertHashTags(false);
                            setCustomHashTags("")
                        }} className={classes.buttonCancelHashTags}>انصراف</Button>
                    </Grid>
                </Grid>
            </DevDialog>
            <div style={{display: "flex"}}>
                <div className={classes.titleLabel}>{"برچسب‌ها"}</div>
                <div style={{marginRight: "auto"}}/>
                <div><Button onClick={() => {
                    setOpenInsertHashTags(true)
                }} size={"small"} className={classes.addHashTag}>
                    <MdPlaylistAdd
                        style={{fontSize: 16, paddingLeft: 2}}/>{"افزودن برچسب جدید"}
                </Button>
                </div>
            </div>
            <Autocomplete
                noOptionsText={"موردی یافت نشد"}
                style={{height: 36, borderRadius: 5, backgroundColor: "#f4f5f7"}}
                multiple
                id="size-small-outlined-multi"
                size="small"
                fullWidth
                onChange={(e, values) => {
                    setHashTags(prevState => (uniqueArrayObject(values, "hashTagId", "hashTagName")))
                }}
                options={hashTagsOptions}
                getOptionLabel={(option) => option.hashTagName}
                value={hashTags}
                // closeIcon={TiDeleteOutline}
                renderInput={(params) => {

                    if (params.inputProps.value) {
                        setTextSearchHashTag(params.inputProps.value)
                    }
                    return (
                        <DevTextField {...params}
                            //               inputProps={{
                            //     ...params.inputProps,
                            //     value: textSearchHashTag,
                            //     onChange: (e) => {
                            //         setTextSearchHashTag(fixNumbersToEn(e.target.value))
                            //     }
                            // }}
                                      value={textSearchHashTag} error={hashTags.length === 0}
                                      helperText={hashTags.length === 0 && "حداقل یک برچسب باید انتخاب شود"}
                                      fullWidth
                                      placeholder="جستجو برچسب"/>
                    )
                }}
            />

        </>
    );
}

export default HashTagsAutoComplete;