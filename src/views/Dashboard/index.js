import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import DevSearchBox from "../../components/common/DevSearchBox";
import KmStatusMe from "./KmStatusMe";
import SystemStatus from "./SystemStatus";
import KnowledgeTypeRadio from "./KnowledgeTypeRadio";
import {FiAtSign} from "react-icons/fi";
import SearchResult from "./SearchResult";
import {GoFileSubmodule} from "react-icons/go";
import {MdForum} from "react-icons/md";
import {GiBookmarklet} from 'react-icons/gi';

Index.propTypes = {};


function IconSwitcher(type) {
    switch (type) {
        case 0:
            return FiAtSign;
        case 1:
            return GiBookmarklet;
        case 2:
            return MdForum;
        case 3:
            return GoFileSubmodule;
        default:
            return undefined;
    }
}

function Index() {
    const [knowledgeTypeRadio, setKnowledgeTypeRadio] = useState(1);
    const [keyword, setKeyword] = useState("");
    const [focus, setFocus] = useState(false);

    function searchGlobal(keywordText) {
        setKeyword(keywordText);
    }

    function handleChangeKnowledgeTypeRadio(value) {
        setKnowledgeTypeRadio(value);
    }

    return (
        <Grid style={{
            height: "100%",
            overflow: "auto",
            alignContent: "baseline"
        }} container>
            <Grid xs={12} item>
                <DevSearchBox onBlur={(e) => {
                    setFocus(false)
                }} onFocus={(e) => {
                    setFocus(true)
                }} Icon={IconSwitcher(knowledgeTypeRadio)} searchMethod={searchGlobal}/>
            </Grid>
            <Grid xs={12} item>
                <KnowledgeTypeRadio handleChangeRadio={handleChangeKnowledgeTypeRadio}/>
            </Grid>
            {focus || keyword ?
                <Grid style={{
                    height: "100%",
                    overflow: "auto",
                    alignContent: "baseline"
                }} container xs={12} item>
                    <SearchResult keyword={keyword} typeSearch={knowledgeTypeRadio}/>
                </Grid> : <>
                    <Grid xs={12} item>
                        <KmStatusMe userId={JSON.parse(localStorage.getItem("user")).userId}/>
                    </Grid>
                    <Grid xs={12} item>
                        <SystemStatus/>
                    </Grid>
                </>}


        </Grid>
    );
}

export default (Index);