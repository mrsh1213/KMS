import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/styles";
import {Button} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import API from "../../config/API";
import {userService} from "../../config/constans";

KmStatusMe.propTypes = {};
const styles = makeStyles((theme) => ({
    card: {
        "width": "100%",
        "height": props => (!props.inProfile ? "20vh" : 104),
        "borderRadius": "10px",
        "boxShadow": props => (props.inProfile ? undefined : "0 4px 16px 0 rgba(69, 91, 99, 0.08)"),
        "backgroundColor": props => (props.inProfile ? "#ffffff" : "#2684fe"),
        marginTop: props => (props.inProfile ? undefined : 8),
        marginBottom: 8
    },
    title: {
        alignSelf: "center",
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.71",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": props => (props.inProfile ? "#c0c7d1" : "#ffffff")
    },
    value: {
        "fontFamily": "'IranSansFaNum'",
        "fontSize": "32px",
        "fontWeight": "bold",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.69",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": props => (props.inProfile ? "#42516e" : "#ffffff")
    },
    textLabelContainer: {
        borderRight: props => (props.inProfile ? "solid 1px #e0e1e5" : "solid 1px #b3d4ff"),
        height: 80
    },
    label: {
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.71",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": props => (props.inProfile ? "#e0e1e5" : "#b3d4ff")
    }
}));

function TextLabel({value, label, classes}) {

    return (
        <Button style={{width: "100%"}}>
            <div>
                <div className={classes.value}>{value}</div>
                <div className={classes.label}>{label}</div>
            </div>
        </Button>
    )
}

function KmStatusMe(props) {
    const {inProfile, userId} = props;
    const classes = styles(props);
    const [status, setStatus] = useState({
        "score": 0,
        "docCount": 0,
        "postsCount": 0,
        "wikiCount": 0
    });
    useEffect(() => {
        API({url: userService.getStatusUserByUid(userId)}).then(res => {
            if (res.status === 200) {
                setStatus(res.data)
            }
        });
    }, [userId]);
    return (
        <Grid alignItems={"center"} container className={classes.card}>
            {!inProfile && <Grid className={classes.title} xs={12} item>
                وضعیت شما در سیستم مدیریت دانش
            </Grid>}
            <Grid xs={3} item>
                <TextLabel classes={classes} value={status.wikiCount} label={"مقاله"}/>
            </Grid>
            <Grid className={classes.textLabelContainer} xs={3} item>
                <TextLabel classes={classes} value={status.postsCount} label={"گفتگو"}/>
            </Grid>
            <Grid className={classes.textLabelContainer} xs={3} item>
                <TextLabel classes={classes} value={status.docCount} label={"سند"}/>
            </Grid>
            <Grid className={classes.textLabelContainer} xs={3} item>
                <TextLabel classes={classes} value={status.score} label={"امتیاز"}/>
            </Grid>
        </Grid>
    );
}

export default KmStatusMe;