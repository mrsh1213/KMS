import React, {useCallback, useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/styles";
import {GiBookmarklet} from 'react-icons/gi';
import {MdForum} from "react-icons/md";
import {GoFileSubmodule} from "react-icons/go";
import {FaUser} from "react-icons/fa";
import Button from "@material-ui/core/Button";
import API from "../../config/API";
import {fndService} from "../../config/constans";
import {useHistory} from "react-router";

SystemStatus.propTypes = {};
const styles = makeStyles((theme) => ({
    title: {
        "fontFamily": "'IranSans'",
        "fontSize": "12px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.67",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#5d6c83",
        position: "absolute",
        paddingRight: 8,
        backgroundColor: "#fafbfd"
    },
    hr: {
        "width": "100%",
        marginTop: 5,
        "backgroundColor": "#fafbfd"
    },
    cardStatus: {
        "width": "100%",
        height: "16vh",
        "borderRadius": "10px",
        "boxShadow": "0 4px 16px 0 rgba(69, 91, 99, 0.08)",
        "backgroundColor": "#ffffff",
        alignItems: "center",
        textAlign: "center",
    },
    value: {
        "fontFamily": "'IranSansFaNum'",
        "fontSize": "32px",
        "fontWeight": "bold",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.69",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#5d6c83"
    },
    label: {
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.71",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#5d6c83"
    },
    iconCardStatus: {
        width: 48,
        height: 48
    }
}));

function CardStatus(props) {
    const {
        handleOnClick, classes, IconCard,
        value, label, color
    } = props;

    function handleRoute(label) {
        switch (label) {
            case "مقاله":
                handleOnClick("/dashboard/wiki");
                break;
            case "گفتگو":
                handleOnClick("/dashboard/posts");
                break;
            case "سند":
                handleOnClick("/dashboard/documents?currentPath=/");
                break;
            case "کاربر":
                handleOnClick("/dashboard");
                break;
            default:
                handleOnClick("/dashboard")
        }
    }

    return (
        <Grid component={Button} onClick={() => {
            handleRoute(label)
        }} container className={classes.cardStatus}>
            <Grid xs={6} item>
                <IconCard style={{color: color}} className={classes.iconCardStatus}/>
            </Grid>
            <Grid xs={6} container item>
                <Grid className={classes.value} xs={6} item>{value}</Grid>
                <Grid xs={6} item/>
                <Grid className={classes.label} xs={6} item>{label}</Grid>
                <Grid xs={6} item/>
            </Grid>
        </Grid>);
}

function SystemStatus(props) {
    const [status, setStatus] = useState({numberOfWiki: 0, numberOfUser: 0, numberOfFile: 0, numberOfPost: 0});
    const history = useHistory();

    useEffect(() => {
        API({url: fndService.getSystemInfo()}).then(res => {
            if (res.status === 200) {
                setStatus(res.data)
            }
        });
    }, []);
    const handleOnClick = useCallback((route) => {
        history.push(route);
    }, [history]);
    const classes = styles();
    return (
        <Grid container>
            <Grid container xs={12} item>
                <span className={classes.title}>وضعیت کلی سیستم</span>
                <hr className={classes.hr}/>
            </Grid>
            <Grid container xs={12} item>
                <Grid xs={6} style={{paddingLeft: 4, paddingBottom: 8}} item>
                    <CardStatus handleOnClick={handleOnClick} value={status.numberOfWiki} label={"مقاله"}
                                color={"#c0b6f2"}
                                IconCard={GiBookmarklet}
                                classes={classes}/>
                </Grid>
                <Grid xs={6} style={{paddingRight: 4, paddingBottom: 8}} item>
                    <CardStatus handleOnClick={handleOnClick} value={status.numberOfPost} label={"گفتگو"}
                                color={"#b2f5fe"}
                                IconCard={MdForum}
                                classes={classes}/>
                </Grid>
                <Grid xs={6} style={{paddingLeft: 4}} item>
                    <CardStatus handleOnClick={handleOnClick} value={status.numberOfFile} label={"سند"}
                                color={"#feefb2"}
                                IconCard={GoFileSubmodule}
                                classes={classes}/>
                </Grid>
                <Grid xs={6} style={{paddingRight: 4}} item>
                    <CardStatus handleOnClick={handleOnClick} value={status.numberOfUser} label={"کاربر"}
                                color={"#abf5d2"}
                                IconCard={FaUser}
                                classes={classes}/>
                </Grid>

            </Grid>
        </Grid>
    );
}

export default SystemStatus;