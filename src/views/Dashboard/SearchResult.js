import React, {useCallback, useContext, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import ListItemWiki from "../Wiki/ListItemWiki";
import InfiniteScroll from "react-infinite-scroll-component";
import PostCard from "../Posts/PostCard";
import {BackDropContext} from "../../config/BackDropContext";
import API from "../../config/API";
import {searchService} from "../../config/constans";
import ProfileItem from "../common/ProfileItem";

SearchResult.propTypes = {
    typeSearch: PropTypes.number,
    keyword: PropTypes.string
};

function SearchResult(props) {
    const [results, setResults] = useState({content: [], number: -1, totalElements: 0});
    const {dispatch} = useContext(BackDropContext);
    const {typeSearch, keyword} = props;


    const getServiceSearch = useCallback((typeSearch, keyword, page, size, userId) => {
        switch (typeSearch) {
            case 0:
                return searchService.searchUsers(encodeURIComponent(keyword), page, size);
            case 1:
                return searchService.searchWiki(encodeURIComponent(keyword), page, size, userId);
            case 2:
                return searchService.searchPost(keyword, page, size, userId);
            case 3:
                return;
            default:
                return null;
        }
    }, []);

    function ComponentType({typeSearch, data}) {
        switch (typeSearch) {
            case 0:
                return (<Grid key={data.userId} xs={12} item>
                    <ProfileItem data={data}/>
                </Grid>);
            case 1:
                return (
                    <Grid key={data.wikiId} xs={12} item>
                        <ListItemWiki wiki={data}/>
                    </Grid>);
            case 2:
                return (
                    <Grid key={data.postId} xs={12} item>
                        <PostCard showComments={false} post={data}/>
                    </Grid>);
            case 3:
                return <div>file</div>;
            default:
                return null;
        }
    };
    const search = useCallback((page, keyword) => {
        API({
            method: "GET",
            url: getServiceSearch(typeSearch, encodeURIComponent(keyword), page, 10, localStorage.getItem("userId"))
        }, dispatch).then(res => {
            if (res.status === 200) {
                setResults(prevState => ({...res.data, content: [...prevState.content, ...res.data.content]}));
            } else if (res.status === 204) {
                setResults({content: [], number: -1, totalElements: 0})
            }
        }).catch(() => {
            setResults({content: [], number: -1, totalElements: 0})

        });
    }, [dispatch, typeSearch]);

    const fetchMoreData = useCallback((page) => {
        console.log("page ", page);
        search(page, keyword)
    }, [dispatch, keyword]);

    useEffect(() => {
        setResults({content: [], number: -1, totalElements: 0});
        if (keyword) {
            search(0, keyword);
        }
    }, [keyword, typeSearch, setResults]);

    useEffect(() => {
        let id = setTimeout(() => {
            document.documentElement.scrollTop = 0
        }, 1000);
        return () => {
            clearTimeout(id);
        }
    }, []);
    return (
        <InfiniteScroll
            initialScrollY={100}
            height={"100%"}
            style={{minWidth: "300px"}}
            dataLength={results.content.length} //This is important field to render the next data
            next={() => {
                fetchMoreData(results.number + 1)
            }}
            hasMore={!results.last}
            loader={results.content.length > 0 && <div style={{textAlign: "center",}}><CircularProgress/></div>}
        >
            {results.content.map(data => {
                    if (data) {
                        return (<ComponentType typeSearch={typeSearch} data={data}/>)
                    } else {
                        return "";
                    }
                }
            )}
        </InfiniteScroll>);
}

export default SearchResult;