import React, {useState} from 'react';
import Chip from "@material-ui/core/Chip";
import {makeStyles} from "@material-ui/styles";
import PropTypes from "prop-types";

KnowledgeTypeRadio.propTypes = {handleChangeRadio: PropTypes.func};
const styles = makeStyles((theme) => ({
        container: {
            marginTop: 8,
            marginBottom: 8,
            display: "flex"
        },
        proSearch: {
            "fontFamily": "'IranSans'",
            "fontSize": "10px",
            "fontWeight": "normal",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.7",
            "letterSpacing": "normal",
            "color": "#2684fe",
            textAlign: "right",
            width: "100%"
        },
        chip: {
            fontFamily: "'IranSans'",
            width: '100%',
            height: '100%',
            maxHeight: 20,
            maxWidth: 56,
            marginRight: 7,
            fontSize: 10,
            fontWeight: "normal",
            fontStretch: "normal",
            fontStyle: "normal",
            lineHeight: 1.7,
            letterSpacing: "normal",
            borderRadius: 5,
            boxShadow: "0 2px 8px 0 rgba(69, 91, 99, 0.08)",
            color: "#c0c7d1",
        }
    }
));

function KnowledgeTypeRadio(props) {
    const [value, setValue] = useState(1);
    const {handleChangeRadio} = props;
    const classes = styles();

    function handleChange(value) {
        setValue(value);
        handleChangeRadio(value);
    }

    return (<div className={classes.container}>


        <Chip className={classes.chip} clickable={value === 1} onClick={() => {
            handleChange(1)
        }} label="مقالات"
              variant={value === 1 ? "default" : "outlined"}
              color="secondary" size="small"/>
        <Chip className={classes.chip} clickable={value === 2} onClick={() => {
            handleChange(2)
        }} label="گفتمان"
              variant={value === 2 ? "default" : "outlined"}
              color="secondary" size="small"/>
        <Chip disabled className={classes.chip} clickable={value === 3} onClick={() => {
            handleChange(3)
        }} label="اسناد"
              variant={value === 3 ? "default" : "outlined"}
              color="secondary" size="small"/>
        <Chip className={classes.chip} clickable={value === 0} onClick={() => {
            handleChange(0)
        }} label="افراد"
              variant={value === 0 ? "default" : "outlined"}
              color={"secondary"} size="small"/>
        {/*<div className={classes.proSearch}>*/}
        {/*    <Button size={"small"}>*/}
        {/*        <div className={classes.proSearch}>جستجو پیشرفته</div>*/}
        {/*    </Button>*/}
        {/*</div>*/}
    </div>);
}

export default KnowledgeTypeRadio;