import React, {useCallback, useContext, useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import DevSearchBox from "../../components/common/DevSearchBox";
import PostCard from "./PostCard";
import API from "../../config/API";
import {postsService, searchService} from "../../config/constans";
import InfiniteScroll from 'react-infinite-scroll-component';
import CircularProgress from "@material-ui/core/CircularProgress";
import {BiCommentAdd} from 'react-icons/bi';
import Fab from "@material-ui/core/Fab";
import {useHistory} from "react-router";
import {BackDropContext} from "../../config/BackDropContext";
import {makeStyles} from "@material-ui/core/styles";

Index.propTypes = {};
const useStyles = makeStyles((theme) => ({
    container: {
        height: "100%",
        overflow: "auto",
        alignContent: "baseline"
    }
}))

function Index(props) {
    const [posts, setPosts] = useState({content: [], number: -1, totalElements: 0});
    const [keyword, setKeyword] = useState("");
    const {dispatch} = useContext(BackDropContext);
    const classes = useStyles();

    useEffect(() => {
        let id = setTimeout(() => {
            document.documentElement.scrollTop = 0
        }, 1000);
        return () => {
            clearTimeout(id);
        }
    }, []);
    const history = useHistory();
    useEffect(() => {
        setPosts({content: [], number: -1, totalElements: 0});
        if (keyword) {
            searchPosts(0, keyword);
        } else {
            fetchMoreData(0);
        }
    }, [keyword, setPosts]);
    const searchPosts = useCallback((page, keyword) => {
        API({
            method: "GET",
            url: searchService.searchPost(encodeURIComponent(keyword), page, 10, localStorage.getItem("userId"))
        }, dispatch).then(res => {
            if (res.status === 200) {
                setPosts(prevState => ({...res.data, content: [...prevState.content, ...res.data.content]}));
            } else if (res.status === 204) {
                setPosts({content: [], number: -1, totalElements: 0})
            }
        }).catch(() => {
            setPosts({content: [], number: -1, totalElements: 0})

        });
    }, [dispatch]);
    const fetchMoreData = useCallback((page) => {
        if (keyword.length === 0) {
            API({
                method: "GET",
                url: postsService.getPosts(page, 10, localStorage.getItem("userId"))
            }, dispatch).then(res => {
                if (res.status === 200) {
                    // setTimeout(() => {
                    setPosts(prevState => ({...res.data, content: [...prevState.content, ...res.data.content]}));
                    // }, 1000);
                } else if (res.status === 204) {
                    setPosts({content: [], number: -1, totalElements: 0})
                }
            })
        } else {
            searchPosts(page, keyword)
        }

    }, [dispatch, keyword]);

    function searchMethod(text) {
        setKeyword(text)
    }

    return (
        <Grid className={classes.container} container>
            <Grid xs={12} item>
                <DevSearchBox searchMethod={searchMethod}/>
            </Grid>
            <Grid xs={12} className={classes.container} container item>
                <InfiniteScroll
                    initialScrollY={200}
                    height={"100%"}
                    style={{minWidth: "275px"}}
                    dataLength={posts.content.length} //This is important field to render the next data
                    next={() => {
                        fetchMoreData(posts.number + 1)
                    }}
                    hasMore={!posts.last}
                    loader={posts.content.length > 0 && <div style={{textAlign: "center",}}><CircularProgress/></div>}
                >
                    {posts.content.map(post => {
                            if (post) {
                                return (<Grid key={post.postId} xs={12} item>
                                    <PostCard showComments={false} post={post}/>
                                </Grid>)
                            } else {
                                return "";
                            }
                        }
                    )}
                </InfiniteScroll>

            </Grid>
            <Fab
                style={{
                    position: "fixed",
                    bottom: 90,
                    left: 10,
                    textAlign: "left"
                }}
                onClick={() => {
                    history.push("/main/createPost")
                }}
                size={"large"}
                color={"primary"}
            >
                <BiCommentAdd style={{width: 24, height: 24}}/>
            </Fab>
        </Grid>
    );
}

export default Index;