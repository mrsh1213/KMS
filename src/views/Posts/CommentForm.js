import React, {useContext, useState} from 'react';
import PropTypes from 'prop-types';
import {Send} from "@material-ui/icons";
import InputBase from "@material-ui/core/InputBase";
import {makeStyles} from "@material-ui/styles";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import API from "../../config/API";
import {postsService, userService} from "../../config/constans";
import {BackDropContext} from "../../config/BackDropContext";
import {inputDetectDirection} from "../../config/Helper";

CommentForm.propTypes = {
    postId: PropTypes.string,
    handleCommentSent: PropTypes.func
};
const styles = makeStyles((theme) => ({
    container: {
        position: "fixed",
        bottom: 0,
        right: 0,
        width: "100%",

    },
    commentForm: {
        "minHeight": "50px !important",
        "WebkitBoxShadow": "0 -1px 3px rgba(0,0,0,0.2)",
        "boxShadow": "0 -1px 3px rgba(0,0,0,0.2)",
        "borderTop": "1px solid rgba(0,0,0,0.05)",
        width: "inherit",
        "height": "30px", "borderRadius": "5px", "backgroundColor": "#f4f5f7",
        marginTop: 8,
        paddingBottom: 3,
        paddingTop: 3,
        paddingLeft: 8,
        paddingRight: 8,
        display: "flex",
        flexDirection: "row",
        alignItems: "center"
    },
    avatar: {
        width: 36,
        height: 36
    },
    iconsSend: {
        color: "#2684fe",
        transform: "rotate(180deg)",
        width: 32,
        height: 32
    }
    ,
    inputRoot: {
        "minHeight": "50px !important",
        color: 'inherit',
        fontFamily: "'IranSans'",
        '&::placeholder': {
            "fontFamily": "'IranSans'",
            "fontSize": "3px",
            color: "red"
        },
    },
    inputInput: {
        '&:placeholder': {
            "fontFamily": "'IranSans'",
            "fontSize": "3px",
            color: "red"
        },
        marginLeft: "5px",
        "height": "inherit",
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "color": "#2f3136",
    },


}));

function CommentForm(props) {
    const [content, setContent] = useState("");
    const {dispatch} = useContext(BackDropContext);
    const classes = styles(props);
    const {postId, handleCommentSent} = props;
    const user = JSON.parse(localStorage.getItem("user"));

    function handleChange(e) {
        inputDetectDirection(e.target);
        setContent(e.target.value)
    }

    function sendComment() {
        if (content.trim()) {
            API({
                data: {
                    "content": content,
                    "postId": postId,
                    "commentedBy": localStorage.getItem("userId")
                }
                , method: "POST", url: postsService.postComment()
            }, dispatch).then(res => {
                setContent("")
                handleCommentSent();
            });
        } else {
            setContent("")
        }
    }

    return (
        <div className={classes.container}>
            <div className={classes.commentForm}>
                <Avatar src={userService.getAvatarByUid(user.userId, 64, 64)}
                        className={classes.avatar}/>
                <InputBase
                    value={content}
                    fullWidth
                    placeholder="نظر خود را بنویسید"
                    classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput
                    }}
                    inputProps={{classes: {input: classes.inputInput, root: classes.inputRoot}}}
                    onChange={handleChange}
                />
                <IconButton onClick={sendComment}><Send className={classes.iconsSend}/></IconButton>
            </div>
        </div>
    );
}

export default CommentForm;