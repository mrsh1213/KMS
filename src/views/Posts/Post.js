import React, {useCallback, useContext, useEffect, useState} from 'react';
import PostCard from "./PostCard";
import {useQuery} from "../../config/Helper";
import API from "../../config/API";
import {postsService} from "../../config/constans";
import {BackDropContext} from "../../config/BackDropContext";

Post.propTypes = {};

function Post(props) {
    const [post, setPost] = useState({postId: 0});
    const postId = useQuery().get("pId");
    const {dispatch} = useContext(BackDropContext);

    const fetchPost = useCallback(() => {
        API({
            method: "GET",
            url: postsService.getPostById(localStorage.getItem("userId"), postId)
        }, dispatch).then(res => {
            if (res.status === 200) {
                // setTimeout(() => {
                setPost(res.data);
                // }, 1000);
            }
        });
    }, [dispatch, postId]);
    useEffect(() => {
        fetchPost();
    }, [fetchPost]);
    return (
        post.postId ? <PostCard post={post} showComments={true}/> : ""
    );
}

export default Post;