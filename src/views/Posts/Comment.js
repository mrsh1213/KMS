import React, {useCallback, useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/styles";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Box from "@material-ui/core/Box";
import {AiFillLike} from "react-icons/ai";
import {MdDelete} from "react-icons/md";
import API from "../../config/API";
import {postsService, userService} from "../../config/constans";
import {detectDirection} from "../../config/Helper";
import {useHistory} from "react-router";
import moment from "moment";
import * as PropTypes from "prop-types";

Comment.propTypes = {deleteComment: PropTypes.func};
const styles = makeStyles((theme) => ({
    avatar: {
        width: 32,
        height: 32,
        marginRight: 5
    },
    actionIcon: {
        width: 18,
        height: 18,
        color: "#c0c7d1",
        marginRight: 4
    },
    actionButton: {},
    actionNum: {
        "fontFamily": "'IranSansFaNum'",
        "fontSize": "10px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.67",
        "letterSpacing": "normal",
        "textAlign": "right",
        "color": "#42516e"
    },
    more: {
        "fontFamily": "'IranSans'",
        "fontSize": "10px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.75",
        "letterSpacing": "normal",
        "textAlign": "right",
        "color": "#2684fe"
    },
    header: {
        display: "flex",
        "fontFamily": "'IranSans'",
        "fontSize": "10px",
        "fontWeight": "600",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.75",
        "letterSpacing": "normal",
        "textAlign": "right",
        "color": "#42516e"
    },
    createDate: {
        "fontFamily": "'IranSans'",
        "fontSize": "8px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#b2b9c3"
    },
    content: {
        "fontFamily": "'IranSans'!important",
        "fontSize": "12px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.75",
        "letterSpacing": "normal",
        "textAlign": "justify",
        "color": "#42516e"
    },
    comment: {
        backgroundColor: "#f4f5f7",
        padding: "1px 8px 8px 8px",
        borderRadius: '1px 8px 8px 8px'
    },
    container: {
        marginTop: 12,
    }

}));

function Comment(props) {
    const {index, deleteComment, comment} = props;
    const {commentId, content, commentedBy, liked, numberOfLikes, wroteAt} = comment;
    const classes = styles(props);
    const [more, setMore] = useState(260);
    const [showLiked, setLiked] = useState(liked);
    // const [showNumberOfComments,] = useState(numberOfComments);
    const [showNumberOfLikes, setNumberOfLikes] = useState(numberOfLikes);
    const [userInfo, setUserInfo] = useState({});
    const history = useHistory();
    useEffect(() => {
        getUser(commentedBy);
    }, [commentedBy]);

    function getUser(Uid) {
        API({url: userService.getInfoByUid(Uid)}).then(res => {
            setUserInfo(res.data);
        });
    }

    const handleDeleteComment = useCallback(() => {
        API({
            method: "DELETE",
            url: postsService.deleteCommentByCommentId(commentId),
        }).then(res => {
            if (res.status === 200) {
                deleteComment(index)
            }
        })
    }, [index, commentId, deleteComment]);
    const handleLike = useCallback(() => {
        API({
            method: "PUT",
            url: postsService.putLikeCommentByCommentId(commentId, localStorage.getItem("userId"))
        }).then(res => {
            if (res.status === 200) {
                setLiked(prevState => !prevState);
                setNumberOfLikes(res.data);
            }
        })
    }, [commentId]);
    return (
        <div className={classes.container}>

            <div>
                <div style={{display: "flex"}}>
                    <Avatar
                        onClick={() => {
                            history.push("/main/profile?uId=" + userInfo.userId);
                        }}
                        src={userService.getAvatarByUid(userInfo.userId, 64, 64)}
                        className={classes.avatar}/>
                    <div style={{width: "100%", display: "flex", flexDirection: "column"}}>
                        <div className={classes.comment}>

                            <div className={classes.header}>
                                <div className={classes.fullName}>{userInfo.fullName}</div>
                                <div style={{marginRight: "auto"}}/>
                                <div className={classes.createDate}>{moment(new Date(wroteAt)).fromNow()}</div>

                            </div>
                            <div dir={detectDirection(content)}
                                 className={classes.content}>{more >= content.length ? content : content.substr(0, more)} {more <= content.length ?
                                <span onClick={() => {
                                    setMore(prevState => prevState + content.length)
                                }} className={classes.more}>مشاهد بیشتر...</span> :
                                content.length >= 260 && <span onClick={() => {
                                    setMore(260)
                                }} className={classes.more}>بستن</span>}</div>
                        </div>
                        <div style={{display: "flex"}}>
                            <Box style={{marginLeft: 5}}>
                                <IconButton onClick={handleLike} className={classes.actionButton} size={"small"}>
                                    <AiFillLike style={showLiked && {color: "#2684fe"}} className={classes.actionIcon}/>
                                </IconButton>
                                <span className={classes.actionNum}>{showNumberOfLikes}</span>
                            </Box>
                            {/*<Box>*/}
                            {/*    <IconButton className={classes.actionButton} size={"small"}>*/}
                            {/*        <ImReply className={classes.actionIcon}/>*/}
                            {/*    </IconButton>*/}
                            {/*    <span className={classes.actionNum}>{showNumberOfComments}</span>*/}
                            {/*</Box>*/}
                            <div style={{marginLeft: "auto"}}/>
                            {JSON.parse(localStorage.getItem("user")).userId === commentedBy ? <Box>
                                <IconButton onClick={handleDeleteComment} className={classes.actionButton}
                                            size={"small"}>
                                    <MdDelete className={classes.actionIcon}/>
                                </IconButton>
                            </Box> : null}
                        </div>
                    </div>
                </div>
            </div>


        </div>
    );
}

export default Comment;