import React, {useCallback, useContext, useRef, useState} from 'react';
import Avatar from "@material-ui/core/Avatar";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/styles";
import {Send} from "@material-ui/icons";
import DevTextField from "../../components/common/DevTextField";
import {FcAddImage, FcRemoveImage} from "react-icons/fc";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import {inputDetectDirection} from "../../config/Helper";
import API from "../../config/API";
import {postsService, userService} from "../../config/constans";
import {useSnackbar} from "notistack";
import {useHistory} from "react-router";
import {BackDropContext} from "../../config/BackDropContext";
import HashTagsAutoComplete from "../common/HashTagsAutoComplete";
import KnowScopeAutoComplete from "../common/KnowScopeAutoComplete";
import AccessLevel from "../common/AccessLevel";
import DevImageCrop from "../../components/common/DevImageCrop";


PostCreate.propTypes = {};
const styles = makeStyles((theme) => ({
    titleLabel: {
        marginTop: 8,
        "fontFamily": "'IranSans'",
        "fontSize": "10px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#c0c7d1"
    },
    card: {
        overflow: "auto",
        height: "100%",
        padding: 16,
        "borderRadius": "10px", "boxShadow": "0 4px 16px 0 rgba(37, 56, 88, 0.1)", "backgroundColor": "#ffffff"
    },
    username: {
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.71",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#344563"
    },
    userRole: {
        "fontFamily": "'IranSans'",
        "fontSize": "10px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#c0c7d1"
    },
    buttonRoot: {
        padding: 0
    },
    buttonText: {
        padding: 0
    },
    selectInput: {
        "fontFamily": "'IranSans'",
        "fontSize": "10px",

    },
    multiText: {
        marginTop: 8,
        marginBottom: 8,
    },
    autocomplete: {
        marginBottom: 8,
    },
    imageButton: {
        padding: 3,
    },
    buttonSave: {
        "height": "32px", "borderRadius": "5px", "backgroundColor": "#2684fe !important",
        color: "#fff",
    }
}));


function PostCreate(props) {
    const [accessLevel, setAccessLevel] = useState("TECHNICAL_GROUPS");
    const [text, setText] = useState("");
    const [hashTags, setHashTags] = useState([]);
    const [knScope, setKnScope] = useState(null);
    const [image, setImage] = useState(null);
    const [preview, setPreview] = useState(null);
    const [errors, setErrors] = useState({text: true, hashTags: true, image: true});
    const {dispatch} = useContext(BackDropContext);
    const {enqueueSnackbar} = useSnackbar();
    const history = useHistory();
    const classes = styles();
    const user = JSON.parse(localStorage.getItem("user"));
    const inputRef = useRef(null);

    function onChangeText(e) {
        inputDetectDirection(e.target);
        setText(e.target.value);
    }

    const handleGetAccessLevel = useCallback((selectedAccessLevel) => {
        setAccessLevel(selectedAccessLevel);
    }, [setAccessLevel]);
    const handleGetListHashTags = useCallback((selectedHashTags) => {
        setHashTags(selectedHashTags);
    }, [setHashTags]);
    const handleGetKnowScope = useCallback((selectedKnScope) => {
        setKnScope(selectedKnScope);
    }, [setKnScope])
    const getCropImage = useCallback((form, blob) => {
        setImage(form);
        setPreview(URL.createObjectURL(blob));
    }, []);

    function submitForm() {
        if (image && text && hashTags.length > 0) {
            API({
                data: image
                , method: "POST", url: postsService.postPostPhoto()
            }, dispatch).then(res => {
                if (res.status === 200 && res.data) {
                    API({
                        data: {
                            "content": text,
                            "isLiked": false,
                            "isBookmarked": false,
                            "hashTagsId": hashTags.map(hashTag => (hashTag.hashTagId)),
                            "projectCode": knScope ? knScope.title : null,
                            "permission": accessLevel,
                            "postPhotoId": res.data,
                            "numberOfLikes": 0,
                            "numberOfComments": 0,
                            "postedBy": user.userId
                        }
                        , method: "POST", url: postsService.postPost()
                    }, dispatch).then(res => {
                        if (res.status === 201) {
                            history.push("/dashboard/posts")
                        } else {
                            enqueueSnackbar("خطا در ثبت گفتمان رخ داده است", {
                                variant: "error", anchorOrigin: {
                                    vertical: 'bottom',
                                    horizontal: 'center',
                                }
                            });
                        }
                    });
                } else {
                    enqueueSnackbar(" حجم تصویر باید کمتر از 1Mb باشد", {
                        variant: "error", anchorOrigin: {
                            vertical: 'bottom',
                            horizontal: 'center',
                        }
                    });
                }

            });
        } else {
            enqueueSnackbar("تصویر و متن و برچسب گفتمان الزامی می باشد", {
                variant: "error", anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'center',
                }
            });

        }
    }

    function handleError(name, value) {
        setErrors(prevState => ({...prevState, [name]: value}))
    }

    return (
        <Grid alignContent={"flex-start"} container className={classes.card}>
            <Grid container xs={8} item>
                <Avatar alt="userProfilePost" src={userService.getAvatarByUid(user.userId, 64, 64)}/>
                <div style={{paddingRight: 5}}>
                    <div className={classes.username}>{user.fullName}</div>
                    <div className={classes.userRole}>{user.jobTitle}</div>
                </div>
            </Grid>
            <Grid container xs={4} item>
                <div style={{paddingRight: 5}}>
                    <AccessLevel handleGetAccessLevel={handleGetAccessLevel}/>
                </div>
            </Grid>
            <Grid xs={12} item>
                {/*<div className={classes.titleLabel}>متن</div>*/}
                <DevTextField
                    error={errors.text}
                    onBlur={() => {
                        handleError("text", !Boolean(text.length))
                    }}
                    helperText={errors.text ? " متن الزامی می باشد" : undefined}
                    className={classes.multiText}
                    rows={20}
                    fullWidth
                    multiline
                    placeholder={"متن خود را وارد کنید"}
                    value={text}
                    onChange={onChangeText}

                />
            </Grid>
            <Grid className={classes.autocomplete} xs={12} item>
                <HashTagsAutoComplete handleGetListHashTags={handleGetListHashTags}/>
            </Grid>
            <Grid style={{marginBottom: 12}} className={classes.autocomplete} xs={12} item>
                <KnowScopeAutoComplete handleGetKnowScope={handleGetKnowScope}/>
            </Grid>
            {preview && <Grid style={{padding: 10, margin: 10}} xs={12} item>
                <img alt={"preview"} style={{width: "100%", height: "100%"}}
                     src={preview}/>
            </Grid>}
            <Grid xs={8} item>
                {image ? <IconButton onClick={() => {
                    setImage(null);
                    setPreview(null)
                }} className={classes.imageButton}>
                    <span>
                       <FcRemoveImage/></span>
                </IconButton> : <>
                    <IconButton className={classes.imageButton} onClick={() => {
                        inputRef.current.click()
                    }}>
                    <span>

                         <FcAddImage/></span>
                    </IconButton>
                    <DevImageCrop aspect={16 / 9} inputRef={inputRef} uploadHandler={getCropImage}/></>
                }
            </Grid>
            <Grid xs={4} item>
                <Button onClick={submitForm} classes={{root: classes.buttonSave}} variant={"contained"} fullWidth>انتشار<Send
                    style={{marginRight: 16, transform: "rotate(180deg)"}}/></Button>
            </Grid>
        </Grid>
    );
}

export default PostCreate;