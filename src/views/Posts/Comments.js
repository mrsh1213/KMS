import React, {memo, useCallback, useContext, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import Comment from "./Comment";
import API from "../../config/API";
import {postsService} from "../../config/constans";
import InfiniteScroll from "react-infinite-scroll-component";
import {BackDropContext} from "../../config/BackDropContext";
import {uniqBy} from "lodash"

Comments.propTypes = {
    postId: PropTypes.string,
    getCount: PropTypes.func
};
const styles = makeStyles((theme) => ({
    container: {
        // overflow: "scroll",
        height: "100%"
    }
}));

function Comments(props) {
    const [comments, setComments] = useState({content: [], number: -1, totalElements: 0});
    const {dispatch} = useContext(BackDropContext);
    const classes = styles(props);
    const {postId, getCount, commentSent} = props;

    const fetchMoreData = useCallback((commentSent) => {

        API({
            method: "GET",
            url: postsService.getCommentsByPostId(localStorage.getItem("userId"), postId, (commentSent === "postId") ? comments.number + 1 : 0, 8)
        }, dispatch).then(res => {
            if (res.status === 200) {
                if (commentSent === "postId") {
                    setComments(prevState => ({
                        ...res.data,
                        content: uniqBy([...prevState.content, ...res.data.content], "commentId")
                    }));
                } else {
                    setComments(prevState => ({
                        ...prevState,
                        content: uniqBy([res.data.content[0], ...prevState.content], "commentId")
                    }))
                }
                getCount(res.data.totalElements)

            }
        })


    }, [postId, comments, dispatch, getCount]);
    const deleteComment = useCallback((index) => {

        let content = comments.content;
        content.splice(index, 1);
        setComments(prevState => ({
            totalElements: content.length,
            ...prevState, content: content
        }))
        getCount(content.length);
    }, [setComments, comments, getCount]);

    useEffect(() => {
        fetchMoreData("postId");
    }, [postId]);
    useEffect(() => {
        if (commentSent !== 1213) {
            fetchMoreData(commentSent);
        }
    }, [commentSent]);

    return (
        <Grid className={classes.container} container>
            <InfiniteScroll
                scrollThreshold={"100px"}
                // initialScrollY={200}
                scrollableTarget={"parentPost"}
                // height={"100%"}
                // style={{minWidth: "150px"}}
                // inverse={true}
                // initialScrollY={280}
                // hasChildren={false}
                dataLength={comments.content.length} //This is important field to render the next data
                next={() => fetchMoreData("postId")}
                hasMore={!comments.last}
                // loader={<div style={{textAlign: "center",}}><CircularProgress size={10}/></div>}
            >
                {comments.content.map((comment, index) => {
                    return (<Grid key={comment.commentId} xs={12} item>
                        <Comment deleteComment={deleteComment} index={index} comment={comment}/>
                    </Grid>)
                })}
            </InfiniteScroll>

        </Grid>
    );
}

export default memo(Comments);