import React, {useCallback, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from "@material-ui/styles";
import {Button} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";
import {AiFillLike} from "react-icons/ai";
import {MdComment} from "react-icons/md";
import {BsFillBookmarkFill} from "react-icons/bs";
import Chip from "@material-ui/core/Chip";
import Box from "@material-ui/core/Box";
import IconButton from "@material-ui/core/IconButton";
import {postsService, userService} from "../../config/constans";
import CommentForm from "./CommentForm";
import Comments from "./Comments";
import API from "../../config/API";
import {detectDirection} from "../../config/Helper";
import {useHistory} from "react-router";
import {AiTwotoneLock} from "react-icons/ai";
import {MdMoreVert} from "react-icons/md";
import {BiNetworkChart, MdPublic, TiGroup} from "react-icons/all";
import moment from "moment";

PostCard.propTypes = {
    post: PropTypes.object,
    showComments: PropTypes.bool,
};
const styles = makeStyles((theme) => ({
    card: {
        height: "91%",
        "width": "100%",
        "maxHeight": "91%",
        overflow: "auto",
        "minHeight": "375px",
        "borderRadius": "10px",
        "boxShadow": "0 4px 16px 0 rgba(69, 91, 99, 0.08)",
        "backgroundColor": "#ffffff",
        marginBottom: 16,
        padding: 10,
    },
    header: {
        height: 40,
        margin: "-6px",
        marginBottom: 18
    },
    username: {
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.71",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#344563"
    },
    content: {
        wordWrap: "break-word",
        "fontFamily": "'IranSans'",
        "fontSize": "12px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "textAlign": "justify",
        "color": "#42516e",
        marginBottom: 8
    },
    userRole: {
        "fontFamily": "'IranSans'",
        "fontSize": "9px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#c0c7d1"
    },
    more: {
        "fontFamily": "'IranSans'",
        "fontSize": "12px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.75",
        "letterSpacing": "normal",
        "textAlign": "right",
        "color": "#2684fe"
    },
    hashTag: {
        "fontFamily": "'IranSans'",
        height: 16,
        borderRadius: 5,
        backgroundColor: "#deebfe",
        "&:hover": {
            backgroundColor: "#deebfe",
        },
        "fontSize": "10px",
        "marginRight": "5px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.75",
        "letterSpacing": "normal",
        "textAlign": "right"
    },
    img: {
        "width": "100%",
        "height": "100%",
    },
    imgCard: {
        marginTop: 8,
        marginBottom: 5,
        "maxWidth": "100%",
        "minHeight": "200px",
        "objectFit": "contain",
        "borderRadius": "5px",
        "border": "solid 1px #f8f9fb"
    },
    footer: {
        marginTop: 8,
        "maxWidth": "320px",
        "maxHeight": "145px",
        "objectFit": "contain",
        "borderRadius": "5px",
        "border": "solid 1px #f8f9fb"
    },
    actionIcon: {
        width: 30,
        height: 30,
        color: "#c0c7d1",
        marginRight: 4
    },
    actionButton: {
        width: 34,
        height: 34,
    },
    avatar: {
        width: 48,
        height: 48,
    },
    actionNum: {
        "fontFamily": "'IranSansFaNum'",
        "fontSize": "12px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.67",
        "letterSpacing": "normal",
        "textAlign": "right",
        "color": "#42516e"
    }
}));


function Permission({permission}) {
    switch (permission) {
        case "TECHNICAL_GROUPS":
            return <span><TiGroup style={{fontSize: "10px"}}/>{" گروه تخصصی "}</span>
        case "WORKS":
            return <span><BiNetworkChart style={{fontSize: "10px"}}/>{" امور "}</span>

        case "PUBLIC":
            return <span><MdPublic style={{fontSize: "10px"}}/>{" عمومی "}</span>
        default:
            return <span>ناشناس</span>
    }
}

function PostCard(props) {
    const {showComments, post} = props;
    const {postId, content, postedAt, hasAccess, permission, postPhotoId, hashTags, numberOfLikes, numberOfComments, bookmarked, liked, postedBy} = post;
    const [showComment] = useState(showComments);
    const [userInfo, setUserInfo] = useState({
        fullName: "",
        jobTitle: ""
    });
    const [showLiked, setLiked] = useState(liked);
    const [commentSent, setCommentSent] = useState(1213);
    const [showBookmarked, setBookmarked] = useState(bookmarked);
    const [showNumberOfLikes, setShowNumberOfLikes] = useState(numberOfLikes);
    const [showNumberOfComments, setShowNumberOfComments] = useState(numberOfComments);
    const [more, setMore] = useState(260);
    const history = useHistory();

    const classes = styles(props);

    function hashTagClick() {

    }

    const getCount = useCallback((numberOfComments, numberOfLikes) => {
        setShowNumberOfComments(numberOfComments);
    }, [setShowNumberOfComments]);

    useEffect(() => {
        setCommentSent(1213)
    }, [showComment]);

    useEffect(() => {
        getUser(postedBy);
    }, [postedBy]);

    const handleCommentSent = useCallback(() => {
        setCommentSent(() => Math.random());
        getCount()
    }, [setCommentSent, getCount]);

    const handleLike = useCallback(() => {
        API({
            method: "PUT",
            url: postsService.putLikePostByPostId(postId, localStorage.getItem("userId"))
        }).then(res => {
            if (res.status === 200) {
                setLiked(prevState => !prevState);
                setShowNumberOfLikes(res.data)
            }
        })
    }, [postId]);
    const handleBookmark = useCallback(() => {
        API({
            method: "PUT",
            url: postsService.putBookmarkPostByPostId(postId, localStorage.getItem("userId"))
        }).then(res => {
            if (res.status === 200) {
                setBookmarked(prevState => !prevState)
            }
        })
    }, [postId]);

    function getUser(uId) {
        API({url: userService.getInfoByUid(uId)}).then(res => {
            if (res.status === 200) {
                setUserInfo(res.data);
            }
        });
    }

    return (
        content && content.toString().length > 0 ?
            <>
                <Grid id={"parentPost"} alignContent={"flex-start"} container className={classes.card}>
                    <Grid alignItems={"center"} container className={classes.header} xs={12} item>
                        <Grid xs={8} item>
                            <Button onClick={() => {
                                history.push("/main/profile?uId=" + userInfo.userId);
                            }} fullWidth style={{display: "flex", justifyContent: "start"}}>
                                {userInfo.userId && <Avatar className={classes.avatar} alt="postUserProfile"
                                                            src={userService.getAvatarByUid(userInfo.userId, 64, 64)}/>}
                                <div style={{paddingRight: 5}}>
                                    <div className={classes.username}
                                         style={userInfo.fullName.length > 20 ? {fontSize: 10} : undefined}>{userInfo.fullName}</div>
                                    <div className={classes.userRole}
                                         style={userInfo.jobTitle.length > 20 ? {fontSize: 9} : undefined}>{userInfo.jobTitle}</div>
                                </div>
                                <div style={{marginRight: "auto"}}/>

                            </Button>
                        </Grid>
                        <Grid xs={4} item>
                            <div style={{
                                float: "left",
                                right: 20,
                                alignItems: "center",
                                display: "flex",
                                position: "relative"
                            }}>
                                <div className={classes.userRole}><Permission permission={permission}
                                                                              className={classes.actionIcon}/></div>
                                <IconButton className={classes.actionButton} size={"small"}><MdMoreVert
                                    className={classes.actionIcon}/></IconButton>
                            </div>
                        </Grid>
                    </Grid>
                    <Grid dir={content ? detectDirection(content) : undefined} className={classes.content} xs={12} item>
                        {content && more >= content.length ? content : content.substr(0, more)} {more <= content.length ?
                        (hasAccess ? <span onClick={() => {
                                setMore(prevState => prevState + content.length)
                            }} className={classes.more}>مشاهد بیشتر...</span> :
                            <AiTwotoneLock style={{color: "#007bff"}}/>) :
                        content.length >= 260 && <span onClick={() => {
                            setMore(260)
                        }} className={classes.more}>بستن</span>}

                    </Grid>
                    <Grid xs={12} item>
                        {hashTags.map(hashTag => <Chip dir={"ltr"} key={hashTag.hashTagId} className={classes.hashTag}
                                                       onClick={hashTagClick}
                                                       label={hashTag.hashTagName}
                                                       variant={"default"}
                                                       color="secondary" size="small"/>)
                        }
                    </Grid>
                    <Grid className={classes.imgCard} xs={12} item>
                        <img
                            className={classes.img}
                            alt="تصویر پست"
                            src={postsService.getPhotoByPhotoId(postPhotoId)}
                            title="تصویر پست"
                        />
                    </Grid>
                    <Grid style={{display: "flex"}} xs={12} item>
                        <Box style={{marginLeft: 13}}>
                            <IconButton disabled={!hasAccess} onClick={handleLike} className={classes.actionButton}
                                        size={"small"}>
                                <AiFillLike style={showLiked && {color: "#2684fe"}} className={classes.actionIcon}/>
                            </IconButton>
                            <span className={classes.actionNum}>{showNumberOfLikes}</span>
                        </Box>
                        <Box>
                            <IconButton disabled={!hasAccess} onClick={() => {
                                if (!showComments) {
                                    history.push("/main/post?pId=" + postId);
                                }
                            }} className={classes.actionButton} size={"small"}>
                                <MdComment style={showComment && {color: "#2684fe"}} className={classes.actionIcon}/>
                            </IconButton>
                            <span className={classes.actionNum}>{showNumberOfComments}</span>
                        </Box>
                        <div style={{marginLeft: "auto"}}/>
                        <Box>
                            <IconButton onClick={handleBookmark} className={classes.actionButton} size={"small"}>
                                <BsFillBookmarkFill style={showBookmarked && {color: "#2684fe"}}
                                                    className={classes.actionIcon}/></IconButton>
                        </Box>
                    </Grid>
                    <Grid xs={12} item>
                        <div className={classes.userRole}>{moment(new Date(postedAt)).fromNow()}</div>
                    </Grid>
                    {showComment && <>
                        <Grid xs={12} item>
                            <Comments getCount={getCount} commentSent={commentSent} postId={postId}/>
                        </Grid>
                    </>}
                </Grid>
                {showComment && <CommentForm handleCommentSent={handleCommentSent} postId={postId}/>}
            </>
            : null
    );
}

export default PostCard;