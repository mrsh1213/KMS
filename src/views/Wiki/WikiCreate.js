import React, {useCallback, useContext, useRef, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/styles";
import {Cancel, Send} from "@material-ui/icons";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import API from "../../config/API";
import {wikiService} from "../../config/constans";
import {useSnackbar} from "notistack";
import {useHistory} from "react-router";
import {BackDropContext} from "../../config/BackDropContext";
import HashTagsAutoComplete from "../common/HashTagsAutoComplete";
import KnowScopeAutoComplete from "../common/KnowScopeAutoComplete";
import AccessLevel from "../common/AccessLevel";
import Editor from "./Editor";
import DevTextField from "../../components/common/DevTextField";
import DevImageCrop from "../../components/common/DevImageCrop";


WikiCreate.propTypes = {};
const styles = makeStyles((theme) => ({
    container: {
        height: "100%",
        overflow: "auto",
        alignContent: "baseline"
    },
    card: {
        padding: 16,
        marginTop: 16,
        "borderRadius": "8px",
        "boxShadow": "0 4px 8px 0 rgba(37, 56, 88, 0.1)",
        "backgroundColor": "#ffffff"
    },
    username: {
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.71",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#344563"
    },
    userRole: {
        "fontFamily": "'IranSans'",
        "fontSize": "10px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#c0c7d1"
    },
    buttonRoot: {
        padding: 0
    },
    buttonText: {
        padding: 0
    },
    selectInput: {
        "fontFamily": "'IranSans'",
        "fontSize": "10px",

    },
    multiText: {
        marginTop: 8,
        marginBottom: 8,
    },
    autocomplete: {
        marginBottom: 8,
    },
    imageButton: {
        padding: 3,
        top: 70,
        right: 14,
        position: "absolute",
        zIndex: 10000
    },
    buttonSave: {
        "height": "32px", "borderRadius": "5px", "backgroundColor": "#2684fe !important",
        color: "#fff",
    },
    coverImage: {
        width: "100%",
        height: 172,
        display: "flex",
        maxHeight: 172,
        objectFit: "contain",
        boxShadow: "0 4px 16px 0 rgba(37, 56, 88, 0.1)",
        backgroundColor: "#deebfe",
        justifyContent: "center",
        alignItems: "center"
    },
    titleAddPhoto: {
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "500",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.57",
        "letterSpacing": "normal",
        "textAlign": "right",
        "color": "#2684fe"
    },
    title: {
        marginTop: 8,
        "fontFamily": "'IranSans'",
        "fontSize": "12px",
        fontWeight: "bold",
        color: "#000",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "textAlign": "left",
    },
    inputTitle: {
        padding: 5,
        fontSize: 16,
        fontWeight: "bold",
    }
}));


function WikiCreate(props) {
    const [accessLevel, setAccessLevel] = useState("TECHNICAL_GROUPS");
    const [hashTags, setHashTags] = useState([]);
    const [knScope, setKnScope] = useState(null);
    const [image, setImage] = useState(null);
    const [editorValue, setEditorValue] = useState(null);
    const [title, setTitle] = useState("");
    const [preview, setPreview] = useState(null);
    const inputRef = useRef(null);
    const {dispatch} = useContext(BackDropContext);
    const {enqueueSnackbar} = useSnackbar();
    const history = useHistory();
    const classes = styles();
    const user = JSON.parse(localStorage.getItem("user"));

    const handleGetAccessLevel = useCallback((selectedAccessLevel) => {
        setAccessLevel(selectedAccessLevel);
    }, [setAccessLevel]);
    const handleGetListHashTags = useCallback((selectedHashTags) => {
        setHashTags(selectedHashTags);
    }, [setHashTags]);
    const handleGetKnowScope = useCallback((selectedKnScope) => {
        setKnScope(selectedKnScope);
    }, [setKnScope]);

    const getEditorValue = useCallback((value) => {
        setEditorValue(value);
    }, [setEditorValue])

    function submitWiki() {
        if (editorValue && image && title && hashTags.length > 0) {
            API({
                data: image
                , method: "POST", url: wikiService.postWikiPhoto()
            }, dispatch).then(res => {
                if (res.status === 200 && res.data) {
                    API({
                        data: {
                            "title": title,
                            "content": editorValue,
                            // "attachmentIds":[],
                            "photoId": res.data,
                            // "hasAttachment":false,
                            "hashTagIds": hashTags.map(hashTag => (hashTag.hashTagId)),
                            "projectCode": knScope ? knScope.title : null,
                            // "resourcesId":[],
                            "permission": accessLevel,
                            "wroteBy": user.userId
                        }
                        , method: "POST", url: wikiService.postWiki()
                    }, dispatch).then(res => {
                        if (res.status === 201) {
                            history.push("/dashboard/wiki")
                        } else {
                            enqueueSnackbar("خطا در ثبت مقاله رخ داده است", {
                                variant: "error", anchorOrigin: {
                                    vertical: 'bottom',
                                    horizontal: 'center',
                                }
                            });
                        }
                    });
                } else {
                    enqueueSnackbar(" حجم تصویر باید کمتر از 1Mb باشد", {
                        variant: "error", anchorOrigin: {
                            vertical: 'bottom',
                            horizontal: 'center',
                        }
                    });
                }

            });
        } else {
            enqueueSnackbar("تصویر و متن و برچسب و عنوان مقاله الزامی می باشد", {
                variant: "error", anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'center',
                }
            });

        }
    }


    const getCropImage = useCallback((form, blob) => {
        setImage(form);
        setPreview(URL.createObjectURL(blob));
    }, []);
    return (
        <Grid className={classes.container} container>
            <Grid xs={12} item container>
                <div className={classes.coverImage}>
                    {preview ? <Grid xs={12} item>
                        <img alt={"preview"} style={{width: "100%", height: 200}}
                             src={preview}/>
                    </Grid> : <><Button size={"small"} variant={"text"}
                                        className={classes.titleAddPhoto} onClick={() => {
                        inputRef.current.click()
                    }}>
                        افزودن تصویر مقاله
                    </Button>
                    </>
                    }
                    {image && <IconButton onClick={() => {
                        setImage(null);
                        setPreview(null)
                    }} className={classes.imageButton}>
                        <Cancel/>
                    </IconButton>
                    }
                    <DevImageCrop inputRef={inputRef} uploadHandler={getCropImage} aspect={5 / 4}/>
                </div>
            </Grid>
            <Grid xs={12} item container
                  className={classes.card}>
                <Grid style={{marginBottom: "10px"}} xs={12} item>
                    <div className={classes.title}>{"عنوان مقاله"}</div>
                    <DevTextField className={classes.inputTitle} value={title} onChange={(e) => {
                        setTitle(e.target.value)
                    }} fullWidth
                                  placeholder="لطفا عنوان مقاله خود را بنویسید"/>
                </Grid>

                <Grid xs={12} item>
                    <Editor getValue={getEditorValue}/>
                </Grid>
            </Grid>
            <Grid xs={12} item container className={classes.card}>
                <Grid xs={12} item>
                    <AccessLevel handleGetAccessLevel={handleGetAccessLevel}/>
                </Grid>
                <Grid className={classes.autocomplete} xs={12} item>
                    <HashTagsAutoComplete handleGetListHashTags={handleGetListHashTags}/>
                </Grid>
                <Grid style={{marginBottom: 12}} className={classes.autocomplete} xs={12} item>
                    <KnowScopeAutoComplete handleGetKnowScope={handleGetKnowScope}/>
                </Grid>
                <Grid style={{marginBottom: 12}} alignItems={"flex-end"} xs={12} item container>

                    <Grid xs={8} item/>
                    <Grid xs={4} item>
                        <Button onClick={submitWiki} classes={{root: classes.buttonSave}} variant={"contained"}
                                fullWidth>انتشار<Send
                            style={{marginRight: 16, transform: "rotate(180deg)"}}/></Button>
                    </Grid>
                </Grid>
                {/*<div dangerouslySetInnerHTML={{__html: editorValue}}>*/}

                {/*</div>*/}
            </Grid>
        </Grid>
    );
}

export default WikiCreate;