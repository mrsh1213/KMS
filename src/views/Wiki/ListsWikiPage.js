import React, {useCallback, useContext, useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import DevSearchBox from "../../components/common/DevSearchBox";
import InfiniteScroll from "react-infinite-scroll-component";
import CircularProgress from "@material-ui/core/CircularProgress";
import API from "../../config/API";
import {searchService, wikiService} from "../../config/constans";
import {BackDropContext} from "../../config/BackDropContext";
import ListItemWiki from "./ListItemWiki";
import {useHistory, withRouter} from "react-router";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {makeStyles} from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import {MdPlaylistAdd} from "react-icons/all";

ListsWikiPage.propTypes = {};
const useStyles = makeStyles((theme) => ({
    container: {
        height: "100%",
        overflow: "auto",
        alignContent: "baseline"
    },
    tab: {
        [theme.breakpoints.down('sm')]: {
            color: "#42516e"
        },
        [theme.selected]: {
            color: "#2684fe"
        }
    },
}))

function ListsWikiPage(props) {
    const [listsWiki, setListWiki] = useState({content: [], number: -1, totalElements: 0});
    const [keyword, setKeyword] = useState("");
    const {dispatch} = useContext(BackDropContext);
    const [sort, setSort] = useState("wroteAt");
    const classes = useStyles();
    const history = useHistory();


    useEffect(() => {
        setListWiki({content: [], number: -1, totalElements: 0});
        if (keyword) {
            searchWikies(0, keyword);
        } else {
            fetchDataWiki(0);
        }
    }, [keyword, setListWiki, sort]);

    const searchWikies = useCallback((page, keyword) => {
        API({
            method: "GET",
            url: searchService.searchWiki(encodeURIComponent(keyword), page, 10, localStorage.getItem("userId"))
        }, dispatch).then(res => {
            if (res.status === 200) {
                setListWiki(prevState => ({...res.data, content: [...prevState.content, ...res.data.content]}));
            } else if (res.status === 204) {
                setListWiki({content: [], number: -1, totalElements: 0})
            }
        }).catch(() => {
            setListWiki({content: [], number: -1, totalElements: 0});
        });
    }, [dispatch]);
    const fetchDataWiki = useCallback((page) => {
        if (keyword.length === 0) {
            API({
                method: "GET",
                url: wikiService.getWikiesBySort(sort, page, 10, localStorage.getItem("userId"))
            }, dispatch).then(res => {
                if (res.status === 200) {
                    // setTimeout(() => {
                    setListWiki(prevState => ({...res.data, content: [...prevState.content, ...res.data.content]}));
                    // }, 1000);
                } else if (res.status === 204) {
                    setListWiki({content: [], number: -1, totalElements: 0})
                }
            })
        } else {
            searchWikies(page, keyword)
        }

    }, [dispatch, keyword, sort]);

    const searchMethod = useCallback((keyword) => {
        setKeyword(keyword);
    }, []);
    const handleChangeSort = useCallback((e, value) => {
        if (sort === value) {
            return;
        } else {
            setListWiki({content: [], number: -1, totalElements: 0});
            setSort(value);
        }
    }, [sort]);

    return (
        <Grid className={classes.container} container>
            <Grid xs={12} item>
                <DevSearchBox searchMethod={searchMethod}/>
            </Grid>
            <Grid xs={12} item>
                <Tabs
                    value={sort}
                    onChange={handleChangeSort}
                    indicatorColor="secondary"
                    textColor="primary"
                    centered
                    variant={"fullWidth"}
                >
                    <Tab className={classes.tab} value={"wroteAt"} label="جدیدترین"/>
                    <Tab className={classes.tab} value={"numberOfSeen"} label="پربازدیدترین"/>
                    <Tab className={classes.tab} value={"top"} label="محبوب‌ترین"/>
                </Tabs>
            </Grid>
            <Grid className={classes.container} container xs={12} item>
                <InfiniteScroll
                    initialScrollY={200}
                    height={"100%"}
                    style={{minWidth: "275px"}}
                    dataLength={listsWiki.content.length}
                    next={() => {
                        fetchDataWiki(listsWiki.number + 1)
                    }}
                    hasMore={!listsWiki.last}
                    loader={listsWiki.content.length > 0 &&
                    <div style={{textAlign: "center",}}><CircularProgress/></div>}
                >
                    {listsWiki.content.map(wiki => {
                            if (wiki) {
                                return (<Grid key={wiki.wikiId} xs={12} item>
                                    <ListItemWiki wiki={wiki}/>
                                </Grid>)
                            } else {
                                return "";
                            }
                        }
                    )}
                </InfiniteScroll>

            </Grid>
            <Fab
                style={{
                    zIndex: 1000,
                    position: "fixed",
                    bottom: 90,
                    left: 10,
                    textAlign: "left"
                }}
                onClick={() => {
                    history.push("/main/createWiki")
                }}
                size={"large"}
                color={"primary"}
            >
                <MdPlaylistAdd style={{width: 24, height: 24}}/>
            </Fab>
        </Grid>
    );
}

export default withRouter(ListsWikiPage);