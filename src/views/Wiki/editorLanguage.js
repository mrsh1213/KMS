export const editorLanguage = {
    code: 'en',
    toolbar: {
        default: 'پیشفرض',
        save: 'ذخیره',
        font: 'فونت',
        formats: 'قالب ها',
        fontSize: 'اندازه فونت',
        bold: 'برجسته',
        underline: 'زیرخط',
        italic: 'کج',
        strike: 'Strike',
        subscript: 'Subscript',
        superscript: 'Superscript',
        removeFormat: 'حذف قالب',
        fontColor: 'رنگ فونت',
        hiliteColor: 'رنگ هایلایت',
        indent: 'Indent',
        outdent: 'Outdent',
        align: 'ترازبندی',
        alignLeft: 'تراز به چپ',
        alignRight: 'تراز به راست',
        alignCenter: 'تراز وسط',
        alignJustify: 'تراز متنی',
        list: 'لیست',
        orderList: 'لیست مرتب شده',
        unorderList: 'لیست مرتب نشده',
        horizontalRule: 'خط افقی',
        hr_solid: 'خطی',
        hr_dotted: 'نقطه ای',
        hr_dashed: 'خط تیره',
        table: 'جدول',
        link: 'لینک',
        math: 'Math',
        image: 'عکس',
        video: 'ویدیو',
        audio: 'صدا',
        fullScreen: 'تمام صفحه',
        showBlocks: 'نمایش بلوکی',
        codeView: 'نمایش کد',
        undo: 'برگرداندن',
        redo: 'پیش بردن',
        preview: 'پیش نمایش',
        print: 'چاپ',
        tag_p: 'پاراگراف',
        tag_div: 'معمولی (DIV)',
        tag_h: 'سربرگ',
        tag_blockquote: 'Quote',
        tag_pre: 'کد',
        template: 'قالب',
        lineHeight: 'ارتفاع خط',
        paragraphStyle: 'استایل پاراگراف',
        textStyle: 'استایل متن',
        imageGallery: 'گالری عکس'
    },
    dialogBox: {
        linkBox: {
            title: 'افزودن لینک',
            url: 'آدرس لینک',
            text: 'متن لینک',
            newWindowCheck: 'نمایش در سر برگ جدید'
        },
        mathBox: {
            title: 'Math',
            inputLabel: 'Mathematical Notation',
            fontSizeLabel: 'Font Size',
            previewLabel: 'Preview'
        },
        imageBox: {
            title: 'افزودن عکس',
            file: 'انتخاب عکس',
            url: 'آدرس عکس',
            altText: 'متن عکس'
        },
        videoBox: {
            title: 'افزودن ویدیو',
            file: 'Select from files',
            url: 'Media embed URL, YouTube/Vimeo'
        },
        audioBox: {
            title: 'Insert Audio',
            file: 'Select from files',
            url: 'Audio URL'
        },
        browser: {
            tags: 'برچسب',
            search: 'جستجو',
        },
        caption: 'افزدون توضیحات',
        close: 'بستن',
        submitButton: 'ثبت',
        revertButton: 'انصراف',
        proportion: 'نسبت تصویر',
        basic: 'پایه',
        left: 'چپ',
        right: 'راست',
        center: 'وسط',
        width: 'عرض',
        height: 'ارتفاع',
        size: 'اندازه',
        ratio: 'نرخ'
    },
    controller: {
        edit: 'ویرایش',
        unlink: 'Unlink',
        remove: 'حذف',
        insertRowAbove: 'اضافه کردن سطر به بالا',
        insertRowBelow: 'اضافه کردن سطر به پایین',
        deleteRow: 'حذف سطر',
        insertColumnBefore: 'افزودن ستون به قبل',
        insertColumnAfter: 'افزودن ستون به بعد',
        deleteColumn: 'حذف ستون',
        fixedColumnWidth: 'ثابت کردن طول ستون',
        resize100: 'Resize 100%',
        resize75: 'Resize 75%',
        resize50: 'Resize 50%',
        resize25: 'Resize 25%',
        autoSize: 'Auto size',
        mirrorHorizontal: 'Mirror, Horizontal',
        mirrorVertical: 'Mirror, Vertical',
        rotateLeft: 'Rotate left',
        rotateRight: 'Rotate right',
        maxSize: 'Max size',
        minSize: 'Min size',
        tableHeader: 'Table header',
        mergeCells: 'Merge cells',
        splitCells: 'Split Cells',
        HorizontalSplit: 'Horizontal split',
        VerticalSplit: 'Vertical split'
    },
    menu: {
        spaced: 'فاصله',
        bordered: 'حاشیه دار',
        neon: 'Neon',
        translucent: 'Translucent',
        shadow: 'سایه',
        code: 'کد'
    }

}
export const optionsEditor = {
    "mode": "classic",
    "rtl": true,
    "katex": "window.katex",
    "font": [
        "IranSans",
        "IranSansMedium",
        "IranSansFaNum",
        "Arial",
        "tahoma",
        "Courier New,Courier",
    ],
    "fontSize": [
        8,
        10,
        12,
        14,
        16,
        18,
        20,
        24,
        30,
        32,
        34,
        36,
    ],
    "formats": [
        "p",
        "blockquote",
        "h1",
        "h2",
        "h3"
    ],
    "colorList": [
        [
            "#ff0000",
            "#ff5e00",
            "#ffe400",
            "#abf200"
        ],
        [
            "#00d8ff",
            "#0055ff",
            "#6600ff",
            "#ff00dd"
        ]
    ],
    "imageGalleryUrl": "https://etyswjpn79.execute-api.ap-northeast-1.amazonaws.com/suneditor-demo",
    "videoResizing": false,
    "videoHeightShow": false,
    "videoFileInput": false,
    "videoUrlInput": false,
    "videoRatioShow": false,
    "audioUrlInput": false,
    "tableCellControllerPosition": "",
    "tabDisable": true,
    "shortcutsDisable": [
        "bold",
        "strike",
        "underline",
        "italic",
        "undo",
        "indent"
    ],
    "lineHeights": [
        {
            "text": "Single",
            "value": 1
        },
        {
            "text": "Double",
            "value": 2
        }
    ],
    "paragraphStyles": [
        "spaced",
        {
            "name": "Box",
            "class": "__se__customClass"
        }
    ],
    "textStyles": [
        "translucent",
        {
            "name": "Emphasis",
            "style": "-webkit-text-emphasis: filled;",
            "tag": "span"
        }
    ],
    "templates": [
        {
            "name": "Template-1",
            "html": "<p>HTML source1</p>"
        },
        {
            "name": "Template-2",
            "html": "<p>HTML source2</p>"
        }
    ],
    "placeholder": "مقاله خود را بنویسید",
    "linkProtocol": "",
    "buttonList": [
        [
            "undo",
            "redo",
            "font",
            "fontSize",
            "formatBlock",
            "paragraphStyle",
            "blockquote",
            "bold",
            "underline",
            "italic",
            "strike",
            // "subscript",
            // "superscript",
            "fontColor",
            "hiliteColor",
            "textStyle",
            "removeFormat",
            "outdent",
            "indent",
            "align",
            "horizontalRule",
            "list",
            "lineHeight",
            "table",
            "link",
            "image",
            "video",
            "audio",
            // "math",
            // "imageGallery",
            "showBlocks",
            "codeView",
            "preview",
            // "print",
            "fullScreen",
            // "newLineDiv",
            // "template"
        ]
    ]
};