import React, {useCallback, useContext, useEffect, useState} from 'react';
import WikiCreate from "./WikiCreate";
import Grid from "@material-ui/core/Grid";
import DevSearchBox from "../../components/common/DevSearchBox";
import Fab from "@material-ui/core/Fab";
import {MdPlaylistAdd} from "react-icons/md";
import {useHistory} from "react-router";
import {func} from "prop-types";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import CardWiki from "./CardWiki";
import API from "../../config/API";
import {postsService, searchService, wikiService} from "../../config/constans";
import {BackDropContext} from "../../config/BackDropContext";

Index.propTypes = {};
const useStyles = makeStyles((theme) => ({
    titleCardHeader: {
        "fontFamily": "'IranSans'",
        "fontSize": "12px",
        "fontWeight": "500",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.67",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#42516e"
    },
    seeMore: {
        "fontFamily": "'IranSans'",
        "fontSize": "10px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#2684fe"
    },
    scrollHorizontal: {
        overflow: "scroll",
        display: "flex"
    },
    hr25: {
        width: "25%",
        border: "inset 1px #42516e",
        margin: 0
    }
}));

function CardHeader({title, classes, moreHandler}) {
    return (<Grid alignContent={"flex-start"} container>
        <Grid className={classes.titleCardHeader} xs={3} item>
            {title}
        </Grid>
        <Grid xs={6} item/>
        <Grid style={{textAlign: "left"}} xs={3} item>
            <Button onClick={moreHandler} className={classes.seeMore} size={"small"} variant={"text"}>
                دیدن همه
            </Button>
        </Grid>
        <Grid xs={12} item>
            <hr style={{margin: 0}}/>
            <hr className={classes.hr25}/>
        </Grid>
    </Grid>);
}

function Index(props) {
    const [newWiki, setNewWiki] = useState([]);
    const history = useHistory();
    const classes = useStyles();
    const [keyword, setKeyword] = useState([]);
    const {dispatch} = useContext(BackDropContext);

    const searchMethod = useCallback((keyword) => {
        setKeyword(keyword);
    }, []);
    // const searchPosts = useCallback((page, keyword) => {
    //     API({
    //         method: "GET",
    //         url: searchService.searchPost(encodeURIComponent(keyword), page, 10, localStorage.getItem("userId"))
    //     }, dispatch).then(res => {
    //         if (res.status === 200) {
    //             setPosts(res.data);
    //         } else if (res.status === 204) {
    //             setPosts({content: [], totalElements: 0})
    //         }
    //     });
    // }, [dispatch]);

    const getNewWiki = useCallback(() => {
        API({method: "GET", url: wikiService.getNewWiki(localStorage.getItem("userId"))}).then(res => {
            if (res.status === 200) {
                setNewWiki(res.data);
            } else {
                setNewWiki([]);
            }
        })
    }, [setNewWiki]);
    useEffect(() => {
        getNewWiki();
    }, [getNewWiki]);

    return (
        <Grid spacing={2} container>
            <Grid xs={12} item>
                <DevSearchBox searchMethod={searchMethod}/>
            </Grid>
            <Grid xs={12} item container>
                <CardHeader moreHandler={() => {
                    history.push("/main/listsWiki/new")
                }} classes={classes} title={"جدید ترین"}/>
                <Grid className={classes.scrollHorizontal} xs={12} item>
                    {newWiki.map(wiki => <CardWiki inCard={true} key={wiki.wikiId} wiki={wiki}/>)}

                </Grid>
            </Grid>
            <Grid xs={12} item>
                <CardHeader moreHandler={() => {
                    history.push("/main/listsWiki/seen")
                }} classes={classes} title={"پر بازدیدترین"}/>
                <Grid className={classes.scrollHorizontal} xs={12} item>
                    {newWiki.reverse().map(wiki => <CardWiki inCard={true} key={wiki.wikiId} wiki={wiki}/>)}
                </Grid>
            </Grid>
            <Fab
                style={{
                    zIndex: 1000,
                    position: "fixed",
                    bottom: 90,
                    left: 10,
                    textAlign: "left"
                }}
                onClick={() => {
                    history.push("/main/createWiki")
                }}
                size={"large"}
                color={"primary"}
            >
                <MdPlaylistAdd style={{width: 24, height: 24}}/>
            </Fab>
        </Grid>
    );
}

export default Index;