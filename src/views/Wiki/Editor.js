import React, {useEffect, useState} from 'react';
import SunEditor from 'suneditor-react';
import {
    editorLanguage,
    optionsEditor
} from "./editorLanguage";
import 'suneditor/dist/css/suneditor.min.css';
import '../../assets/css/sunEditor.css';
import PropTypes from 'prop-types';

Editor.propTypes = {
    getValue: PropTypes.func
};

function Editor({getValue}) {
    const [value, setValue] = useState(null);
    useEffect(() => {
        getValue(value)
    }, [value, getValue])

    function handleChange(content) {
        setValue(content); //Get Content Inside Editor
    }

    return (
        <>
            <SunEditor value={value} onChange={handleChange} setOptions={optionsEditor} lang={editorLanguage}/>
        </>
    );
}

export default Editor;