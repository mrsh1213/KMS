import React, {useCallback, useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import Grid from "@material-ui/core/Grid";
import {userService, wikiService} from "../../config/constans";
import moment from "moment";
import API from "../../config/API";
import {AiTwotoneLock, BiNetworkChart, BsFillBookmarkFill, FaEye, MdPublic, TiGroup} from "react-icons/all";
import Rate from "rc-rate/es";
import Avatar from "@material-ui/core/Avatar";
import {useHistory} from "react-router";

ListItemWiki.propTypes = {};
const useStyles = makeStyles((theme) => ({
        image: {
            "width": "95%", "height": "96px", "border": "solid 1px #e0e1e5"
        },
        container: {
            "height": "128px",
            "padding": "8px",
            "borderRadius": "10px",
            "boxShadow": "0 4px 16px 0 rgba(37, 56, 88, 0.1)",
            "backgroundColor": "#ffffff",
            marginTop: 16,
            "width": "100%",
        },
        title: {
            "fontFamily": "IranSansMedium",
            "fontSize": "14px",
            "fontWeight": "500",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.57",
            "letterSpacing": "normal",
            "color": "#344563"
        },
        content: {
            "fontFamily": "IranSans",
            "fontSize": "8px",
            "fontWeight": "normal",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.58",
            "letterSpacing": "normal",
            "color": "#c0c7d1"
        },
        createDate: {
            "fontFamily": "IranSans",
            "fontSize": "10px",
            "fontWeight": "normal",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.5",
            "letterSpacing": "normal",
            "color": "#c0c7d1"
        },
        actionIcon: {
            width: 20,
            height: 20
        },
        textSubHeader: {
            display: "flex",
            alignItems: "center",
            "fontFamily": "'IranSans'",
            padding: "2px 4px",
            "fontSize": "8px",
            "fontWeight": "normal",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.5",
            "letterSpacing": "normal",
            "color": "#000",
            "textAlign": "left"
        },
        avatar: {
            width: 20,
            height: 20,
        },
    }))
;

function Permission({permission}) {
    switch (permission) {
        case "TECHNICAL_GROUPS":
            return <span><TiGroup style={{fontSize: 9}}/>{" گروه تخصصی "}</span>
        case "WORKS":
            return <span><BiNetworkChart style={{fontSize: 9}}/>{" امور "}</span>

        case "PUBLIC":
            return <span><MdPublic style={{fontSize: 9}}/>{" عمومی "}</span>
        default:
            return <span><MdPublic style={{fontSize: 9}}/>{" عمومی0 "}</span>
    }
}

function ListItemWiki(props) {
    const {title, wikiId, photoId, wroteAt, bookmarked, wroteBy, numberOfSeen, rate, permission, hasAccess} = props.wiki;
    const [userInfo, setUserInfo] = useState({
        fullName: "",
        jobTitle: ""
    });

    const classes = useStyles();
    const history = useHistory();

    const getUser = useCallback(() => {
        API({url: userService.getInfoByUid(wroteBy)}).then(res => {
            if (res.status === 200) {
                setUserInfo(res.data);
            }
        });
    }, [wroteBy]);
    useEffect(() => {
        getUser();
    }, [getUser]);
    const handleRoute = useCallback(() => {
        if (hasAccess) {
            history.push("/main/wiki?wId=" + wikiId);
        }
    }, [wikiId, history, hasAccess]);
    return (
        title && title.toString().length > 0 ?
            <ListItem button onClick={handleRoute} key={wikiId} className={classes.container}>
                <Grid container>

                    <Grid xs={4} item>
                        <img className={classes.image} alt="cover1"
                             src={photoId ? wikiService.getWikiPhoto(photoId, (window.screen.width / 3).toFixed(0), (window.screen.width / 4).toFixed(0)) : "https://picsum.photos/64/64?p=" + Math.random()}/>
                    </Grid>
                    <Grid container xs={8} item>
                        <Grid xs={6} item className={classes.createDate}>
                         <span><Permission permission={permission}/>{(!hasAccess) &&
                         <AiTwotoneLock style={{color: "#007bff"}}/>}</span>
                    </Grid>
                    <Grid xs={5} item/>
                    <Grid style={{textAlign: "left"}} xs={1} item className={classes.createDate}>
                        <BsFillBookmarkFill style={bookmarked && {color: "#2684fe"}}
                                            className={classes.actionIcon}/>
                    </Grid>
                    <Grid xs={12} item title={title}
                          className={classes.title}>{title.length > 27 ? title.substr(0, 27) + "..." : title}</Grid>
                    <Grid xs={9} item
                          className={classes.textSubHeader}>
                        {userInfo.userId && <Avatar className={classes.avatar} alt="wikiUserProfile"
                                                    src={userService.getAvatarByUid(userInfo.userId, 64, 64)}/>}
                        <div style={{paddingRight: 5}}>
                            {userInfo.fullName}
                        </div>
                    </Grid>
                    <Grid xs={3} item style={{textAlign: "left"}}
                          className={classes.createDate}>
                        {moment(new Date(wroteAt)).fromNow()}
                    </Grid>
                    <Grid xs={4} item className={classes.createDate}><span style={{
                        fontFamily: 'IranSansFaNum',
                        fontSize: 9
                    }}><FaEye/>{" " + numberOfSeen + " "}بازدید</span></Grid>
                        <Grid style={{textAlign: "left"}} xs={8} item className={classes.createDate}>
                            <Rate direction={"rtl"} value={rate} allowHalf={true}
                                  disabled={true}/>
                            <span style={{fontFamily: 'IranSansFaNum'}}>امتیاز{" " + rate.toFixed(1) + " "}</span>
                        </Grid>

                    </Grid>
                </Grid>
            </ListItem>
            : null
    );
}

export default ListItemWiki;