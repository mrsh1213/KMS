import React, {useCallback, useEffect, useState} from 'react';
import {useQuery} from "../../../config/Helper";
import Grid from "@material-ui/core/Grid";
import API from "../../../config/API";
import {wikiService} from "../../../config/constans";
import CardWiki from "../CardWiki";

Index.propTypes = {};

function Index(props) {
    const [wiki, setWiki] = useState(null);
    const wikiId = useQuery().get("wId");

    const getWikiById = useCallback(() => {
        API({
            method: "GET",
            url: wikiService.getWikiById(wikiId, localStorage.getItem("userId"))
        }).then(res => {
            if (res.status === 200) {
                setWiki(res.data)
            }
        })
    }, [wikiId]);
    useEffect(() => {
        getWikiById();
    }, [getWikiById])
    return (
        wiki &&
        <CardWiki wiki={wiki}/>
    );
}

export default Index;