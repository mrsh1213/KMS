import React, {useCallback, useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import Button from "@material-ui/core/Button";
import {userService, wikiService} from "../../config/constans";
import PropTypes from "prop-types"
import {useHistory} from "react-router";
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";
import API from "../../config/API";
import moment from "moment";
import IconButton from "@material-ui/core/IconButton";
import Rate from 'rc-rate';
import 'rc-rate/assets/index.css';
import '../../assets/css/rc-react.css';
import {
    MdPublic, FaEye, BiNetworkChart, TiGroup, BsFillBookmarkFill,
    FiMoreHorizontal, AiTwotoneLock
} from "react-icons/all";
import {Edit} from "@material-ui/icons";
import Chip from "@material-ui/core/Chip";

const useStyles = makeStyles((theme) => ({
    card: {
        placeContent: "baseline",
        minWidth: 232,
        margin: (props) => props.inCard ? 8 : 0,
        padding: 0,
        overflow: "auto",
        height: "100%",
        "maxWidth": (props) => props.inCard ? "232px" : undefined,
        "maxHeight": (props) => props.inCard ? "160px" : undefined,
        "borderRadius": (props) => props.inCard ? "10px" : 0,
        "boxShadow": "0 4px 16px 0 rgba(37, 56, 88, 0.1)",
        "backgroundColor": "#ffffff !important"
    },
    image: {
        maxWidth: "100%",
        height: "auto",
        minWidth: (props) => props.inCard ? 232 : "100%",
        minHeight: (props) => props.inCard ? 110 : "100%",
        maxHeight: (props) => props.inCard ? 110 : "25%",
    },
    cover: {
        padding: 0,
        position: "relative",
        "maxWidth": (props) => props.inCard ? "232px" : undefined,
        "minHeight": (props) => props.inCard ? "110px" : undefined,
        // maxHeight: "30%",
        "backgroundColor": "#acb6c6"
    },
    textHeader: {
        margin: "2px 8px",
        "fontFamily": "'IranSans'",
        "fontSize": (props) => props.inCard ? "13px" : 16,
        "fontWeight": "bold",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.62",
        "letterSpacing": "normal",
        "textAlign": "left",
        "color": "#ffffff"
    },
    containerDetail: {
        margin: "2px 8px 2px 3px",
    },
    textDetail: {
        alignSelf: "center",
        "fontFamily": "'IranSans'",
        "fontSize": (props) => props.inCard ? "8px" : 10,
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "color": "#c0c7d1",
        "textAlign": "left"
    },
    textSubHeader: {
        display: "flex",
        alignItems: "center",
        "fontFamily": "'IranSans'",
        padding: "2px 4px",
        "fontSize": (props) => props.inCard ? "7px" : 9,
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "color": "#ffffff",
        "textAlign": "left"
    },
    header: {
        position: "absolute",
        bottom: 0,
        zIndex: 1
    },
    avatar: {
        width: (props) => props.inCard ? 20 : 40,
        height: (props) => props.inCard ? 20 : 40,
    },
    gradientCover: {
        position: "absolute",
        height: "100%",
        width: "100%",
        backgroundImage: 'linear-gradient(to bottom, #ffffff00, #0000009e)'
    },
    actionIcon: {
        width: (props) => props.inCard ? 20 : 26,
        height: (props) => props.inCard ? 20 : 26
    },
    hashTag: {
        "fontFamily": "'IranSans'",
        height: 16,
        borderRadius: 5,
        backgroundColor: "#deebfe",
        "&:hover": {
            backgroundColor: "#deebfe",
        },
        "fontSize": "10px",
        "marginRight": "5px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.75",
        "letterSpacing": "normal",
        "textAlign": "right"
    },
}));
CardWiki.propTypes = {
    wiki: PropTypes.object
};

function Permission({permission}) {
    switch (permission) {
        case "TECHNICAL_GROUPS":
            return <span><TiGroup style={{fontSize: "small"}}/>{" گروه تخصصی "}</span>
        case "WORKS":
            return <span><BiNetworkChart style={{fontSize: "small"}}/>{" امور "}</span>

        case "PUBLIC":
            return <span><MdPublic style={{fontSize: "small"}}/>{" عمومی "}</span>
        default:
            return <span>ناشناس</span>
    }
}

function CardWiki(props) {
    const {wiki, inCard} = props;
    const {photoId, hashTags, title, wikiId, hasAccess, wroteBy, lastUpdatedBy, lastUpdatedAt, numberOfSeen, wroteAt, bookmarked, rate, permission} = wiki;
    const [showBookmarked, setBookmarked] = useState(bookmarked);
    const [showRate, setRate] = useState(rate);
    const [showUserLastUpdate, setUserLastUpdate] = useState({fullName: ""});
    const [userInfo, setUserInfo] = useState({
        fullName: "",
        jobTitle: ""
    });
    const classes = useStyles(props);
    const history = useHistory();
    const getUser = useCallback(() => {
        API({url: userService.getInfoByUid(wroteBy)}).then(res => {
            if (res.status === 200) {
                setUserInfo(res.data);
            }
        });
    }, [wroteBy]);

    const getUserLastUpdate = useCallback(() => {
        if (lastUpdatedBy) {
            API({url: userService.getInfoByUid(lastUpdatedBy)}).then(res => {
                if (res.status === 200) {
                    setUserLastUpdate(res.data);
                }
            });
        }
    }, [lastUpdatedBy]);

    useEffect(() => {
        getUser();
        getUserLastUpdate();
    }, [getUser, getUserLastUpdate]);
    const handleRoute = useCallback(() => {
        if (hasAccess) {
            history.push("/main/wiki?wId=" + wikiId);
        }
    }, [wikiId, history, hasAccess]);



    const handleRate = useCallback((value) => {
        API({
            method: "PUT",
            url: wikiService.putRateWiki(wikiId, localStorage.getItem("userId")),
            data: {
                rateValue: value
            }
        }).then(res => {
            if (res.status === 200) {
                setRate(value)
            }
        });

    }, [setRate, wikiId]);
    const handleBookmark = useCallback(() => {
        API({
            method: "PUT",
            url: wikiService.putBookmarkWikiByWId(wikiId, localStorage.getItem("userId"))
        }).then(res => {
            if (res.status === 200) {
                setBookmarked(prevState => !prevState)
            }
        })
    }, [wikiId]);
    return (
        <Card variant={"text"} component={Grid} container className={classes.card}>
            <Grid component={inCard ? Button : undefined} onClick={inCard ? handleRoute : undefined} xs={12}
                  container item className={classes.cover}>
                <img className={classes.image} alt="cover_wiki"
                     src={photoId ? wikiService.getWikiPhoto(photoId, 400, (window.screen.height / 2).toFixed(0)) : "https://picsum.photos/232/92?p=" + Math.random()}/>
                <Grid xs={12} className={classes.header} container item>
                    <Grid xs={12} item className={classes.textHeader}>
                        {title}
                    </Grid>
                    <Grid xs={8} item className={classes.textSubHeader}>
                        {userInfo.userId && <Avatar onClick={() => {
                            history.push("/main/profile?uId=" + userInfo.userId);
                        }} className={classes.avatar} alt="wikiUserProfile"
                                                    src={userService.getAvatarByUid(userInfo.userId, 100, 100)}/>}
                        <div style={{paddingRight: 5}}>
                                {userInfo.fullName}
                                {/*{userInfo.jobTitle}*/}
                            </div>
                        </Grid>
                        <Grid xs={4} item className={classes.textSubHeader}
                              style={{fontFamily: 'IranSansFaNum', justifyContent: "flex-end"}}>
                            {moment(new Date(wroteAt)).fromNow()}
                        </Grid>
                    </Grid>
                    <div className={classes.gradientCover}/>
                </Grid>
                <Grid container item={12} className={classes.containerDetail}>
                    <Grid xs={6} item className={classes.textDetail}>
                        <IconButton onClick={handleBookmark} size={"small"}>
                            <BsFillBookmarkFill style={showBookmarked && {color: "#2684fe"}}
                                                className={classes.actionIcon}/></IconButton>
                    </Grid>
                    <Grid style={{textAlign: "left", paddingLeft: 8}} xs={5} item className={classes.textDetail}>
                        <span><Permission permission={permission}/>{(!hasAccess) &&
                        <AiTwotoneLock style={{color: "#007bff"}}/>}</span>
                    </Grid>
                    <Grid xs={1} item className={classes.textDetail}>
                        <IconButton size={"small"}><FiMoreHorizontal className={classes.actionIcon}/></IconButton>
                    </Grid>
                    <Grid xs={4} item className={classes.textDetail}>
                        <span style={{
                            fontFamily: 'IranSansFaNum',
                            fontSize: inCard ? 9 : 12
                        }}><FaEye/>{" " + numberOfSeen + " "}بازدید</span>
                    </Grid>
                    <Grid style={{textAlign: "left"}} xs={8} item className={classes.textDetail}>
                        <span style={{fontFamily: 'IranSansFaNum'}}>امتیاز{" " + showRate.toFixed(1) + " "}</span>
                        <Rate onChange={handleRate} direction={"rtl"} value={showRate} allowHalf={true}
                              disabled={inCard}/>
                    </Grid>
                    {(!inCard) && <><Grid className={classes.textDetail} xs={9} item>
                        {`آخرین ویرایش ${moment(new Date(lastUpdatedAt ? lastUpdatedAt : wroteAt)).fromNow()} توسط ${showUserLastUpdate.fullName ? showUserLastUpdate.fullName : userInfo.fullName}`}
                        {}
                    </Grid>
                        <Grid style={{textAlign: "left"}} className={classes.textDetail} xs={3} item>
                            {/*color: "#2684fe"*/}
                            <Button style={{fontSize: 12, color: "rgba(۲۵,۴۹,۵,0.48)"}} disabled size={"small"}><Edit
                                style={{fontSize: 16}}/> {" ویرایش "}</Button>
                        </Grid>
                        <Grid xs={12} item>
                            <hr/>
                        </Grid>
                        <Grid xs={12} item style={{padding: 5}} dangerouslySetInnerHTML={{__html: wiki.content}}/>
                        <Grid xs={12} className={classes.textHeader} style={{color: "#000"}} item>
                            برچسب ها
                            <hr style={{margin: 0}}/>
                            {hashTags.map(hashTag => <Chip dir={"ltr"} key={hashTag.hashTagId}
                                                           className={classes.hashTag}
                                // onClick={hashTagClick}
                                                           label={hashTag.hashTagName}
                                                           variant={"default"}
                                                           color="secondary" size="small"/>)
                            }
                        </Grid>
                    </>}
                </Grid>
        </Card>
    );
}

export default CardWiki;