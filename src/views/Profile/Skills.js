import React, {useCallback, useEffect, useState} from 'react';
import API from "../../config/API";
import {userService} from "../../config/constans";
import Skill from "./Skill";
import IconButton from "@material-ui/core/IconButton";
import {MdAddCircle} from "react-icons/md";
import Slider from "@material-ui/core/Slider";
import {Close, Done} from "@material-ui/icons";
import {useSnackbar} from "notistack";
import DevTextField from "../../components/common/DevTextField";

Skills.propTypes = {};

const styledEdit = {
    background: "#fff",
    padding: 3,
    border: "none !important",
    fontWeight: "bold",
    borderBottom: "1px solid #757575",
    overflowWrap: "anywhere",
    marginBottom: 32,
    fontFamily: "'IranSans'",
    "fontSize": "12px",
    "fontStretch": "normal",
    "textAlign": "right",
    "fontStyle": "normal",
    "lineHeight": "1.58",
    "letterSpacing": "normal",
    "color": "#42516e"
};
const marks = [
    {
        value: 0,
        label: 'آشنایی',
    },
    {
        value: 25,
        label: 'کم',
    },
    {
        value: 50,
        label: 'متوسط',
    },
    {
        value: 75,
        label: 'خوب',
    },
    {
        value: 100,
        label: 'عالی',
    },
];

function Skills(props) {
    const [skills, setSkills] = useState([]);
    const [titleSkill, setTitleSkill] = useState("");
    const [rate, setRate] = useState(75);
    const [edited, setEdited] = useState(false);
    const [editMode, setEditMode] = useState(false);
    const {userId} = props;
    const {enqueueSnackbar} = useSnackbar();
    const getSkills = useCallback((userId) => {
        API({url: userService.getSkillsByUid(userId)}).then(res => {
            setSkills(res.data);
        })
    }, [])
    useEffect(() => {
        getSkills(userId);
    }, [userId, edited, getSkills]);

    const handleEdited = useCallback(() => {
        setEdited(false);
        getSkills(userId);
    }, [setEdited, getSkills, userId])

    function addSkills() {
        API({
            data: {name: titleSkill.trim(), rate: rate},
            method: "Post",
            url: userService.postSkillsByUid(localStorage.getItem("userId"))
        }).then(res => {
            if (res.status === 201) {
                setEditMode(false);
                setTitleSkill("");
                setRate(0);
                getSkills(props.userId);
            }
        }).catch((e) => {
            if (e.response.status === 400) {
                enqueueSnackbar(e.response.data.message, {
                    variant: "error", anchorOrigin: {
                        vertical: 'bottom',
                        horizontal: 'center',
                    }
                });
            }
        });
    }

    function cancelAddSkills() {
        setEditMode(false);
        setTitleSkill("");
        setRate(75);
    }

    function onBlurTitleSkill(e) {
        setTitleSkill(e.target.value)
    }

    function handleChangeSlider(e, newValue) {
        if (newValue !== rate) {
            setRate(newValue);
        }
    }

    return (
        <div>

            {JSON.parse(localStorage.getItem("user")).userId === userId ?
                <>
                    <div style={editMode ? {
                        backgroundColor: "#f8f9fc",
                        padding: 12, textAlign: "left"
                    } : {textAlign: "right"}}>
                        {editMode ?
                            <>
                                <div style={{textAlign: "center"}}>
                                    {/*<div onBlur={onBlurTitleSkill} style={styledEdit} contentEditable={"true"}*/}
                                    {/*     suppressContentEditableWarning={true}*/}
                                    {/*>{titleSkill}</div>*/}
                                    <DevTextField
                                        fullWidth
                                        margin={"none"}
                                        color={"secondary"}
                                        variant="outlined"
                                        label="عنوان مهارت را بنویسید"
                                        value={titleSkill}
                                        onChange={onBlurTitleSkill}
                                    />
                                    <Slider
                                        value={rate}
                                        onChange={handleChangeSlider}
                                        style={{width: "90%"}}
                                        aria-labelledby="discrete-slider-always"
                                        step={25}
                                        marks={marks}
                                        valueLabelDisplay={"on"}
                                        valueLabelFormat={() => "سطح"}
                                    />
                                </div>
                                <IconButton size={"small"} onClick={cancelAddSkills}><Close
                                    style={{fontSize: 22}}/></IconButton>
                                <IconButton size={"small"} onClick={addSkills}><Done
                                    style={{fontSize: 22}}/></IconButton>
                            </>
                            :
                            <IconButton size={"small"} onClick={() => {
                                setEditMode(true)
                            }}><MdAddCircle style={{fontSize: 22}}/></IconButton>
                        }
                    </div>
                </>
                : ""}
            <hr/>
            {skills.length === 0 ?
                <div className={"text-muted align-self-center"}>هیچ مهارتی اضافه نشده است</div>
                :
                skills.map(skill => <Skill handleEdited={handleEdited} userId={userId} key={skill.skillId}
                                           skill={skill}/>)
            }
        </div>
    );
}

export default Skills;