import React, {useCallback, useContext, useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/styles";
import Avatar from "@material-ui/core/Avatar";
import KmStatusMe from "../Dashboard/KmStatusMe";
import API from "../../config/API";
import {userService} from "../../config/constans";
import {MdAddAPhoto} from "react-icons/md";
import IconButton from "@material-ui/core/IconButton";
import {Person} from "@material-ui/icons";
import Dialog from "@material-ui/core/Dialog";
import "react-image-crop/dist/ReactCrop.css";
import DevImageCrop from "../../components/common/DevImageCrop";
import {BackDropContext} from "../../config/BackDropContext";

PersonInfo.propTypes = {
    userId: PropTypes.string
};
const styles = makeStyles(() => ({
    card: {
        "fontFamily": "'IranSans'",
        "borderRadius": "10px",
        "boxShadow": "0 4px 16px 0 rgba(37, 56, 88, 0.1)",
        "backgroundColor": "#ffffff",
        minHeight: 240,
        textAlign: "center",
        padding: 16
    },
    avatar: {
        width: 94,
        height: 94,
        "boxShadow": "0 3px 6px 0 rgba(37, 56, 88, 0.2)"
    },
    fullName: {
        marginTop: 12,
        "fontFamily": "'IranSans'",
        "fontSize": "18px",
        "fontWeight": "bold",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.56",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#344563"
    },
    jobTitle: {
        "fontFamily": "'IranSans'",
        "fontSize": "12px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.58",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#42516e"
    },
    numFollowers: {
        "fontFamily": "'IranSansFaNum'",
        "fontSize": "24px",
        "fontWeight": "bold",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.58",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#42516e"
    },
    titleFollowers: {
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.57",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#c0c7d1"
    },
    MuiIconButtonRoot: {
        padding: 0
    },
    buttonFollow: {
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.57",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#ffffff",
        "borderRadius": "5px !important",
        "backgroundColor": "#2684fe !important",
        width: 97,
        height: 32
    }
}));


function PersonInfo(props) {
    const [info, setInfo] = useState({
        jobTitle: "--",
        fullName: "--",
        followers: 5,
        following: 10
    });
    const [image, setImage] = useState(null);
    const [showImage, setShowImage] = useState(false);
    const {dispatch} = useContext(BackDropContext);
    const inputRef = useRef(null);
    const classes = styles();
    const {userId} = props;


    function uploadAvatar(form) {
        API({method: "POST", data: form, url: userService.postAvatarByUid(userId)}, dispatch).then(res => {
            if (res.status === 200) {
                getAvatar();
                // window.location.reload();
            }
        })

    }

    function getUser(Uid) {
        API({url: userService.getInfoByUid(Uid)}).then(res => {
            setInfo(prevState => ({...prevState, ...res.data}))
        });
    }

    const getAvatar = useCallback(() => {
        API({url: userService.getJsonAvatarByUid(userId, 100, 100)}).then(res => {
            if (res.status === 200) {
                setImage(res.data.avatar)
            }
        });
    }, [userId]);

    useEffect(() => {
        getUser(userId);
        getAvatar();
    }, [userId, getAvatar]);


    function closeImage() {
        setShowImage(false);
    }


    const editableImage = JSON.parse(localStorage.getItem("user")).userId === userId ? {
        onClick: () => {
            inputRef.current.click()
        }
    } : () => ({});

    return (<>
            <Dialog
                open={showImage}
                onClose={closeImage}
            >
                <img alt="avatar" src={"data:image/png;base64," + image}/>
            </Dialog>
            <Grid container alignItems={"center"} className={classes.card}>
                {/*{JSON.parse(localStorage.getItem("user")).userId === userId ? <div style={{*/}
                {/*    width: "100%",*/}
                {/*    textAlign: "left"*/}
                {/*}}>*/}
                {/*    <IconButton onClick={handleToggle} size={"small"}>*/}
                {/*        <AiOutlineSetting/>*/}
                {/*    </IconButton>*/}
                {/*    <div style={{position: "relative"}}>*/}
                {/*        <Popper style={{position: "absolute"}} open={openMenu} anchorEl={anchorRef.current}*/}
                {/*                role={undefined} transition disablePortal>*/}
                {/*            {({TransitionProps, placement}) => (*/}
                {/*                <Grow*/}
                {/*                    {...TransitionProps}*/}
                {/*                    style={{transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}*/}
                {/*                >*/}
                {/*                    <Paper>*/}
                {/*                        <ClickAwayListener onClickAway={handleClose}>*/}
                {/*                            <MenuList autoFocusItem={openMenu} id="menu-list-grow"*/}
                {/*                                      onKeyDown={handleListKeyDown}>*/}
                {/*                                <MenuItem onClick={handleOpenChangePassword}>تغییر رمز عبور</MenuItem>*/}
                {/*                            </MenuList>*/}
                {/*                        </ClickAwayListener>*/}
                {/*                    </Paper>*/}
                {/*                </Grow>*/}
                {/*            )}*/}
                {/*        </Popper>*/}
                {/*    </div>*/}
                {/*</div> : null}*/}
                <Grid xs={4} item>
                    {/*<div className={classes.titleFollowers}>دنبال کننده</div>*/}
                    {/*<div className={classes.numFollowers}>{info.followers}</div>*/}
                </Grid>
                <Grid xs={4} item>
                    {info.userId || true ? <>
                            <IconButton {...(image ? {onClick: undefined} : editableImage)}
                                        classes={{root: classes.MuiIconButtonRoot}}>
                                <Avatar className={classes.avatar}
                                        alt="User_Profile"
                                        src={"data:image/png;base64," + image}>
                                    <Person style={{fontSize: 40}}/>
                                </Avatar>
                            </IconButton>

                            {JSON.parse(localStorage.getItem("user")).userId === userId ? <>
                                <DevImageCrop maxSizeMB={0.5} circularCrop={true} uploadHandler={uploadAvatar}
                                              inputRef={inputRef}/>
                                <IconButton  {...editableImage} size={"small"}><MdAddAPhoto
                                    style={{fontSize: 18}}/></IconButton> </> : ""}
                        </>
                        : null}

                </Grid>
                <Grid xs={4} item>
                    {/*<div className={classes.titleFollowers}>دنبال شونده</div>*/}
                    {/*<div className={classes.numFollowers}>{info.following}</div>*/}
                </Grid>
                <Grid xs={12} item>
                    <div className={classes.fullName}>{info.fullName}</div>
                    <div className={classes.jobTitle}>{info.jobTitle}</div>
                </Grid>
                <Grid xs={12} item>
                    <KmStatusMe userId={userId} inProfile={true}/>
                </Grid>
                <Grid xs={12} item>
                    {JSON.parse(localStorage.getItem("user")).userId === userId ?
                        "" : ""
                        // <Button className={classes.buttonFollow} variant={"contained"}>دنبال کردن</Button>
                    }
                </Grid>
            </Grid>
        </>
    );
}

export default PersonInfo;