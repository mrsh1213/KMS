import React, {useContext, useRef, useState} from 'react';
import {useSnackbar} from "notistack";
import API from "../../config/API";
import {userService} from "../../config/constans";
import Dialog from "@material-ui/core/Dialog";
import Grid from "@material-ui/core/Grid";
import DevTextField from "../../components/common/DevTextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import Button from "@material-ui/core/Button";
import {BackDropContext} from "../../config/BackDropContext";
import {makeStyles} from "@material-ui/styles";

ChangePassword.propTypes = {};
const styles = makeStyles(() => ({
    textField: {
        "fontFamily": "'IranSans'",
        marginTop: 4,
        marginBottom: 4,
        height: 34
    },
    inputBaseFullWidth: {
        height: "100%"
    },
    btnSave: {
        "fontFamily": "'IranSans'",
        "height": "32px", "borderRadius": "5px",
    },
    btnCancel: {
        "fontFamily": "'IranSans'",
        "height": "32px", "borderRadius": "5px",
    },
}));

function ChangePassword({userId}) {
    const [password, setPassword] = useState("");
    const [rePassword, setRePassword] = useState("");
    const [currentPassword, setCurrentPassword] = useState("");
    const [showPassword, setShowPassword] = useState(false);
    const [showDialogChangePassword, setShowDialogChangePassword] = useState(false);
    const classes = styles();
    const {dispatch} = useContext(BackDropContext);

    const {enqueueSnackbar} = useSnackbar();

    function handlePassword(e) {
        setPassword(e.target.value)
    }

    function handleRePassword(e) {
        setRePassword(e.target.value)
    }

    function handleCurrentPassword(e) {
        setCurrentPassword(e.target.value)
    }

    function handleClickShowPassword() {
        setShowPassword(prevState => !prevState);
    }

    const handleOpenChangePassword = () => {
        setShowDialogChangePassword(true);
    };
    const handleCloseChangePassword = () => {
        setCurrentPassword("");
        setPassword("");
        setRePassword("");
        setShowPassword(false);
        setShowDialogChangePassword(false);
    };

    function submitFormChangePassword() {
        if (password && rePassword === password && currentPassword) {
            API({
                method: "PUT",
                data: {password: password, oldPassword: currentPassword},
                url: userService.putChangePassword(localStorage.getItem("userId"))
            }, dispatch).then(res => {
                if (res.status === 200) {
                    handleCloseChangePassword();
                    enqueueSnackbar("رمزعبور با موفقیت تغییر کرد", {
                        variant: "success", anchorOrigin: {
                            vertical: 'bottom',
                            horizontal: 'center',
                        }
                    });
                } else {
                    console.log(res);
                }
            }).catch((e) => {
                enqueueSnackbar(e.response.data.message, {
                    variant: "error", anchorOrigin: {
                        vertical: 'bottom',
                        horizontal: 'center',
                    }
                });
            });
        } else {
            enqueueSnackbar("رمزعبور یکسان نمی باشد", {
                variant: "error", anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'center',
                }
            });
        }
    }

    return (
        JSON.parse(localStorage.getItem("user")).userId === userId ?
            <>
                <Button onClick={handleOpenChangePassword} variant={"outlined"} color={"primary"}>تغییر رمزعبور</Button>
                <Dialog
                    fullWidth
                    open={showDialogChangePassword}
                    onClose={handleCloseChangePassword}
                >
                    <Grid style={{padding: 8}} spacing={1} container>
                        <Grid xs={12} item><h6>تغییر رمز عبور</h6></Grid>
                        <Grid xs={12} item>
                            <DevTextField
                                autoComplete={false}
                                margin={"none"}
                                className={classes.textField}
                                color={"primary"}
                                variant="outlined"
                                onChange={handleCurrentPassword}
                                required
                                fullWidth
                                name="currentPassword"
                                label="رمزعبور فعلی"
                                value={currentPassword}
                                type={showPassword ? 'text' : 'password'}
                                id="currentPassword"
                                InputProps={{
                                    classes: {input: classes.inputBaseFullWidth},
                                    endAdornment: <InputAdornment position="end">
                                        <IconButton
                                            aria-label="currentPasswordToggle"
                                            onClick={handleClickShowPassword}
                                            edge="end"
                                        >
                                            {showPassword ? <Visibility/> : <VisibilityOff/>}
                                        </IconButton>
                                    </InputAdornment>
                                }}
                                labelwidth={70}
                            />
                        </Grid>
                        <Grid xs={12} item>
                            <DevTextField
                                autoComplete={false}
                                margin={"none"}
                                className={classes.textField}
                                color={"primary"}
                                variant="outlined"
                                onChange={handlePassword}
                                required
                                fullWidth
                                name="changePassword"
                                label="رمزعبور جدید"
                                value={password}
                                type={showPassword ? 'text' : 'password'}
                                id="changePassword"
                                InputProps={{
                                    classes: {input: classes.inputBaseFullWidth}
                                }}
                                labelwidth={70}
                            />
                        </Grid>
                        <Grid xs={12} item>
                            <DevTextField
                                autoComplete={false}
                                margin={"none"}
                                className={classes.textField}
                                color={"primary"}
                                variant="outlined"
                                onChange={handleRePassword}
                                required
                                fullWidth
                                name="reChangePassword"
                                label="تکرار رمزعبور جدید"
                                value={rePassword}
                                type={showPassword ? 'text' : 'password'}
                                id="reChangePassword"
                                InputProps={{
                                    classes: {input: classes.inputBaseFullWidth},
                                }}
                                labelwidth={70}
                            />
                        </Grid>
                        <Grid xs={6} item/>
                        <Grid xs={3} item>
                            <Button onClick={handleCloseChangePassword} fullWidth classes={{root: classes.btnCancel}}
                                    color={"primary"}
                                    variant={"outlined"}>انصراف</Button>
                        </Grid>
                        <Grid xs={3} item>
                            <Button onClick={submitFormChangePassword} color={"primary"} fullWidth
                                    classes={{root: classes.btnSave}}
                                    variant={"contained"}>تایید</Button>
                        </Grid>
                    </Grid>
                </Dialog>
            </> : null
    );
}

export default ChangePassword;