import React, {useCallback, useEffect, useState} from 'react';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import {Close, Done, Edit, Image} from "@material-ui/icons";
import {makeStyles} from "@material-ui/styles";
import API from "../../config/API";
import {userService} from "../../config/constans";
import IconButton from "@material-ui/core/IconButton";
import {useSnackbar} from "notistack";

ContactInfo.propTypes = {};
const styles = makeStyles(() => ({
    listItemTextRoot: {

        display: "flex",
        flexDirection: "column-reverse"

    },
    listItemTextSecondary: {
        "fontFamily": "'IranSans'",
        "fontSize": "10px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "color": "#42516e"
    },
    listItemTextPrimary: {
        "fontFamily": "'IranSans'",
        "fontSize": "12px",
        "fontWeight": "bold",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.58",
        "letterSpacing": "normal",
        "color": "#42516e"
    },
    cardAvatarGreen: {
        padding: 8,
        "width": "48px", "height": "48px", "borderRadius": "10px", "backgroundColor": "#e3fcef"
    },
    cardAvatarBlue: {
        padding: 8,
        "width": "48px", "height": "48px", "borderRadius": "10px", "backgroundColor": "#deebfe"
    },
    cardAvatarOrange: {
        padding: 8,
        "width": "48px", "height": "48px", "borderRadius": "10px", "backgroundColor": "#feeae5"
    },
    cardAvatarPurple: {
        padding: 8,
        "width": "48px", "height": "48px", "borderRadius": "10px", "backgroundColor": "#eae6ff"
    },
}));
const styledEdit = {
    maxHeight: 25,
    background: "#fff",
    padding: 3,
    fontWeight: "bold",
    borderBottom: "1px solid #757575",
    overflowWrap: "anywhere"
};

function ContactInfo(props) {
    const [contactInfo, setContactInfo] = useState({
        popakId: "",
        email: "",
        officeAutomationMail: "",
        internalPhone: ""
    });
    const [editMode, setEditMode] = useState(false);
    const {userId} = props;
    const {enqueueSnackbar} = useSnackbar();
    const classes = styles();
    const getContactInfoAPI = useCallback(() => {
        API({url: userService.getContactInfoByUid(userId)}).then(res => {
            if (res.status === 200) {
                setContactInfo(res.data)
            }
        });
    }, [userId]);

    useEffect(() => {
        getContactInfoAPI();
    }, [getContactInfoAPI]);

    function updateContactInfo() {
        let cInfo = {
            popakId: document.getElementById("popakId").innerText,
            email: document.getElementById("email").innerText,
            officeAutomationMail: document.getElementById("officeAutomationMail").innerText,
            internalPhone: document.getElementById("internalPhone").innerText
        };
        API({
            method: "PUT",
            url: userService.putContactInfoByUid(localStorage.getItem("userId")),
            data: cInfo
        }).then(res => {
            if (res.status === 200) {
                setEditMode(false);
                getContactInfoAPI();
            }
        }).catch(err => {
            if (err.response.status === 400) {
                enqueueSnackbar(err.response.data.message, {
                    variant: "error", anchorOrigin: {
                        vertical: 'bottom',
                        horizontal: 'center',
                    }
                });
            }
        })
    }

    return (<>
            {JSON.parse(localStorage.getItem("user")).userId === userId ?
                <div style={{textAlign: editMode ? "left" : undefined}}>
                    {editMode ? <div>
                            <IconButton size={"small"} onClick={() => setEditMode(false)}><Close
                                style={{fontSize: 22}}/></IconButton>
                            <IconButton size={"small"} onClick={updateContactInfo}><Done
                                style={{fontSize: 22}}/></IconButton>
                        </div>
                        : <IconButton size={"small"} onClick={() => {
                            setEditMode(prevState => !prevState)
                        }}> <Edit style={{fontSize: 22}}/></IconButton>}</div> : null}
            <List component="nav" aria-label="secondary mailbox folders">
                <ListItem disableGutters button={!editMode}>
                    <ListItemAvatar>
                        <Avatar
                            src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAGC0lEQVR4Ab3YA5AkTRMG4ByuZm377LU/LD7btnW2bdu2bdu2bRv5z9u9XRH9z/TqMBHvGc9mZlV1NeX991axQjxJRzvLp9K2Mu1pY+xSa7bT6uhrtDT8Li0JP00LQjbTvOC+NCv4Kxrn71bcf7/of/BClhPtrViP9lU6Q9vLMm0qJWdtLNPqKKZl4VKsIDmzApmm+9+myX5DaUZAzDMF0bG4zwCxgph2lZcQxrYJbH4nm50Tc9kx6h0pLnHvskN2Dpv+SmX9qDCmKb5yxvk8oTFe7WjzS6anAuEfoKPVutORqkwHK7N+aaKEcKIPrflEigt9zhb60ppv2JW+l+JGP0kB0NghNh/kwTTSfQ0N9AgsNki06FT8PDpRTcLgq3ZyfE/CIB5lcqUA5PFuEiqlwrjTr+xBv1vzJ7u8+jrTYHemEdYMczsKVPFBF5OHAqPbGS+1wZEkjKiOR+jbHLOBOGSMOwe1C2XfeTr2+6GSCuNJf1rzD3vRf+wZ/Tnre/vIsAGW7VoDr4FJqkFnEhitwleuYFCRaivMXHqBkaN6hkg/rrBax+ErdPz2CgP7TtehWvkgGYN4Uw1rarFn0M8yaoAzUy+HAUUC0dX0CDqbcB8g08fZwCCiMqhC5X3E5XfouOwGHX+9xsjvrzLwJ6uNUoByC/0S1UFlgBEgH6rLbokfyaDejkx9HLILB6FV5xLZMDRDYMI+TOIKvcO56hxPrrbGxK9v1QMiEPgxgip9sNzEjn/HKq1SYXypgRTHn6rKoG6mtQWCUB1ruxjVwTJWKhP0Sgan7XXg1JPE8UdIqlDmZhn1Yz4sZZmO45YQZ1i/rzzdzJ7mvwESGAXkR43Zx/9vGdTDzNTJmKEJwuwAZByYZTPEqFL2QT1/sREIOQC9slwPCILZQvBrGGK7GMSfmqFKAEmzpA26lLwKIOw1Cij6mwRO32LhrAskVSjtkE5pmYAhmCNAlEqlN8nld7L6q5MxRCQ0/Uem7iamLsYjNiCx71xORrvEflOmViXOu6FDAFK1TBlqzBRWG9qE6nyy5lXefH0d///nxPGrPKzfev7xwwlcxqcDB7g0lEGdjdbvHaJtQJgfgPQbUpU2if0GsLhZvjYg7ENY8inLYqTdetT8WQLw4MEjnjdjD9f9cwbHRXYUrQqgVvlpy8ZGfjKokyHPFnQ9NQsgw7R0gGyOBSWYjZe/+0Oqwt472/nWo5sS4NCBcwKDSgCBuVEgMqalwARRB6xGGdRB/7W9Cr0HEJa7GmN7TvXptoi1Pr06rrQ3xCpMILUHSB7sTnpUqIbdCmGgUSFgtA5N7MKrlh2yi0GbIt2bAoMUiAmmzqgQQBoVupJZESCc6BoYAdq66YQ9D35dAyNACgbBDAGk2ovUjxoXk+9ilVlcPwdGgNSn9x+aoO2bT2m0qq2CUYEwPwDh9Nfeh7Dsc95TYRQQMDg0tUCPHz/Bki60VSHUlX0r/KIM9CU8Fmvu1ABhpxatsn2c0ALhg/kqFINgoOU9yNRFe6e+kuQvnfRHquLEtmkVMIWAxEorCBPs1hoYOV1NFQs+7U8n9MKDGaoEDCIw8gleKAity6ky1A4G6Y7qoDKFn/aiSsfirqFKTt+9k4/5U/U4AVBhnwnDdwIEDCIw3onfS4eqrovTI1SnMJC4ZQCEW4bl/Q9tnm1wLhX2OXroKqqjwvjFVpcgAGEzLNYzNW4beLjHtceS95kKhOVdFJBNZXo7ImjXPKys4oFwQ91feRCqhIuh039vsI+5Jo6FQkCiZcAgqDIeW5Vn6bHY80p8UcRtFSDEML0K/vFCQThCMNRokal1DG4aEgZLXFSmpCAEd3nc4ZWr89bdxwtcYf/9NY0tP2XL9zBksOUMDbR88Gzv9mghUAWAMOwfdGus3FSVi+EAcQd7liAEbzvsgfbtP8e/jezM5kkRLO7yE7wX0QSfFPH3nxtodZQArdpwgN8fWQdvOhAJUwjkGYPWx0igVjNGc8qE79SvXmYGTgZEae8LBYl3QXhJNS+kX2Hvf54/aEXkZVoS3sL6Y79ngSg5aF3sEFoV/TeuTM8YIvI/Lv6InAAhonsAAAAASUVORK5CYII="}
                            variant={"square"} className={classes.cardAvatarGreen}>
                            <Image/>
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText classes={{
                        root: classes.listItemTextRoot,
                        primary: classes.listItemTextPrimary,
                        secondary: classes.listItemTextSecondary
                    }}
                                  primary={<div id="popakId" onKeyPress={(e) => {
                                      if (e.key === "Enter") {
                                          e.preventDefault();
                                      }
                                  }} suppressContentEditableWarning
                                                style={editMode ? styledEdit : undefined}
                                                contentEditable={editMode}>{contactInfo.popakId}</div>}
                                  secondary="شناسه پوپک"/>
                </ListItem>
                <ListItem disableGutters button={!editMode}>
                    <ListItemAvatar>
                        <Avatar
                            src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAXpJREFUWEftl01SwkAQhV9PdC2eQDdhKzeQo+AJdA8pkgL2egI5Ct4At8mGI8Q1Ms+aWEkBEjL5QVgky6S75/XX3VMdwZkfOfP5aAVkBO59dq6v9BiUgQg6pygNiRjC+fpbBStfYnNGJsCdbF5F5PkUB+/HJPkWec7LjoDuVK8A3P2PAMSRp273BdC8IPEh4AIi40bFkAEhfRE8mrjhSCX0sxJ0pzoTEHmq787YE805BA+1hBCfVDKIhrJ0J3phLSA9tDvZ+JVpkEHoOX4ay0pAYizih0MJMseyNLayzksktwSpA4ElRJ4MulI09rOesQfyXYDedikLBWTGtjQssq4mwExGEQ3LrCsLOEbDfCtdpmNjWDRyh2gYHzO2h2qdF8++B/IibPVGlVGtLyDtjd/bbKfDiygevQltnJuwaYRAHSGtgJbA5RFwpzoW4KZOZ9v6EviKRipZfC9nKU3Wckeb7WdwKhImc5Dz9Ub5f9ZyW3xN27W/Zmcn8AOFef8hRDDUqAAAAABJRU5ErkJggg=="}
                            variant={"square"} className={classes.cardAvatarBlue}>
                            <Image/>
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText classes={{
                        root: classes.listItemTextRoot,
                        primary: classes.listItemTextPrimary,
                        secondary: classes.listItemTextSecondary
                    }}
                                  primary={<div id="email" onKeyPress={(e) => {
                                      if (e.key === "Enter") {
                                          e.preventDefault();
                                      }
                                  }} suppressContentEditableWarning
                                                style={editMode ? styledEdit : undefined}
                                                contentEditable={editMode}>{contactInfo.email}</div>}
                                  secondary="پست الکترونیک"/>
                </ListItem>
                <ListItem disableGutters button={!editMode}>
                    <ListItemAvatar>
                        <Avatar
                            src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAixJREFUWEfFl11u2lAQhc+xkpfaUrOD1mQBoSp+LtkBXUG8BGcFZAchKyjZATsoeTaVzAKAZAdUwlUlIk5lG8KfqS+QxLyhmTvz3XNn5l4TJf9Ycn6UDzCq4mx2at8S9N9TDUFtaxpfc+jZbYBX75l8mUv3HNScMYmPpQAIYw49R6UknyctBJDwG1QbwiPAM0I+yE/b0OpTDEw2I6oF8CLx/T+A9ESi4YZxtAicFK1O7STAet0ID5XepJ7Y3QjjPJCFbVhzuiC+FQJQuHR7k+5msBTixI7WlJgDDD07IuCvQifrR55dFfCjEsZfzACkp0ov/rxL0kHNuSHRfLHPAQae3YLoW1TTDeO7xD6o2QHBpqj78zAODAEySXcD2AHJ202AdLdfPzRmtNoAfs7tl5ZmvvvrTyf5bwYARYlc+yqwVisnTpqQz5PGal0YAgCcPrtu9PcxD2I1SGqfH4FJFxgDSOic9ybfN4OmZ7oq/1sBZIkVEQzccPKQVPJMuCJz+v0tFDCR81ifwkl4bIKi9YYA6kNMp5uA6mteXrkAi/lviZ3cSZjVQp1AkH8vFO17ad8CEHRnTeObXfN8ayx7tj8TW4eqsgYg6fq8F7fM+TPPrDvYPQRiBUD9ShhX902+8N+6GwwDLQH26OO82KOaUxdfZr9h+rX3QDZwjFduOaoqcO/jM2zDw7GKVpb6KE3avfxn+c43XpF2R9jTnVMdTuOg/E+zIzbyKktLV+AfhWQsXB6MxucAAAAASUVORK5CYII="}
                            variant={"square"} className={classes.cardAvatarOrange}>
                            <Image/>
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText classes={{
                        root: classes.listItemTextRoot,
                        primary: classes.listItemTextPrimary,
                        secondary: classes.listItemTextSecondary
                    }}
                                  primary={<div id="officeAutomationMail" onKeyPress={(e) => {
                                      if (e.key === "Enter") {
                                          e.preventDefault();
                                      }
                                  }} suppressContentEditableWarning
                                                style={editMode ? styledEdit : undefined}
                                                contentEditable={editMode}>{contactInfo.officeAutomationMail}</div>}
                                  secondary="پست الکترونیکی سازمانی"/>
                </ListItem>
                <ListItem disableGutters button={!editMode}>
                    <ListItemAvatar>
                        <Avatar
                            src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAjNJREFUWEfFV0ty2kAQfT0uZxvfIPENyAkiIPtIVFmqrExOEHyCkBOY3IAswVVIBwiyfAKTG+AbmK1dzEsNBS4gI41EEauXmunu17/XI0HNIjX7R/0AfC8+U+r5GoLuq2aDGGp9eiVBezQUyOWrOl87I/hLOu3xI4C3tQAgHg0A1uF847MMgAUJU6Y5oM8A6ULwrgLoBYFMgM82nWIAxIPW8JMsnG2UTdPKydOgZN8s9BKe0c/rtUIAWqSZ/L7I9pGvJ2fmzATxdZKGw42+DUQ+AOJhkobv81LdaY36EPmed07iLk5Db/vc98YNdYL77W+5AGwGthU7rZsehNd5APaztyqderoXkZ2g8gEAs3gafjgoA5bsBa3xQATf9u0V98ByeZ5kX+Y2EEFrnIngo+3MEEw8jbp+e+QrSFw0MY4xZDKZRsG+AVf6Qf6YpFHf1SfGrpMHCMyg0Ytvw7tVEyleQqRXFJUGg2QaJWVo3gmgAuG8XK0dwFFLYMIi+QcQs7QggoZreR2jCVf8TyWJnQlNL4gHsGdjQ5LzOI3Ot8tXegxJ/KQ+7SdZsIrYJZ3W2CynwX5WbESk1PM/9L3bhJSrSXphjFWSNcWanfHyrjAbMJ6GzdJUbOocp5Gp70Finfkqy8jF/S5U/qcbT5G32/dIPFKjWWodbwjH5SjvnAoNhVUv7IgBIcIMEN+m+1+IqEoQtT5KASzqf5ZXfONVyW7R3QXBhMs3vfp/zY4V0qF2as/AX31/QbvRCztNAAAAAElFTkSuQmCC"}
                            variant={"square"} className={classes.cardAvatarPurple}>
                            <Image/>
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText classes={{
                        root: classes.listItemTextRoot,
                        primary: classes.listItemTextPrimary,
                        secondary: classes.listItemTextSecondary
                    }} primary={<div id="internalPhone" onKeyPress={(e) => {
                        if (e.key === "Enter") {
                            e.preventDefault();
                        }
                    }} suppressContentEditableWarning
                                     style={editMode ? styledEdit : undefined}
                                     contentEditable={editMode}>{contactInfo.internalPhone}</div>}
                                  secondary="تلفن سازمانی"/>
                </ListItem>

            </List>
        </>
    );
}

export default ContactInfo;