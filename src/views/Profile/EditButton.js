import React from 'react';
import IconButton from "@material-ui/core/IconButton";
import {Close, Done, Edit} from "@material-ui/icons";

EditButton.propTypes = {};


function EditButton(props) {
    const {editMode, setEditMode, handleEdit} = props;
    return (
        <div style={{float: "left", marginBottom: 12}}>
            {editMode ? <div>
                <IconButton size={"small"} onClick={() => setEditMode(false)}><Close
                    style={{fontSize: 22}}/></IconButton>
                <IconButton
                    size={"small"}
                    onClick={handleEdit}
                    aria-label="done"
                >
                    <Done style={{fontSize: 22}}/>
                </IconButton></div> : <IconButton
                size={"small"}
                onClick={() => {
                    setEditMode(true);
                }}
                aria-label="edit"
            >
                <Edit style={{fontSize: 22}}/>
            </IconButton>}
        </div>
    );
}

export default EditButton;