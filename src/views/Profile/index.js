import React from 'react';
import Grid from "@material-ui/core/Grid";
import PersonInfo from "./PersonInfo";
import DevCardCollapse from "../../components/common/DevCardCollapse";
import Skills from "./Skills";
import ContactInfo from "./ContactInfo";
import About from "./About";
import {useQuery} from "../../config/Helper";
import ChangePassword from "./ChangePassword";

Index.propTypes = {};

function Index(props) {
    // const [countHashTags, setCountHashTags] = useState("0");
    const userId = useQuery().get("uId");
    // function getCountHashTags(count) {
    //     setCountHashTags(count);
    // }

    return (
        <Grid container>
            <Grid xs={12} item>
                <PersonInfo userId={userId}/>
            </Grid>
            {/*<Grid xs={12} item>*/}
            {/*    <DevCardCollapse*/}
            {/*        title={<span>موضوعات دنبال شده <span style={{*/}
            {/*            color: "#fff",*/}
            {/*            fontFamily: "'IranSansFaNum'",*/}
            {/*            "borderRadius": "20px",*/}
            {/*            padding: "1px 6px",*/}
            {/*            "backgroundColor": "#2684fe"*/}
            {/*        }} children={countHashTags}/></span>}>*/}
            {/*        <HashTagsFollowed userId={userId} getCountHashTags={getCountHashTags}/>*/}
            {/*    </DevCardCollapse>*/}
            {/*</Grid>*/}
            <Grid xs={12} item>
                <DevCardCollapse
                    title={"درباره"}>
                    <About userId={userId}/>
                </DevCardCollapse>
            </Grid>
            <Grid xs={12} item>
                <DevCardCollapse
                    title={"مهارت‌ها"}>
                    <Skills userId={userId}/>
                </DevCardCollapse>
            </Grid>
            <Grid xs={12} item>
                <DevCardCollapse title={"اطلاعات تماس"}>
                    <ContactInfo userId={userId}/>
                </DevCardCollapse>
            </Grid>
            {JSON.parse(localStorage.getItem("user")).userId === userId ? <Grid xs={12} item>
                <DevCardCollapse title={"تنظیمات"}>
                    <ChangePassword userId={userId}/>
                </DevCardCollapse>
            </Grid> : null}
        </Grid>
    );
}

export default Index;