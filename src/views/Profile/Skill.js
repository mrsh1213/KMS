import React, {useCallback, useState} from 'react';
import LinearProgress from "@material-ui/core/LinearProgress";
import {makeStyles} from "@material-ui/styles";
import {Close, Delete, Done, Edit} from "@material-ui/icons";
import Slider from "@material-ui/core/Slider";
import IconButton from "@material-ui/core/IconButton";
import API from "../../config/API";
import {userService} from "../../config/constans";
import DevDialog from "../../components/common/DevDialog";
import Button from "@material-ui/core/Button";
import DevTextField from "../../components/common/DevTextField";

Skill.propTypes = {};
const styles = makeStyles(() => ({
    cardSkill: {
        marginBottom: 16,
        textAlign: "center"
    },
    titleSkill: {
        fontFamily: "'IranSans'",
        "fontSize": "12px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "textAlign": "left",
        "fontStyle": "normal",
        "lineHeight": "1.58",
        "letterSpacing": "normal",
        "color": "#42516e"
    },
    progress: {
        backgroundColor: "#2684fe", height: 6
    }
}));
const marks = [
    {
        value: 0,
        label: 'آشنایی',
    },
    {
        value: 25,
        label: 'کم',
    },
    {
        value: 50,
        label: 'متوسط',
    },
    {
        value: 75,
        label: 'خوب',
    },
    {
        value: 100,
        label: 'عالی',
    },
];


const styledEdit = {
    background: "#fff",
    padding: 3,
    fontWeight: "bold",
    borderBottom: "1px solid #757575",
    overflowWrap: "anywhere",
    marginBottom: 32
};

function Skill(props) {
    const {skill, userId, handleEdited} = props;
    const [editMode, setEditMode] = useState(false);
    const [openConfirm, setOpenConfirm] = useState(false);
    const [rate, setRate] = useState(skill.rate);
    const [titleSkill, setTitleSkill] = useState(skill.name);
    const classes = styles();

    const handleCloseConfirm = useCallback(() => {
        setOpenConfirm(false);
    }, []);
    const cancelUpdateSkill = useCallback(() => {
        setEditMode(false);
        handleEdited();
    }, [handleEdited]);
    const updateSkill = useCallback(() => {
        if (titleSkill.trim()) {
            API({
                method: "PUT",
                url: userService.putSkillsByUid(localStorage.getItem("userId")),
                data: {skillId: skill.skillId, name: titleSkill, rate: rate}
            }).then(res => {
                if (res.status === 200) {
                    setEditMode(false);
                    handleEdited();
                }
            })
        } else {
            alert("فیلد عنوان الزامی میباشد")
        }
    }, [titleSkill, rate, skill, handleEdited]);
    const deleteSkill = useCallback(() => {
        API({
            method: "DELETE",
            url: userService.deleteSkillBySid(skill.skillId),
        }).then(res => {
            if (res.status === 200) {
                handleEdited();
            }
        })
    }, [skill, handleEdited]);

    function onBlurTitleSkill(e) {
        setTitleSkill(e.target.value)
    }

    function handleChangeSlider(e, newValue) {
        if (newValue !== rate) {
            setRate(newValue);
        }
    }

    return (
        <div className={classes.cardSkill}>

            <div style={{textAlign: editMode ? "left" : "right"}}>
                {editMode ? <>
                        <div style={{textAlign: "center"}}>
                            {/*<div onBlur={onBlurTitleSkill} style={styledEdit} contentEditable={"true"}*/}
                            {/*     suppressContentEditableWarning={true}*/}
                            {/*     className={classes.titleSkill}>{titleSkill}</div>*/}
                            <DevTextField
                                fullWidth
                                margin={"none"}
                                InputProps={{
                                    classes: {input: classes.inputBaseFullWidth}
                                }}
                                color={"secondary"}
                                variant="outlined"
                                label="عنوان مهارت را بنویسید"
                                value={titleSkill}
                                onChange={onBlurTitleSkill}
                            />
                            <Slider
                                value={rate}
                                onChange={handleChangeSlider}
                                style={{width: "90%"}}
                                aria-labelledby="discrete-slider-always"
                                step={25}
                                marks={marks}
                                valueLabelDisplay={"on"}
                                valueLabelFormat={() => "سطح"}
                            />
                        </div>
                        <IconButton size={"small"} onClick={cancelUpdateSkill}><Close
                            style={{fontSize: 22}}/></IconButton>
                        <IconButton size={"small"} onClick={updateSkill}><Done
                            style={{fontSize: 22}}/></IconButton> </> :
                    <>
                        <div className={classes.titleSkill}>{skill.name}</div>
                        <LinearProgress style={{width: "100%"}} aria-valuemax={100} aria-valuemin={0}
                                        variant="determinate" value={rate}/>
                        {JSON.parse(localStorage.getItem("user")).userId === userId ?
                            <>
                                <DevDialog handleClose={handleCloseConfirm} open={openConfirm}>
                                    <div>{`آیا مایل به حذف مهارت ${titleSkill} هستید؟`}</div>
                                    <hr/>
                                    <div style={{display: "flex"}}>
                                        <Button size={"small"} autoFocus onClick={handleCloseConfirm}
                                                variant={"outlined"}>
                                            انصراف
                                        </Button>
                                        <div style={{marginLeft: "8px"}}/>
                                        <Button color={"primary"} size={"small"} onClick={deleteSkill}
                                                variant={"contained"} autoFocus>
                                            حذف
                                        </Button>
                                    </div>
                                </DevDialog>
                                <IconButton size={"small"} onClick={() => {
                                    setEditMode(true)
                                }}> <Edit style={{fontSize: 22}}/></IconButton>
                                <IconButton size={"small"} onClick={() => {
                                    setOpenConfirm(true);
                                }}> <Delete style={{fontSize: 22}}/></IconButton>
                            </> : null}
                    </>
                }
            </div>

        </div>
    );
}

export default Skill;