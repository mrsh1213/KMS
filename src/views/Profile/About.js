import React, {useCallback, useEffect, useState} from 'react';
import EditButton from "./EditButton";
import API from "../../config/API";
import {userService} from "../../config/constans";
import {useSnackbar} from "notistack";

About.propTypes = {};
const styledEdit = {
    background: "#fff",
    padding: 3,
    fontWeight: "bold",
    borderBottom: "1px solid #757575",
    overflowWrap: "anywhere"
};

function About(props) {
    useState();
    const [editMode, setEditMode] = useState(false);
    const {userId} = props;
    const [about, setAbout] = useState("");
    const {enqueueSnackbar} = useSnackbar();

    const getAboutAPI = useCallback(() => {
        API({url: userService.getAboutMeByUid(userId)}).then(res => {
            if (res.status === 200) {
                setAbout(res.data["aboutMe"])
            }
        });
    }, [userId])

    useEffect(() => {
        getAboutAPI();
    }, [getAboutAPI]);

    function handleEdit() {
        if (about.length <= 2500 && about.length >= 10) {
            API({
                method: "PUT",
                url: userService.putAboutMeByUid(localStorage.getItem("userId")),
                data: {aboutMe: about}
            }).then(res => {
                if (res.status === 200) {
                    setEditMode(prevState => !prevState);
                    getAboutAPI();
                }
            })
        } else {
            enqueueSnackbar("متن درباره من باید بین 10 الی 100 کاراکتر باشد", {
                variant: "error", anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'center',
                }
            });
        }

    }

    return (
        <>

        <div style={editMode ? styledEdit : undefined} contentEditable={editMode} onBlur={(e) => {
                setAbout(e.target.innerText)
            }} suppressContentEditableWarning={true}>
                {about}
            </div>
            {(localStorage.getItem("userId") === userId) &&
            <EditButton handleEdit={handleEdit} editMode={editMode} setEditMode={(value) => {
                setEditMode(value)
            }}/>}

        </>
    );
}

export default About;