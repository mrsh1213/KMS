import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/styles";
import Chip from "@material-ui/core/Chip";

HashTagsFollowed.propTypes = {};
const styles = makeStyles(() => ({
    hashTag: {
        "fontFamily": "'IranSans'",
        height: 16,
        borderRadius: 5,
        backgroundColor: "#deebfe",
        "&:hover": {
            backgroundColor: "#deebfe",
        },
        "fontSize": "10px",
        "marginRight": "5px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.75",
        "letterSpacing": "normal",
        "textAlign": "right"
    }
}));

function HashTagsFollowed(props) {
    const [hashTags, setHashTags] = useState([]);
    const {getCountHashTags} = props;
    const classes = styles();
    useEffect(() => {
        getHashTagsAPI();
    }, [])
    useEffect(() => {
        getCountHashTags(hashTags.length);
    }, [hashTags, getCountHashTags]);


    function getHashTagsAPI() {
        setTimeout(() => {
            setHashTags([
                {
                    hashTagId: "1",
                    hashTagName: "docker"
                }, {
                    hashTagId: "2",
                    hashTagName: "admin"
                }, {
                    hashTagId: "3",
                    hashTagName: "مدیریت عامل"
                }, {
                    hashTagId: "4",
                    hashTagName: "ReactJs"
                }, {
                    hashTagId: "5",
                    hashTagName: "جاواسکریپت"
                },]);
        }, 50);
    }

    return (
        <div style={{textAlign: "justify"}}>
            {hashTags.map(hashTag => <Chip key={hashTag.hashTagId} className={classes.hashTag}
                // onClick={hashTagClick}
                                           label={hashTag.hashTagName}
                                           variant={"default"}
                                           color="secondary" size="small"/>)
            }
        </div>
    );
}

export default HashTagsFollowed;