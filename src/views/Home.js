import React from 'react';
import Button from "@material-ui/core/Button";
import {useHistory} from "react-router";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import {Hidden} from "@material-ui/core";
import SystemStatusHome from "./common/SystemStatusHome";

Home.propTypes = {};
const useStyles = makeStyles((theme) => ({
    root: {
        textAlign: "center",
        margin: 16,
        [theme.breakpoints.up('sm')]: {margin: 0}
    },
    appBar: {
        marginBottom: 10,
        color: "#000",
        backgroundColor: "#fafbfd"
    },
    menuItem: {
        marginLeft: 32,
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "500",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.57",
        "letterSpacing": "normal",
        "textAlign": "right",
        "color": "#455a64"
    },
    item: {
        color: "#455a64"
    },
        btnLogin: {
            "fontFamily": "'IranSans'",
            "boxShadow": "0 3px 6px 0 rgba(0, 100, 209, 0.2)",
            "backgroundColor": "#0052cc !important",
            "fontSize": "14px",
            "fontWeight": "500",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.57",
            "letterSpacing": "normal",
            "textAlign": "right",
            "color": "#ffffff"
        },
        sainaSub: {
            "fontFamily": "'IranSans'",
            "fontSize": "9px",
            "fontWeight": "600",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.5",
            "letterSpacing": "normal",
            "textAlign": "center",
            "color": "#455a64"
        },
        saina: {
            "fontFamily": "'IranSans'",
            "fontSize": "18px",
            "fontWeight": "600",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.54",
            "letterSpacing": "normal",
            "textAlign": "center",
            "color": "#0052cc"
        },
        logo: {
            display: "flex",
            alignItems: "center"
        },
        title: {
            "fontFamily": "'IranSans'",
            textAlign: "center",
            "fontWeight": "900",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.56",
            "letterSpacing": "normal",
            "color": "#0052cc",
            "fontSize": "5vw",
            [theme.breakpoints.up('sm')]: {
                "fontSize": "3vw !important",
            }
        },
        subtitle: {
            "fontFamily": "'IranSans'",
            "fontWeight": "bold",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.54",
            "letterSpacing": "normal",
            "textAlign": "center",
            "color": "#455a64",
            "fontSize": "3.7vw",
            [theme.breakpoints.up('sm')]: {
                "fontSize": "2.1vw !important",
            }
        },
        content: {
            direction: "ltr",
            wordSpacing: "-2px",
            "fontFamily": "'IranSans'",
            "fontSize": "16px",
            "fontWeight": "500",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.56",
            "letterSpacing": "normal",
            "textAlign": "justify",
            "color": "#455a64",
            padding: 16
        }
    }))
;

function Home(props) {
    const history = useHistory();
    const classes = useStyles();

    function handleClickSignIn() {
        history.push("/signIn")
    }

    return (
        <div className={classes.root}>
            <AppBar elevation={0} className={classes.appBar} position="static">
                <Toolbar variant={"dense"}>
                    {/*<Hidden mdUp implementation="css">*/}
                    {/*    <IconButton edge="start" className={classes.menuButton}*/}
                    {/*                color="inherit" aria-label="menu">*/}
                    {/*        <Menu/>*/}
                    {/*    </IconButton>*/}
                    {/*</Hidden>*/}
                    <div className={classes.logo}>
                        <img width={55} height={48} alt="logo-gray" src={"/icons/bgGray3x.png"}/>
                        <span>
                            <div className={classes.saina}>ســـــاینا</div>
                        <div className={classes.sainaSub}>
                            <div> سیستم اشتراک‌گــــذاری</div>
                            <div>یافته‌هـای ناب اطلاعاتی</div>
                        </div>
                            </span>
                    </div>
                    <Hidden smDown>
                        <div className={classes.menuItem}><a className={classes.item} href="#main">صفحه اصلی</a></div>
                        <div className={classes.menuItem}><a className={classes.item} href="#feature">ویژگی‌</a></div>
                        <div className={classes.menuItem}><a className={classes.item} href="#about">درباره</a></div>
                        <div className={classes.menuItem}><a className={classes.item} href="#tellUs">تماس با ما</a>
                        </div>
                    </Hidden>
                    <div style={{flexGrow: 1}}/>
                    <Button onClick={handleClickSignIn} className={classes.btnLogin} variant={"contained"}>ورود</Button>
                </Toolbar>
            </AppBar>
            <Grid container>
                <Grid xs={12} sm={6} item>
                    <h3 className={classes.title}>
                        همچو سیمرغ که طوفان نبرد از جایش
                    </h3>
                    <h4 className={classes.subtitle}>سیستم اشتراک‌گذاری یافته‌های ناب اطلاعاتی</h4>
                    <div className={classes.content}>مدیریت دانش رویکردی یکپارچه به شناسایی، کسب و استخراج بازیابی،
                        ارزیابی، تسهیم و خلق کلیه منابع دانش سازمان است به گونه ای که سازمان را در جهت دستیابی به اهداف
                        سازمانی کمک نماید. هدف مدیریت دانش برقراری ارتباط بین خبرگان و افراد مجربه سازمان با افرادی است
                        که نیاز به دانش خاصی را دارند. ایجاد چنین ارتباطی به کمک فرایندها و ابزارهای مدیریت دانش تسهیل
                        می گردد. موفقیت در زمینه مدیریت دانش نیازمند ایجاد یک محیط جدید کاری است، که دانش و تجربه
                        بتوانند به راحتی تسهیم شوند.
                    </div>
                </Grid>
                <Grid xs={12} sm={6} item>
                    <img style={{padding: 50}} width={"100%"} alt="canape-saina" src="/images/canapeSaina.png"/>
                </Grid>

                <Grid xs={12} sm={12} item>
                    <h3 className={classes.title}>ویژگی‌ها</h3>
                    <div style={{textAlign: "left"}} className={classes.content}><span role="img"
                                                                                       aria-label="emoji-circle"> Cloud Native ⚫</span>
                    </div>
                    <div style={{textAlign: "left"}} className={classes.content}><span role="img"
                                                                                       aria-label="emoji-circle"> Microservice Architecture ⚫</span>
                    </div>
                    <div style={{textAlign: "left"}} className={classes.content}><span role="img"
                                                                                       aria-label="emoji-circle"> Progressive Web Application(PWA) ⚫
                    </span></div>
                </Grid>
                <Grid xs={12} sm={6} item>
                    <img style={{padding: 16}} width={"100%"} alt="feature" src="/images/feature.png"/>
                </Grid>
                <Grid xs={12} sm={6} item>
                    <img style={{padding: 16}} width={"100%"} alt="feature-option" src="/images/featureOption.png"/>
                </Grid>

                <Grid dir={"ltr"} xs={12} sm={12} item>
                    <h3 className={classes.title}>درباره</h3>
                    <div className={classes.content}>ساینا اسمی با اصالت پارسی و در زبان اوستایی به
                        معنای سیمرغ , پرنده اساطیری است که از بهشت به زمین آمده و نماد دانایی و خرد می باشد.
                    </div>
                </Grid>
                <Grid xs={12} sm={6} item>
                    <SystemStatusHome/>
                </Grid>
                <Grid xs={12} sm={6} item>
                    <img style={{padding: 16}} width={"100%"} alt="app-mobile" src="/images/appMobile.png"/>
                </Grid>
            </Grid>
        </div>
    );
}

export default Home;