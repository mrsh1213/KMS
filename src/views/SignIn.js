import React, {useEffect, useState} from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import {makeStyles} from '@material-ui/core/styles';
import {useHistory} from "react-router";
import DevTextField from "../components/common/DevTextField";
import {useSnackbar} from "notistack";
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import API from "../config/API";
import {userService} from "../config/constans";
import {Help, Home} from "@material-ui/icons";
import AddToHomeScreenPrompt from "../components/common/AddToHomeScreenPrompt";
import {fixNumbersToEn} from "../config/Helper";

const useStyles = makeStyles((theme) => ({
    gridRoot: {
        textAlign: "center",
        height: "100%"
    },
    appName: {
        "fontFamily": "'IranSans'",
        "fontSize": "18px",
        "fontWeight": "bold",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.56",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#344563",
        marginBottom: 30,
    },
    copyRight: {
        "fontFamily": "'IranSans'",
        "fontSize": "8px",
        "fontWeight": "500",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#6b778c"
    },
    linkCompany: {
        color: "#2684ff !important",
    },
    textField: {
        marginTop: 8,
        marginBottom: 8,
        height: 48
    },
    inputBaseFullWidth: {
        height: "100%"
    },
    signInButton: {
        "height": "48px", "borderRadius": "5px",
        "backgroundColor": "#0052cc !important",
        color: "#fff",
        marginTop: 24
    },
    container: {height: "100%", display: "flex", flexDirection: "column"},
    sainaSub: {
        "fontFamily": "'IranSans'",
        "fontSize": "16px",
        "fontWeight": "900",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#455a64"
    },
    saina: {
        "fontFamily": "'IranSans'",
        "fontSize": "30px",
        "fontWeight": "600",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.54",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#0052cc"
    }
}));

function SignIn() {
    const [password, setPassword] = useState("");
    const [showPassword, setShowPassword] = useState(false);
    const [username, setUsername] = useState("");
    const {enqueueSnackbar} = useSnackbar();
    const classes = useStyles();
    const history = useHistory();


    useEffect(() => {
        if (localStorage.getItem("user")) {
            history.push("/dashboard")
        }
    }, [history])

    function handleClickShowPassword() {
        setShowPassword(prevState => !prevState);
    }

    function handleRouteHome() {
        history.push("/")
    }

    function handleHelp() {
        window.open('/kms_help.pdf', '_self', 'fullscreen=yes');
    }

    function handlePassword(e) {
        setPassword(e.target.value)
    }

    function handleUsername(e) {
        setUsername(e.target.value);
    }

    function handleLogin() {
        if (password !== "" && username !== "") {
            API({
                data: {
                    "userName": fixNumbersToEn(username),
                    "password": password
                }, method: "POST", url: userService.postLogin()
            }).then(res => {
                if (res.status === 200) {
                    localStorage.setItem("user", JSON.stringify(res.data));
                    localStorage.setItem("userId", res.data.userId);
                    history.push("/dashboard");
                } else {
                    enqueueSnackbar("رمزعبور یا نام کاربری اشتباه می باشد", {
                        variant: "error", anchorOrigin: {
                            vertical: 'bottom',
                            horizontal: 'center',
                        }
                    });
                }
            }).catch(err => {
                enqueueSnackbar("رمزعبور یا نام کاربری اشتباه می باشد", {
                    variant: "error", anchorOrigin: {
                        vertical: 'bottom',
                        horizontal: 'center',
                    }
                });
            });
        } else {
            enqueueSnackbar("رمزعبور و نام کاربری را وارد نمایید", {
                variant: "warning", anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'center',
                }
            });
        }
    }

    /*function handleLogin() {
            if (password === "12345678" && username !== "") {
                API({url: userService.getInfoByUsername(fixNumbersToEn(username))}).then(res => {
                    if (res.status === 200) {
                        localStorage.setItem("user", JSON.stringify(res.data));
                        localStorage.setItem("userId", res.data.userId);
                        history.push("/dashboard");
                    } else {
                        enqueueSnackbar("رمزعبور یا نام کاربری اشتباه می باشد", {
                            variant: "error", anchorOrigin: {
                                vertical: 'bottom',
                                horizontal: 'center',
                            }
                        });
                    }
                }).catch(err => {
                    enqueueSnackbar("رمزعبور یا نام کاربری اشتباه می باشد", {
                        variant: "error", anchorOrigin: {
                            vertical: 'bottom',
                            horizontal: 'center',
                        }
                    });
                });
            } else {
                enqueueSnackbar("رمزعبور و نام کاربری را وارد نمایید", {
                    variant: "warning", anchorOrigin: {
                        vertical: 'bottom',
                        horizontal: 'center',
                    }
                });
            }
        }*/
    return (
        <AddToHomeScreenPrompt>
            <div className={classes.container}>
                <div style={{display: "flex"}}>
                    <div><IconButton onClick={handleRouteHome}><Home/></IconButton>
                        <div className={classes.copyRight} style={{textAlign: "center"}}>خانه</div>
                    </div>
                    <div style={{marginLeft: "auto"}}/>
                    <div>
                        <IconButton title={"راهنما"} onClick={handleHelp}><Help/></IconButton>
                        <div className={classes.copyRight} style={{textAlign: "center"}}>راهنما</div>
                    </div>
                </div>
                <Grid classes={{root: classes.gridRoot}} justify={"center"} alignContent={"center"}
                      alignItems={"center"}
                      container>
                    <Grid xs={12} item>
                        <img width={110} height={96} alt={"logoApp"} src="/icons/bgGray3x.png"/>
                    </Grid>
                    <Grid className={classes.appName} xs={12} item>
                        <div className={classes.saina}>ســـــاینا</div>
                        <div className={classes.sainaSub}>
                            <div> سیستم اشتراک‌گــــذاری</div>
                            <div>یافته‌هـای ناب اطلاعاتی</div>
                        </div>
                    </Grid>
                    <Grid xs={9} item>
                        <DevTextField
                            margin={"none"}
                            className={classes.textField}
                            InputProps={{
                                classes: {input: classes.inputBaseFullWidth}
                            }}
                            color={"secondary"}
                            variant="outlined"
                            required
                            fullWidth
                            id="username"
                            label="نام کاربری"
                            value={username}
                            onChange={handleUsername}
                            name="username"
                            autoComplete="username"
                            autoFocus
                        />
                    </Grid>
                    <Grid xs={9} item>
                        <DevTextField
                            margin={"none"}
                            className={classes.textField}
                            color={"secondary"}
                            variant="outlined"
                            onChange={handlePassword}
                            required
                            fullWidth
                            name="password"
                            label="رمز عبور"
                            value={password}
                            type={showPassword ? 'text' : 'password'}
                            id="password"
                            autoComplete="current-password"
                            InputProps={{
                                classes: {input: classes.inputBaseFullWidth},
                                endAdornment: <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        edge="end"
                                    >
                                        {showPassword ? <Visibility/> : <VisibilityOff/>}
                                    </IconButton>
                                </InputAdornment>
                            }}
                            labelwidth={70}
                        />
                    </Grid>
                    <Grid xs={12} item>
                        {/*<a aria-disabled={"true"} href="/#">آیا رمز عبور خود را فراموش کرده اید؟</a>*/}
                    </Grid>
                    <Grid xs={9} item>
                        <Button onClick={handleLogin} fullWidth variant={"contained"} className={classes.signInButton}>ورود
                            به
                            سامانه</Button>
                    </Grid>


                </Grid>
                <div style={{marginBottom: "auto"}}/>
                <div style={{textAlign: "center"}}>
                    <img alt={"logoCompony"} src={"/irisa.png"}/>
                    <div className={classes.copyRight}>طراحی و پیاده سازی توسط شرکت <a href={"/#"}
                                                                                       className={classes.linkCompany}>ایریسا</a>
                    </div>
                </div>
            </div>
        </AddToHomeScreenPrompt>
    );
}

export default SignIn;