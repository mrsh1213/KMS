import React, {memo, useCallback, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import moment from "moment";
import IconButton from "@material-ui/core/IconButton";
import {MdMoreHoriz} from "react-icons/md";
import ListItem from "@material-ui/core/ListItem";
import API from "../../config/API";
import {userService} from "../../config/constans";
import {BiNetworkChart, MdPublic, TiGroup} from "react-icons/all";

FolderCard.propTypes = {
    doc: PropTypes.object.isRequired,
    handleClick: PropTypes.func.isRequired,
};
const styles = makeStyles((theme) => ({
    container: {
        "marginBottom": 12,
        "minHeight": "70px",
        "padding": "10px",
        "borderRadius": "10px",
        "boxShadow": "0 4px 16px 0 rgba(37, 56, 88, 0.1)",
        "backgroundColor": "#ffffff"
    },
    name: {
        "fontFamily": "'IranSans'",
        "fontSize": "12px",
        "fontWeight": "bold",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.57",
        "letterSpacing": "normal",
        "color": "#42516e",
        "textAlign": "center",
    },
    author: {
        "fontFamily": "'IranSans'",
        "fontSize": "9px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#42516e"
    },
    numberOfFiles: {
        "fontFamily": "'IranSansFaNum'",
        "fontSize": "9px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#c0c7d1"
    },
    createdAt: {
        "fontFamily": "'IranSansFaNum'",
        "fontSize": "9px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "textAlign": "right",
        "color": "#c0c7d1"
    },
    more: {
        "textAlign": "right",
    },
    childItem: {
        minHeight: "inherit"
    },
    imageCover: {
        "backgroundColor": "#feefb2",
        width: 60,
        height: 60,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 32
    },
    icon: {
        width: 32,
        height: 32,
    }
}));

function Permission({permission}) {
    switch (permission) {
        case "TECHNICAL_GROUPS":
            return <span><TiGroup style={{fontSize: "10px"}}/>{" گروه تخصصی "}</span>
        case "WORKS":
            return <span><BiNetworkChart style={{fontSize: "10px"}}/>{" امور "}</span>

        case "PUBLIC":
            return <span><MdPublic style={{fontSize: "10px"}}/>{" عمومی "}</span>
        default:
            return <span>ناشناس</span>
    }
}

function FolderCard(props) {
    const {doc, handleClick} = props;
    const {folderId, name, permission, createdAt, author, numberOfFiles} = doc;
    const [userInfo, setUserInfo] = useState({
        fullName: "",
        jobTitle: ""
    });
    const classes = styles();

    const getUser = useCallback(uId => {
        API({url: userService.getInfoByUid(uId)}).then(res => {
            if (res.status === 200) {
                setUserInfo(res.data);
            }
        });
    }, []);
    useEffect(() => {
        getUser(author);
    }, [getUser, author]);

    return (userInfo.fullName ?
            <ListItem key={folderId} button onClick={() => {
                handleClick({...doc, fullName: userInfo.fullName})
            }} className={classes.container}>
                <Grid className={classes.childItem} alignItems={"center"} justify={"center"} container>
                    <Grid xs={3} item>
                        <div className={classes.imageCover}>
                            <img alt="folder-ico" className={classes.icon}
                                 src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAANVJREFUWEftlzEOgjAUhr+X6CyeQK/kALOeQG8g3kBvwAyDV9Ib4KxJTSEBjMZQQOrwmDr09X39WsKP4PkRz/1RgMqAORPwYA+sgeDr0RgSidgMcXw1QMoRYdt60YEgaoCMC7BoDWAnDgDRBDBOzbtNzoGECQdZYcf1W2AyxgAosQ0nidj5A4BcQuY+AZCwtD/2HahujgKoATWgBtSAGvhDAyk5wqxb0HGuukrI8vVz7BpKnXs2Cj4mIhvL78RFLP+VCcOtyIRT4rdM2GdDfWr118y7gSeXr10hu4YdQQAAAABJRU5ErkJggg=="}/>
                        </div>
                    </Grid>
                    <Grid className={classes.childItem} direction={"column"} alignItems={"flex-start"}
                          justify={"space-around"}
                          container xs={4} item>
                        <Grid className={classes.name} xs={12} item>{name}</Grid>
                        <Grid className={classes.author} xs={12} item>مالک: {userInfo.fullName}</Grid>
                        <Grid xs={12} className={classes.numberOfFiles} item>تعداد‌فایل‌ها: {numberOfFiles}</Grid>
                    </Grid>
                    <Grid className={classes.childItem} direction={"column"} alignItems={"flex-end"}
                          justify={"space-between"}
                          container xs={5} item>
                        <Grid className={classes.more} xs={12} item>
                            <IconButton size={"small"}>
                                <MdMoreHoriz style={{color: "#c0c7d1"}}/>
                            </IconButton>
                        </Grid>
                        <Grid justify={"flex-start"} className={classes.author} xs={12} item>
                            دسترسی: <Permission permission={permission}/>
                        </Grid>
                        <Grid className={classes.createdAt} xs={12}
                              item>{moment(new Date(createdAt)).fromNow()}</Grid>

                    </Grid>
                </Grid>
            </ListItem>
            : null
    );
}

export default memo(FolderCard);