import React, {memo, useCallback, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import moment from "moment";
import IconButton from "@material-ui/core/IconButton";
import {MdMoreHoriz} from "react-icons/md";
import ListItem from "@material-ui/core/ListItem";
import API from "../../config/API";
import {userService} from "../../config/constans";

FileCard.propTypes = {
    doc: PropTypes.object.isRequired,
    handleClick: PropTypes.func.isRequired,
};
const styles = makeStyles((theme) => ({
    container: {
        "marginBottom": 12,
        "minHeight": "70px",
        "padding": "10px",
        "borderRadius": "10px",
        "boxShadow": "0 4px 16px 0 rgba(37, 56, 88, 0.1)",
        "backgroundColor": "#ffffff"
    },
    fileName: {
        "fontFamily": "'IranSans'",
        "fontSize": "12px",
        "fontWeight": "bold",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.57",
        "letterSpacing": "normal",
        "color": "#42516e",
        "textAlign": "center",
    },
    author: {
        "fontFamily": "'IranSans'",
        "fontSize": "9px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#42516e"
    },
    size: {
        "fontFamily": "'IranSansFaNum'",
        "fontSize": "9px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#c0c7d1"
    },
    createdAt: {
        "fontFamily": "'IranSansFaNum'",
        "fontSize": "9px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "textAlign": "right",
        "color": "#c0c7d1"
    },
    more: {
        "textAlign": "right",
    },
    childItem: {
        minHeight: "inherit"
    },
    imageCover: {
        "backgroundColor": "#fedbd2",
        width: 60,
        height: 60,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 32
    },
    icon: {
        width: 32,
        height: 32,
    }
}));

function FileCard(props) {
    const {doc, handleClick} = props;
    const {fileId, fileName, author, createdAt, size} = doc;
    const [userInfo, setUserInfo] = useState({
        fullName: "",
        jobTitle: ""
    });
    const classes = styles();
    const getUser = useCallback(uId => {
        API({url: userService.getInfoByUid(uId)}).then(res => {
            if (res.status === 200) {
                setUserInfo(res.data);
            }
        });
    }, []);

    useEffect(() => {
        getUser(author);
    }, [getUser, author]);
    return (userInfo.fullName ?
            <ListItem key={fileId} button onClick={() => {
                handleClick(doc)
            }} className={classes.container}>
                <Grid className={classes.childItem} alignItems={"center"} justify={"center"} container>
                    <Grid xs={3} item>
                        <div className={classes.imageCover}>
                            <img alt="folder-ico" className={classes.icon}
                                 src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAbJJREFUWEftljFIG3EUxn+fsWBRbKlIFxfF2QRc3NrqUByKIsnkJJ1aobsgYrfsil0qtK4XkOpQWtDZRUycBAUXdQstUqmoyStNDjTmTO4kR1q4G+//3vd+970/755o8qMm16cKwEb7O+noSINNgJ4GA7S0nNxMkJxqgFRiCXgTRORW7Aec7LTA/Gh4AMTzoCd+kq9jbBn0DOgvvTNWyGSnBMV6Ol4OXAGxeokV58Y8hfNFYm2biAEX4guHsZS2ty9raXk5UAC1BAR4r0x2vnx/2r+BhsoQtoHyr+Qc/b5Lr6EApZqpnodY1zrSSLmobfHr7KW+Hpx6QTQcoFRycPABvQUHMea2Y5fC+bBW9/K3IUIBKN9DWkjGV5AmXYh9LvRcazsnNyFCA3AhRDKxiHjrtuNQTq4vDIDPwKc7L65sDvTi77mcbMVHVzuQjBeRQhvREcB/4EAq4esnEmhS3giu70AEEDnQ9EEUtSBy4B9w4Cfw6L6jtnae/ZCTq1j5vTaij6DXoQCYLSiTe1d7IxpPPKbV0kgTQHdjQOwYk8NpcVbfd89qAjSmoH+V0FYvvwh/AJIMySFOaCh6AAAAAElFTkSuQmCC"}/>
                        </div>
                    </Grid>
                    <Grid className={classes.childItem} direction={"column"} alignItems={"flex-start"}
                      justify={"space-around"}
                      container xs={4} item>
                    <Grid className={classes.fileName} xs={12} item>{fileName}</Grid>
                    <Grid className={classes.author} xs={12} item>مالک: {userInfo.fullName}</Grid>
                    <Grid xs={12} className={classes.size} item>حجم فایل: {size}Mb</Grid>

                </Grid>
                <Grid className={classes.childItem} direction={"column"} alignItems={"flex-end"}
                      justify={"space-between"}
                      container xs={5} item>
                    <Grid className={classes.more} xs={12} item>
                        <IconButton size={"small"}>
                            <MdMoreHoriz style={{color: "#c0c7d1"}}/>
                        </IconButton>
                    </Grid>
                    <Grid className={classes.createdAt} xs={12}
                          item>{moment(new Date(createdAt)).fromNow()}</Grid>

                </Grid>
                </Grid>
            </ListItem> :
            null
    );
}

export default memo(FileCard);