import React, {memo, useCallback, useEffect, useState} from 'react';
import moment from "moment";
import PropTypes from 'prop-types';
import Card from "@material-ui/core/Card";
import {makeStyles} from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import {MdMoreHoriz} from "react-icons/md";
import {AiFillFolderOpen} from "react-icons/ai";
import IconButton from "@material-ui/core/IconButton";
import {BiArrowBack} from "react-icons/bi";
import API from "../../config/API";
import {documentService, userService} from "../../config/constans";
import {AiTwotoneLock, BiNetworkChart, MdPublic, TiGroup} from "react-icons/all";

DirectoryInfo.propTypes = {
    path: PropTypes.array,
    backPath: PropTypes.func,
    getHasAccess: PropTypes.func
};
const styles = makeStyles((theme) => ({
    iconButtonAddFolder: {
        "fontSize": "24px",
        "fontWeight": "normal",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.58",
        "letterSpacing": "normal",
        "textAlign": "center",
        "color": "#42516e",
        marginRight: 12
    },
    textButtonAddFolder: {
        marginTop: 14,
        "fontFamily": "'IranSans'",
            "fontSize": "12px",
            "fontWeight": "normal",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.58",
            "letterSpacing": "normal",
            "textAlign": "center",
            "color": "#42516e",
            "backgroundColor": "#e0e1e5 !important"

        },
        iconButtonAddFile: {
            "fontSize": "20px",
            "fontWeight": "normal",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.58",
            "letterSpacing": "normal",
            "textAlign": "center",
            "color": "#fff",
            marginRight: 12
        },
        textButtonAddFile: {
            marginTop: 14,
            "fontFamily": "'IranSans'",
            "fontSize": "12px",
            "fontWeight": "normal",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.58",
            "letterSpacing": "normal",
            "textAlign": "center",
            "color": "#fff",
            "backgroundColor": "#2684fe !important"

        },
        card: {
            marginTop: 16,
            "fontFamily": "'IranSans'",
            "minHeight": "160px",
            "padding": "16px",
            "borderRadius": "10px",
            "boxShadow": "0 4px 16px 0 rgba(52, 69, 99, 0.2)",
            "backgroundColor": "#f4f5f7"
        },
        imageCover: {
            "backgroundColor": "#fef7cf",
            width: 60,
            height: 60,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            borderRadius: 32
        },
        icon: {
            color: "#ffab00",
            width: 32,
            height: 32,
        },
        title: {
            "fontFamily": "'IranSans'",
            "fontSize": "14px",
            "fontWeight": "bold",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.57",
            "letterSpacing": "normal",
            "color": "#42516e"
        },
        detail: {
            "fontFamily": "'IranSans'",
            "fontSize": "9px",
            "fontWeight": "normal",
            "fontStretch": "normal",
            "fontStyle": "normal",
            paddingTop: 8,
            "lineHeight": "1.5",
            "letterSpacing": "normal",
            "color": "#42516e"
        },
        pathText: {
            textAlign: "right",
            "fontFamily": "'IranSans'",
            "fontSize": "10px",
            "fontWeight": "bold",
            "fontStretch": "normal",
            "fontStyle": "normal",
            "lineHeight": "1.5",
            "letterSpacing": "normal",
            "color": "#42516e"
        },
        path: {
            marginBottom: 16,
            borderRadius: 8,
            "padding": "4px",
            "backgroundColor": "rgba(224, 225, 229, 0.5)"
        }
    }))
;

function Permission({permission}) {
    switch (permission) {
        case "TECHNICAL_GROUPS":
            return <span><TiGroup style={{fontSize: "10px"}}/>{" گروه تخصصی "}</span>
        case "WORKS":
            return <span><BiNetworkChart style={{fontSize: "10px"}}/>{" امور "}</span>

        case "PUBLIC":
            return <span><MdPublic style={{fontSize: "10px"}}/>{" عمومی "}</span>
        default:
            return <span>ناشناس</span>
    }
}

function DirectoryInfo(props) {
    const [currentFolder, setCurrentFolder] = useState({});
    const [userInfo, setUserInfo] = useState({});
    const {path, backPath, getHasAccess} = props;
    const classes = styles();

    const getUser = useCallback(uId => {
        API({url: userService.getInfoByUid(uId)}).then(res => {
            if (res.status === 200) {
                setUserInfo(res.data);
            }
        });
    }, []);

    useEffect(() => {
        API({
            method: "GET",
            url: documentService.getFolderInfoByPath(path.join("/"), localStorage.getItem("userId"))
        }).then(res => {
            if (res.status === 200) {
                getHasAccess(res.data.hasAccess);
                setCurrentFolder(res.data);
                getUser(res.data.author);
            }
        })
    }, [path, getUser, getHasAccess])
    return (userInfo.fullName ?
            <Card className={classes.card}>
                <Grid container>
                    <Grid container alignItems={"center"} className={classes.path} xs={12} item>
                        <Grid xs={11} item>
                            <div dir={"ltr"} className={classes.pathText}>{"~/" + path.join(" / ")}</div>
                        </Grid>
                        <Grid xs={1} item>
                            <IconButton size={"small"} onClick={backPath}><BiArrowBack/></IconButton>
                        </Grid>
                    </Grid>
                    <Grid xs={3} item>
                        <div className={classes.imageCover}>
                            <AiFillFolderOpen className={classes.icon}
                            />
                        </div>
                    </Grid>
                    <Grid xs={4} item>
                        <Grid className={classes.title} xs={12} item>{currentFolder.name}</Grid>
                        <Grid className={classes.detail} xs={12} item>مالک: {userInfo.fullName}</Grid>
                        <Grid className={classes.detail} xs={12} item>تعداد فایل‌ها:{currentFolder.numberOfFiles}</Grid>
                    </Grid>
                    <Grid xs={5} item>
                        <Grid style={{textAlign: "left"}} xs={12} item>
                            {!currentFolder.hasAccess && <AiTwotoneLock style={{color: "#007bff"}}/>}
                            <IconButton
                                size={"small"}><MdMoreHoriz/></IconButton>
                        </Grid>
                        <Grid className={classes.detail} xs={12} item>دسترسی: <Permission
                            permission={currentFolder.permission}/></Grid>
                        <Grid className={classes.detail} xs={12} item>تاریخ
                            ایجاد: {moment(new Date(currentFolder.createdAt)).fromNow()}</Grid>
                    </Grid>
                    {/* <Grid xs={6} item>
                    <Button className={classes.textButtonAddFile}>
                        <AiFillFileAdd className={classes.iconButtonAddFile}/>
                        افزودن فایل جدید
                    </Button>
                </Grid>
                <Grid xs={6} item>
                    <Button className={classes.textButtonAddFolder}>
                        <MdCreateNewFolder className={classes.iconButtonAddFolder}/>
                        افزودن پوشه جدید
                    </Button>
                </Grid>*/}
                </Grid>
            </Card> :
            null
    );
}

export default memo(DirectoryInfo);