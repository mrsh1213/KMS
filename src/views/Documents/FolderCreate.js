import React, {useCallback, useContext, useState} from 'react';
import PropTypes from 'prop-types';
import {useQuery} from "../../config/Helper";
import DevDialog from "../../components/common/DevDialog";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/styles";
import Divider from "@material-ui/core/Divider";
import DevTextField from "../../components/common/DevTextField";
import AccessLevel from "../common/AccessLevel";
import Button from "@material-ui/core/Button";
import {Send} from "@material-ui/icons";
import API from "../../config/API";
import {documentService, postsService} from "../../config/constans";
import {BackDropContext} from "../../config/BackDropContext";
import {useSnackbar} from "notistack";

FolderCreate.propTypes = {
    open: PropTypes.bool,
    handleClose: PropTypes.func
};
const styles = makeStyles((theme) => ({
    buttonSave: {
        "height": "32px", "borderRadius": "5px", "backgroundColor": "#2684fe !important",
        color: "#fff",
    },
    imageCover: {
        "backgroundColor": "#feefb2",
        width: 60,
        height: 60,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 32,
        margin: 5
    },
    icon: {
        width: 32,
        height: 32,
    },
    titleDialog: {
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "bold",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.67",
        "letterSpacing": "normal",
        "color": "#42516e"
    },
    inputTitle: {
        padding: 5,
        fontSize: 16,
        fontWeight: "bold",
    }
}));

function FolderCreate(props) {
    const [title, setTitle] = useState("")
    const [accessLevel, setAccessLevel] = useState("TECHNICAL_GROUPS");
    const {open, handleClose, currentPath} = props;
    const classes = styles();
    const {enqueueSnackbar} = useSnackbar();
    const {dispatch} = useContext(BackDropContext);

    // let currentPath = useQuery().get("currentPath");

    function handleTitle(e) {
        setTitle(e.target.value)
    }

    function addFolder(e) {
        if (title) {
            API({
                data: {
                    name: title,
                    permission: accessLevel
                }
                , method: "POST", url: documentService.postFolder(currentPath, localStorage.getItem("userId"))
            }, dispatch).then(res => {
                if (res.status === 201) {
                    handleClose(true);
                } else {
                    enqueueSnackbar("خطا در ایجاد فولدر رخ داده است", {
                        variant: "error", anchorOrigin: {
                            vertical: 'bottom',
                            horizontal: 'center',
                        }
                    });
                }
            });
        } else {
            enqueueSnackbar("لطفا عنوان را نمایید", {
                variant: "error", anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'center',
                }
            });
        }
    }

    const handleGetAccessLevel = useCallback((selectedAccessLevel) => {
        setAccessLevel(selectedAccessLevel);
    }, [setAccessLevel]);
    return (
        <DevDialog maxWidth={"xs"} open={open} handleClose={handleClose}>
            <div className={classes.titleDialog}>ایجاد پوشه</div>
            <Divider variant={"middle"}/>
            <Grid justify={"center"} alignItems={"center"} container>
                <Grid xs={3} item>
                    <div className={classes.imageCover}>
                        <img alt="folder-ico" className={classes.icon}
                             src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAANVJREFUWEftlzEOgjAUhr+X6CyeQK/kALOeQG8g3kBvwAyDV9Ib4KxJTSEBjMZQQOrwmDr09X39WsKP4PkRz/1RgMqAORPwYA+sgeDr0RgSidgMcXw1QMoRYdt60YEgaoCMC7BoDWAnDgDRBDBOzbtNzoGECQdZYcf1W2AyxgAosQ0nidj5A4BcQuY+AZCwtD/2HahujgKoATWgBtSAGvhDAyk5wqxb0HGuukrI8vVz7BpKnXs2Cj4mIhvL78RFLP+VCcOtyIRT4rdM2GdDfWr118y7gSeXr10hu4YdQQAAAABJRU5ErkJggg=="}/>
                    </div>
                </Grid>
                <Grid xs={9} item>
                    <DevTextField className={classes.inputTitle} fullWidth onChange={handleTitle} value={title}
                                  label={"عنوان پوشه"}
                                  placeholder="لطفا عنوان پوشه را بنویسید"/>
                </Grid>
                <Grid container xs={4} item>
                    <AccessLevel handleGetAccessLevel={handleGetAccessLevel}/>
                </Grid>
                <Grid xs={4} item/>
                <Grid xs={4} item>
                    <Button onClick={addFolder} classes={{root: classes.buttonSave}} variant={"contained"} fullWidth>ایجاد<Send
                        style={{marginRight: 16, transform: "rotate(180deg)"}}/></Button>
                </Grid>
            </Grid>
        </DevDialog>
    );
}

export default FolderCreate;