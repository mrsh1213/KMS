import React, {useCallback, useContext, useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FolderCard from "./FolderCard";
import {AiFillFileAdd} from "react-icons/ai";
import {MdCreateNewFolder} from "react-icons/md";
import {makeStyles} from "@material-ui/styles";
import DevSearchBox from "../../components/common/DevSearchBox";
import DirectoryInfo from "./DirectoryInfo";
import Fab from "@material-ui/core/Fab";
import {useHistory} from "react-router";
import {useQuery} from "../../config/Helper";
import FileCard from "./FileCard";
import API from "../../config/API";
import {documentService, searchService} from "../../config/constans";
import CircularProgress from "@material-ui/core/CircularProgress";
import InfiniteScroll from "react-infinite-scroll-component";
import FolderCreate from "./FolderCreate";
import {BackDropContext} from "../../config/BackDropContext";

const useStyles = makeStyles((theme) => ({
    container: {
        height: "100%",
        overflow: "auto",
        alignContent: "baseline"
    }
}))

function Index() {
    const classes = useStyles();
    const [keyword, setKeyword] = useState("");
    const [reload, setReload] = useState(false);
    const [docs, setDocs] = useState({
        content: [], number: -1, totalElements: 0
    });
    const {dispatch} = useContext(BackDropContext);
    let currentPath = useQuery().get("currentPath");
    currentPath = currentPath.substr(1, currentPath.length);
    let arrayCurrentPath = currentPath ? currentPath.split("/") : [];
    const [path, setPath] = useState([...arrayCurrentPath]);
    const [openFolderCreate, setOpenFolderCreate] = useState(false);
    const [hasAccess, setHasAccess] = useState(true);
    const history = useHistory();
    // const location = useLocation();

    useEffect(() => {
        history.push("/dashboard/documents?currentPath=/" + path.join("/"));
        // window.location.reload();
    }, [path]);


    // useEffect(() => {
    //     if (window.history && window.history.pushState) {
    //         window.onpopstate= function(e) {
    //             let currentPath=new URLSearchParams(location.search).get("currentPath");
    //             currentPath=currentPath.substr(1, currentPath.length);
    //             setPath(currentPath ? currentPath.split("/") : [])
    //         };
    //     }
    //     //
    // }, [location])

    const handleClickFolderCard = useCallback((folder) => {
        setPath(prevState => {
            return [...prevState, folder.name];
        });
    }, []);
    const handleClickFileCard = useCallback((file) => {
        //download
    }, []);
    const handleBackPath = useCallback(() => {
        setPath(prevState => {
            let temp = [...prevState];
            temp.pop();
            return temp;
        });
    }, []);
    const handleGetHasAccess = useCallback((hasAccess) => {
        console.log(hasAccess);
        setHasAccess(hasAccess);
    }, []);
    useEffect(() => {
        setDocs({content: [], number: -1, totalElements: 0});
        if (keyword) {
            searchDocs(0, keyword);
        } else {
            fetchDataDocs(0);
        }
    }, [keyword, setDocs, path, reload]);

    const searchDocs = useCallback((page, keyword) => {
        API({
            method: "GET",
            url: searchService.searchWiki(encodeURIComponent(keyword), page, 10, localStorage.getItem("userId"))
        }).then(res => {
            if (res.status === 200) {
                setDocs(prevState => ({...res.data, content: [...prevState.content, ...res.data.content]}));
            } else if (res.status === 204) {
                setDocs({content: [], number: -1, totalElements: 0})
            }
        }).catch(() => {
            setDocs({content: [], number: -1, totalElements: 0});
        });
    }, []);
    const fetchDataDocs = useCallback((page) => {
        if (keyword.length === 0) {
            API({
                method: "GET",
                url: documentService.getFilesAndFolders("/" + path.join("/"), page, 10, localStorage.getItem("userId"))
            }, dispatch).then(res => {
                if (res.status === 200) {
                    // setTimeout(() => {
                    setDocs(prevState => ({...res.data, content: [...prevState.content, ...res.data.content]}));
                    // }, 1000);
                } else if (res.status === 204) {
                    setDocs({content: [], number: -1, totalElements: 0})
                }
            })
        } else {
            searchDocs(page, keyword)
        }

    }, [keyword, path]);

    const searchMethod = useCallback((keyword) => {
        setKeyword(keyword);
    }, []);
    const handleCloseFolderCreate = useCallback((reload) => {
        if (reload) {
            setReload(prevState => !prevState);
        }
        setOpenFolderCreate(false);
    }, []);
    const handleOpenFolderCreate = useCallback(() => {
        setOpenFolderCreate(true);
    }, []);
    return (<>
            <FolderCreate currentPath={currentPath} open={openFolderCreate} handleClose={handleCloseFolderCreate}/>
            <Grid className={classes.container} container>
                <Grid xs={12} item>
                    <DevSearchBox searchMethod={searchMethod}/>
                </Grid>
                {path.length > 0 && <Grid style={{paddingBottom: 16}} xs={12} item>
                    <DirectoryInfo getHasAccess={handleGetHasAccess} backPath={handleBackPath} path={path}/>
                </Grid>}
                <Grid xs={12} item className={classes.container} container>
                    <InfiniteScroll
                        initialScrollY={200}
                        height={"100%"}
                        style={{minWidth: "275px"}}
                        dataLength={docs.content.length}
                        next={() => {
                            fetchDataDocs(docs.number + 1)
                        }}
                        hasMore={!docs.last}
                        loader={docs.content.length > 0 &&
                        <div style={{textAlign: "center",}}><CircularProgress/></div>}
                    >
                        {docs.content.map(doc => {
                                if (doc.folderId) {
                                    return (
                                        <Grid key={doc.folderId} xs={12} item>
                                            <FolderCard handleClick={handleClickFolderCard} doc={doc}/>
                                        </Grid>)
                                } else if (doc.fileId) {
                                    return (
                                        <Grid key={doc.fileId} xs={12} item>
                                            <FileCard handleClick={handleClickFileCard} doc={doc}/>
                                        </Grid>);
                                } else {
                                    return "";
                                }
                            }
                        )}
                    </InfiniteScroll>
                </Grid>{(hasAccess || path.length === 0) && <>
                {path.length > 0 && <Fab
                    style={{
                        position: "fixed",
                        bottom: 150,
                        left: 10,
                        textAlign: "left"
                    }}
                    onClick={() => {
                        history.push("/main/createFile?currentPath=" + path.join("/"))
                    }}
                    size={"large"}
                    color={"primary"}
                >
                    <AiFillFileAdd style={{width: 24, height: 24}}/>
                </Fab>}
                <Fab
                    style={{
                        position: "fixed",
                        bottom: 90,
                        left: 10,
                        textAlign: "left"
                    }}
                    onClick={() => {
                        handleOpenFolderCreate();
                    }}
                    size={"large"}
                    color={"primary"}
                >
                    <MdCreateNewFolder style={{width: 24, height: 24}}/>
                </Fab></>}
            </Grid>
        </>
    );
}

export default Index;
