import React, {useCallback, useContext, useRef, useState} from 'react';
import {useQuery} from "../../config/Helper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {Send} from "@material-ui/icons";
import DevTextField from "../../components/common/DevTextField";
import AccessLevel from "../common/AccessLevel";
import HashTagsAutoComplete from "../common/HashTagsAutoComplete";
import KnowScopeAutoComplete from "../common/KnowScopeAutoComplete";
import {BackDropContext} from "../../config/BackDropContext";
import {useSnackbar} from "notistack";
import {makeStyles} from "@material-ui/styles";
import API from "../../config/API";
import {documentService} from "../../config/constans";
import {useHistory} from "react-router";
import Box from "@material-ui/core/Box";
import LinearProgress from "@material-ui/core/LinearProgress";
import Typography from "@material-ui/core/Typography";

const styles = makeStyles((theme) => ({
    container: {
        height: "100%",
        overflow: "auto",
        alignContent: "baseline"
    },
    card: {
        alignItems: "center",
        padding: 16,
        marginTop: 16,
        "borderRadius": "8px",
        "boxShadow": "0 4px 8px 0 rgba(37, 56, 88, 0.1)",
        "backgroundColor": "#ffffff"
    },
    autocomplete: {
        marginBottom: 8,
    },
    buttonSave: {
        "height": "32px", "borderRadius": "5px", "backgroundColor": "#2684fe !important",
        color: "#fff",
    },
    titleUploadFile: {
        "fontFamily": "'IranSans'",
        "fontSize": "14px",
        "fontWeight": "500",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.57",
        "letterSpacing": "normal",
        "color": "#2684fe"
    },
    description: {
        marginTop: 8,
        "fontFamily": "'IranSans'",
        "fontSize": "12px",
        fontWeight: "bold",
        color: "#000",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.7",
        "letterSpacing": "normal",
        "textAlign": "left",
    },
    inputDescription: {
        padding: 5,
        fontSize: 16,
        fontWeight: "bold",
    },
    imageCover: {
        fontSize: 16,
        "backgroundColor": "#deebfe",
        width: 60,
        height: 60,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 32
    },
    icon: {
        width: 32,
        height: 32,
    },
    pathText: {
        textAlign: "right",
        "fontFamily": "'IranSans'",
        "fontSize": "10px",
        "fontWeight": "bold",
        "fontStretch": "normal",
        "fontStyle": "normal",
        "lineHeight": "1.5",
        "letterSpacing": "normal",
        "color": "#42516e"
    },
    path: {
        borderRadius: 8,
        "padding": "4px",
        "backgroundColor": "rgba(224, 225, 229, 0.5)"
    }
}));

FileCreate.propTypes = {};

function FileCreate(props) {
    const [accessLevel, setAccessLevel] = useState("TECHNICAL_GROUPS");
    const [hashTags, setHashTags] = useState([]);
    const [knScope, setKnScope] = useState(null);
    const [docId, setDocId] = useState(null);
    const [file, setFile] = useState({});
    const [description, setDescription] = useState("");
    const [progress, setProgress] = React.useState(0);
    const {dispatch} = useContext(BackDropContext);
    const {enqueueSnackbar} = useSnackbar();
    let currentPath = useQuery().get("currentPath");
    const classes = styles();
    const history = useHistory();
    // const user = JSON.parse(localStorage.getItem("user"));
    const handleGetAccessLevel = useCallback((selectedAccessLevel) => {
        setAccessLevel(selectedAccessLevel);
    }, [setAccessLevel]);
    const handleGetListHashTags = useCallback((selectedHashTags) => {
        setHashTags(selectedHashTags);
    }, [setHashTags]);
    const handleGetKnowScope = useCallback((selectedKnScope) => {
        setKnScope(selectedKnScope);
    }, [setKnScope]);

    function submitFile() {
        if (docId && hashTags.length > 0) {
            API({
                data: {
                    "description": description,
                    "docId": docId,
                    "hashTagIds": hashTags.map(hashTag => (hashTag.hashTagId)),
                    "projectCode": knScope ? knScope.title : null,
                    "permission": accessLevel,
                }
                , method: "POST", url: documentService.postFile(currentPath, localStorage.getItem("userId"))
            }, dispatch).then(res => {
                if (res.status === 201) {
                    history.push("/dashboard/documents?currentPath=" + currentPath)
                } else {
                    enqueueSnackbar("خطا در ثبت سند رخ داده است", {
                        variant: "error", anchorOrigin: {
                            vertical: 'bottom',
                            horizontal: 'center',
                        }
                    });
                }
            });

        } else {
            enqueueSnackbar(" برچسب و بارگزاری سند الزامی می باشد", {
                variant: "error", anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'center',
                }
            });

        }
    }

    const handleUploadDoc = useCallback((e) => {
        let form_data = new FormData();
        setFile(e.target.files[0]);
        form_data.append('file', e.target.files[0]);
        API({
                method: "POST",
                data: form_data,
                url: documentService.uploadFile(currentPath, localStorage.getItem("userId")),
                onUploadProgress: (progressEvent) => {
                    const totalLength = progressEvent.lengthComputable ? progressEvent.total : progressEvent.target.getResponseHeader('content-length') || progressEvent.target.getResponseHeader('x-decompressed-content-length');
                    if (totalLength !== null) {
                        setProgress(Math.round((progressEvent.loaded * 100) / totalLength));
                    }
                }
            }
        ).then(res => {
            setDocId(res.data);
            setProgress(100);
            enqueueSnackbar("فایل با موفقیت بارگذاری شد", {
                variant: "success", anchorOrigin: {
                    vertical: 'bottom',
                    horizontal: 'center',
                }
            });
        });
    }, [description, currentPath]);

    return (
        <Grid className={classes.container} container>
            <Grid container alignItems={"center"} className={classes.path} xs={12} item>
                <Grid xs={11} item>
                    <div dir={"ltr"} className={classes.pathText}>{"~/" + currentPath}</div>
                </Grid>
            </Grid>
            <Grid xs={12} item container className={classes.card}>
                <Grid xs={3} item>
                    <input
                        onChange={handleUploadDoc}
                        accept="*/*"
                        style={{display: 'none'}}
                        id={"uploadFile"}
                        name={"uploadFile"}
                        multiple
                        type="file"
                    />
                    <label htmlFor={"uploadFile"}>
                        <span className={classes.imageCover}>
                        <img alt="folder-ico" className={classes.icon}
                             src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAAAcFJREFUWEftl00oBGEYx///d0gopOTiINk9Orv5OjhJKScnOQjloBxoViOrnFyIFIWrk5wcODu7mVUuKEmklgPzPtqPsGbt7mi2cZg5zvPx/83zPu8770ME/DBgfbgA2i2po6FXQA4RaPYGKCu2acx5iXEBRJacDZITXpJ89xWRzYSppkBKKTlcANEl/QCisZTgTx+RHSG7CLSn34ns244ahUVdLI+7AnH9TsAoFphjJ60ksV6r5RRAR9Z2aDdxGON8K5QrzxJoh4TyBCCyaMcMK90/FXJMoDMbf/JSw4HrGb7+ls9XgJRIy6pU17zIEYC+9GoAZ/LO/kuLz/kgfAdIi2xJZfReDgAMZkXPk4q9N/N8+AlRHoCUiiUqUqH3CY5k+hIJ5bD7wuLtd4jyAWRUGV3W6wAnM7sDV3ZMtfkOIJA9EbX7W6MpygKAnpTdNlXOR+fbhppwn5CedkUB5xDg/1cgGtcl/UT+2hNFKxAChBWIxHWwB1G4BGEFAq9AJK6fCNT/9agtEvdomyrnyp9nLnC2QY6VA0BE1hIxY7rgjajVkobKr9GsyQ8QAW4AOXiuUubdLJMFAfwQ9JIj8On4A8JL2CHmvo8cAAAAAElFTkSuQmCC"}/>
                             +
                    </span>
                    </label>
                </Grid>
                {file.name ? <Grid xs={9} container item>
                        <Grid xs={12} item>
                            {file.name.substr(0, 30)} ...</Grid>
                        <Grid xs={6} item>{file.type}</Grid>
                        <Grid style={{textAlign: "end"}} xs={6} item>{(file.size / 1000000).toFixed(2) + " مگابایت"}</Grid>

                        <Grid xs={12} item>
                            <Box display="flex" alignItems="center">
                                <Box width="100%" mr={1}>
                                    <LinearProgress variant="determinate" value={progress}/>
                                </Box>
                                <Box minWidth={35}>
                                    <Typography variant="body2" color="textSecondary">{`${Math.round(
                                        progress,
                                    )}%`}</Typography>
                                </Box>
                            </Box>
                        </Grid>
                    </Grid>
                    : <Grid xs={9} item className={classes.titleUploadFile}> <Typography variant="subtitle1"
                                                                                         color="textSecondary">
                        لطفا سند خود رو بارگذاری نمایید
                    </Typography></Grid>}
            </Grid>
            <Grid xs={12} item
                  className={classes.card}>
                <div className={classes.description}>{"توضیحات"}</div>
                <DevTextField rows={6} className={classes.inputDescription} value={description} onChange={(e) => {
                    setDescription(e.target.value)
                }} fullWidth multiline label="توضیحات"
                              placeholder="لطفا توضیحات فایل را بنویسید"/>

            </Grid>
            <Grid xs={12} item container className={classes.card}>
                <Grid xs={12} item>
                    <AccessLevel handleGetAccessLevel={handleGetAccessLevel}/>
                </Grid>
                <Grid className={classes.autocomplete} xs={12} item>
                    <HashTagsAutoComplete handleGetListHashTags={handleGetListHashTags}/>
                </Grid>
                <Grid style={{marginBottom: 12}} className={classes.autocomplete} xs={12} item>
                    <KnowScopeAutoComplete handleGetKnowScope={handleGetKnowScope}/>
                </Grid>
                <Grid style={{marginBottom: 12}} alignItems={"flex-end"} xs={12} item container>

                    <Grid xs={8} item/>
                    <Grid xs={4} item>
                        <Button onClick={submitFile} classes={{root: classes.buttonSave}} variant={"contained"}
                                fullWidth>انتشار<Send
                            style={{marginRight: 16, transform: "rotate(180deg)"}}/></Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default FileCreate;