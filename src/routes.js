import React from 'react'
import {Route, Switch} from 'react-router-dom'
import Dashboard from './views/Dashboard'
import Documents from './views/Documents'
import Posts from "./views/Posts";
import theme from "./config/themeMain";
import {ThemeProvider} from "@material-ui/styles";
import PostCreate from "./views/Posts/PostCreate";
import ResultSearch from "./components/Layout/ResultSearch";
import Setting from "./views/Settings";
import Profile from "./views/Profile";
import Post from "./views/Posts/Post";
import WikiCreate from "./views/Wiki/WikiCreate";
import ListsWikiPage from "./views/Wiki/ListsWikiPage";
import WikiPage from "./views/Wiki/WikiPage/index";
import FileCreate from "./views/Documents/FileCreate";
import FolderCreate from "./views/Documents/FolderCreate";

export const DashboardRouter = props => {

    return (<ThemeProvider theme={theme}>
            <Switch>
                <Route exact path="/dashboard" component={Dashboard}/>
                <Route path="/dashboard/wiki" component={ListsWikiPage}/>
                <Route path="/dashboard/posts" component={Posts}/>
                <Route path="/dashboard/documents" component={Documents}/>
                <Route path={"/dashboard/search"} component={ResultSearch}/>
            </Switch>
        </ThemeProvider>
    )
}
export const RootRouter = props => {

    return (<ThemeProvider theme={theme}>
            <Switch>
                <Route exact path="/main/setting" component={Setting}/>
                <Route path="/main/post" component={Post}/>
                <Route path="/main/createPost" component={PostCreate}/>
                <Route path="/main/createWiki" component={WikiCreate}/>
                <Route path="/main/createFile" component={FileCreate}/>
                <Route path="/main/createFolder" component={FolderCreate}/>
                <Route path="/main/wiki" component={WikiPage}/>
                <Route path={["/main/listsWiki/new", "/main/listsWiki/seen"]} component={ListsWikiPage}/>
                <Route path="/main/profile" component={Profile}/>
                <Route path="/main/exit" render={() => {
                    localStorage.clear();
                    return ""
                }}/>
            </Switch>
        </ThemeProvider>
    )
}