/*// export const API_SERVER = "http://172.25.113.125:8484/v1.0";
// export const API_SERVER = "http://217.182.230.9:8484/v1.0";
// export const GATEWAY_SERVER = `http://217.182.230.9:8484`;
// export const GATEWAY_SERVER = `http://172.25.113.135:8888/api`;*/
export const GATEWAY_SERVER = "https://kms.irisaco.com/api";

export const documentService = {
    getFilesAndFolders: (currentPath, page, size, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/document-service/${apiVersion}/showFileAndFolder?currentPath=${currentPath}&uId=${uId}&page=${page}&size=${size}`,
    getFolderInfoByPath: (currentPath, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/document-service/${apiVersion}/folderInfo?uId=${uId}&currentPath=/${currentPath}`,
    postFolder: (currentPath, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/document-service/${apiVersion}/createFolder?uId=${uId}&currentPath=/${currentPath}`,
    postFile: (currentPath, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/document-service/${apiVersion}/createFile?uId=${uId}&currentPath=/${currentPath}`,
    uploadFile: (currentPath, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/document-service/${apiVersion}/uploadFile?uId=${uId}&currentPath=/${currentPath}`,
};
export const fndService = {
    getSystemInfo: (apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/fnd-service/${apiVersion}/systemInfo`,
};
export const searchService = {
    searchUsers: (keyword, page, size, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/search-service/${apiVersion}/search/users?page=${page}&size=${size}&keyword=${keyword}`,
    searchWiki: (keyword, page, size, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/search-service/${apiVersion}/search/wiki?uId=${uId}&page=${page}&size=${size}&keyword=${keyword}`,
    searchPost: (keyword, page, size, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/search-service/${apiVersion}/search/post?uId=${uId}&page=${page}&size=${size}&keyword=${keyword}`,
};
export const wikiService = {
    getWikiesBySort: (sort, page, size, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/wiki-service/${apiVersion}/showAll?uId=${uId}&page=${page}&size=${size}&sortBy=${sort}`,
    getWikiPhoto: (pId, w, h, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/wiki-service/${apiVersion}/loadPhoto?pId=${pId}&w=${w}&h=${h}`,
    getNewWiki: (uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/wiki-service/${apiVersion}/latestWiki?uId=${uId}`,
    getWikiById: (wId, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/wiki-service/${apiVersion}/getWikiById?wId=${wId}&uId=${uId}`,
    postWiki: (apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/wiki-service/${apiVersion}/addWiki`,
    postWikiPhoto: (apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/wiki-service/${apiVersion}/uploadPhoto`,
    putBookmarkWikiByWId: (wId, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/wiki-service/${apiVersion}/bookmarkWiki?wId=${wId}&uId=${uId}`,
    putRateWiki: (wId, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/wiki-service/${apiVersion}/rateWiki?wId=${wId}&uId=${uId}`,
};
export const postsService = {
    getPosts: (page, size, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/post-service/${apiVersion}/all?uId=${uId}&page=${page}&size=${size}`,
    getPostById: (uId, pId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/post-service/${apiVersion}/getPostById?uId=${uId}&pId=${pId}`,
    getCommentsByPostId: (uId, postId, page, size, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/post-service/${apiVersion}/comments/${postId}?uId=${uId}&&page=${page}&size=${size}`,
    getPhotoByPhotoId: (photoId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/post-service/${apiVersion}/loadPhoto?h=200&w=300&pId=${photoId}`,
    getHashTag: (keyword, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/hashTag-service/${apiVersion}/hashTags?search=${keyword}`,
    postHashTag: (apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/hashTag-service/${apiVersion}/addHashTag`,
    postPost: (apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/post-service/${apiVersion}/addPost`,
    postComment: (apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/post-service/${apiVersion}/addComment`,
    postPostPhoto: (apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/post-service/${apiVersion}/uploadPhoto`,
    putLikePostByPostId: (postId, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/post-service/${apiVersion}/likePost?pId=${postId}&uId=${uId}`,
    putBookmarkPostByPostId: (postId, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/post-service/${apiVersion}/bookmarkPost?pId=${postId}&uId=${uId}`,
    putLikeCommentByCommentId: (commentId, uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/post-service/${apiVersion}/likeComment?cId=${commentId}&uId=${uId}`,
    deleteCommentByCommentId: (commentId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/post-service/${apiVersion}/deleteComment?cId=${commentId}`,

};

export const userService = {
    getProjects: (keyword, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/scopes?keyword=${keyword}`,
    getSkillsByUid: (uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/skills?uId=${uId}`,
    getAboutMeByUid: (uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/aboutMeInfo?uId=${uId}`,
    getInfoByUsername: (un, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/loggedInUserInfo?un=${un}`,
    getInfoByUid: (uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/loggedInUserInfo?uId=${uId}`,
    getStatusUserByUid: (uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/statusUserInfo?uId=${uId}`,
    getContactInfoByUid: (uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/contactInfo?uId=${uId}`,
    getAvatarByUid: (uId, w, h, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/loadAvatar?uId=${uId}&w=${w}&h=${h}`,
    getJsonAvatarByUid: (uId, w, h, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/loadAvatarJson?uId=${uId}&w=${w}&h=${h}`,
    postLogin: (apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/login`,
    postAvatarByUid: (uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/uploadPhoto?uId=${uId}`,
    postSkillsByUid: (uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/addSkill?uId=${uId}`,
    putSkillsByUid: (uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/updateSkill?uId=${uId}`,
    putAboutMeByUid: (uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/updateAboutMeInfo?uId=${uId}`,
    putContactInfoByUid: (uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/updateContactInfo?uId=${uId}`,
    putChangePassword: (uId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/changePassword?uId=${uId}`,
    deleteSkillBySid: (sId, apiVersion = "v1.0", serviceVersion = "v1.0") => `${GATEWAY_SERVER}/${serviceVersion}/user-service/${apiVersion}/deleteSkill?sId=${sId}`,
};
