import {createMuiTheme} from '@material-ui/core/styles'
import {blue} from '@material-ui/core/colors'

const theme = createMuiTheme({
    typography: {
        fontFamily: ['IranSans']
    },
    palette: {
        secondary: {
            main: "#f8fcfe"
        },
        primary: {
            main: blue[900]
        },
        error: {
            main: "#f44336"
        },
    },
    direction: 'rtl',
    overrides: {
        MuiPickersToolbarText: {
            toolbarTxt: {
                fontSize: '1.6rem'
            }
        },
        MuiAutocomplete: {
            tagSizeSmall: {
                backgroundColor: "#deebfe !important"
            },
            inputRoot: {
                padding: '3px 50px 3px 3px !important',
                minWidth: 75
            },
            input: {
                '&::placeholder': {
                    "fontFamily": "'IranSans' !important",
                    "fontSize": "12px !important",
                    color: "#c0c7d1 !important"
                },
            }
        },
        MuiInputLabel: {
            outlined: {
                transform: "translate(14px, 11px) scale(1)"
            }
        },
        MuiInputBase: {
            root: {
                "fontSize": "10px",
            },
            input: {},
            fullWidth: {
                height: "inherit"
            }
        },
        MuiFormControl: {
            fullWidth: {
                minWidth: 95,
                backgroundColor: "#f4f5f7",
                justifyContent: "center",
            }
        },
        MuiFormHelperText: {

            contained: {
                marginLeft: 0
            }
        },
        MuiDialogContent: {
            root: {
                padding: "8px 8px"
            }
        },
        MuiLinearProgress: {
            colorPrimary: {
                backgroundColor: "#fcfcfc"
            }
        },
        MuiOutlinedInput: {
            input: {
                padding: "7px 3px 7px 3px",
                border: "none"
            },
            multiline: {
                padding: "12px 6px 12px 6px",
                lineHeight: "1.5"
            }
        }, PrivateNotchedOutline: {
            root: {
                borderStyle: "none"
            }
        },
        MuiFormLabel: {
            root: {
                fontSize: 12
            }
        },
        MuiChip: {
            outlined: {
                backgroundColor: "#ffffff",
            },
            root: {
                height: "100%",
                justifyContent: "center",


            },
            sizeSmall: {
                height: 16,
                minWidth: 58,
                borderRadius: 5,
                backgroundColor: "#b3b9c5",
                "fontSize": "9px",
                "fontWeight": "normal",
                "fontStretch": "normal",
                "fontStyle": "normal",
                "lineHeight": "1.5",
                "letterSpacing": "normal",
                "textAlign": "right",
                "color": "#42516e"
            },
            labelSmall: {
                padding: 1,
                direction: "ltr"
            },
            clickableColorSecondary: {
                backgroundColor: "#deebfe",
                color: "#2684fe!important",
                '&:hover': {
                    backgroundColor: "unset"
                }
            },
            label: {
                whiteSpace: "unset"
            }
        },
        MuiListItemAvatar: {
            alignItemsFlexStart: {
                marginTop: 0
            }
        }, MuiMenuItem: {
            root: {
                fontSize: 10,
                minHeight: "unset"
            }
        },
        MuiListItem: {
            gutters: {
                paddingRight: 16,
                paddingLeft: 8
            }
        },
        MuiPopover: {
            paper: {
                transform: "translateY(35px)!important"
            }
        },
        MuiAvatar: {
            root: {
                width: 40,
                height: 40
            },
            colorDefault: {
                color: "#000",
                backgroundColor: "unset"
            }
        },
        MuiSlider: {
            markLabel: {
                fontSize: 10
            },
            markLabelActive: {
                color: "#2684fe"
            }
        },
        MuiSelect: {
            iconOutlined: {
                right: 3,
                fontSize: 20
            }
        },
        MuiTabs: {
            root: {
                minHeight: 40
            },
            indicator: {
                backgroundColor: "#2684fe"
            }
        },
        MuiTab: {
            labelIcon: {
                minHeight: 60,
                textColorSecondary: {
                    color: "#c0c7d1",
                }
            },
            MuiToggleButton: {
                root: {}
            }

        }
    }
});

// theme.overrides.MuiButton = {
//     root: {
//         [theme.breakpoints.up('md')]: {
//             fontSize: '0.8rem'
//         },
//         [theme.breakpoints.down('md')]: {
//             fontSize: '1.5rem'
//         }
//     }
// };

export default (theme)