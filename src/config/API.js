import axios from "axios";


export default function API(conf = {
    method: 'Get',
    url: ''
}, dispatch) {
    if (dispatch) {
        dispatch({type: "SHOW"});
        return axios(conf).then((res) => {
            dispatch({type: "HIDE"})
            return res;
        }).catch((err) => {
            dispatch({type: "HIDE"})
             throw err;
        });
    } else {
        return axios(conf);
    }
    // setTimeout(()=>{dispatch({type: "HIDE"})},5000)

}
