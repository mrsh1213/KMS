import React, {createContext, useReducer} from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import Backdrop from "@material-ui/core/Backdrop";
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: 99000,
        color: '#fff',
    },
}));
const backdropReducer = (state, action) => {
    switch (action.type) {
        case "SHOW":
            return {open: true};
        case "HIDE":
            return {open: false};
        default:
            return state;
    }
};
const initReducers = {
    open: false
};
const BackDropContext = createContext("BackDrop");

const BackDropProvider = ({children}) => {
    const [data, dispatch] = useReducer(backdropReducer, initReducers);
    const classes = useStyles();
    return (
        <BackDropContext.Provider value={{data, dispatch}}>
            <Backdrop className={classes.backdrop} open={data.open}>
                <CircularProgress color="inherit"/>
            </Backdrop>
            {children}
        </BackDropContext.Provider>
    );
};

export {BackDropContext, BackDropProvider}