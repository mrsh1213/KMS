import {useLocation} from "react-router";

export function arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
}

export function uniqueArrayObject(list, id, name) {
    let mymap = new Map();

    return list.filter(el => {
        const val = mymap.get(el[name]);
        if (val) {
            if (el[id] < val) {
                mymap.delete(el[name]);
                mymap.set(el[name], el[id]);
                return true;
            } else {
                return false;
            }
        }
        mymap.set(el[name], el[id]);
        return true;
    });
}

export function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g];
const arabicNumbers = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g];

export function fixNumbersToEn(str) {
    if (typeof str === 'string') {
        for (let i = 0; i < 10; i++) {
            str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
        }
    }
    return str;
};

export function inputDetectDirection(input) {
    let rtlChars = '\u0600-\u06FF';
//Arabic Supplement - Range:
//0750–077F
    rtlChars += '\u0750-\u077F';
//Arabic Presentation Forms-A - Range:
//FB50–FDFF
    rtlChars += '\uFB50-\uFDFF';
//Arabic Presentation Forms-B - Range:
//FE70–FEFF
    rtlChars += '\uFE70-\uFEFF';
//ASCII Punctuation - Range:
//0000-0020
    let controlChars = '\u0000-\u0020';
//General Punctuation - Range
//2000-200D
    controlChars += '\u2000-\u200D';
//Start Regular Expression magic
    let reRTL = new RegExp('^[' + controlChars + ']*[' + rtlChars + ']');
    let reControl = new RegExp('^[' + controlChars + ']*$');
    //Get the value of the input text
    let value = input.value;

    //Change the direction to rtl if the value matches one of the Arabic characters
    if (value.match(reRTL)) {
        input.dir = 'rtl';
    }

    //Don't do anything for control and punctuation characters
    else if (value.match(reControl)) {
        return false;
    }
    //Change the direction to ltr for any other character that is not Arabic, or control.
    else {
        input.dir = 'ltr';
    }
}

export function detectDirection(text) {
    let rtlChars = '\u0600-\u06FF';

    rtlChars += '\u0750-\u077F';

    rtlChars += '\uFB50-\uFDFF';

    rtlChars += '\uFE70-\uFEFF';
    let controlChars = '\u0000-\u0020';
    controlChars += '\u2000-\u200D';
    let reRTL = new RegExp('^[' + controlChars + ']*[' + rtlChars + ']');
    let reControl = new RegExp('^[' + controlChars + ']*$');
    //Get the value of the input text
    let value = text;

    //Change the direction to rtl if the value matches one of the Arabic characters
    if (value.match(reRTL)) {
        return 'rtl';
    }

    //Don't do anything for control and punctuation characters
    else if (value.match(reControl)) {
        return 'ltr';
    }
    //Change the direction to ltr for any other character that is not Arabic, or control.
    else {
        return 'ltr';
    }
}